<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class myAccount_model extends CI_Model {
	
	private $need;
	private $category;
	private $tag;
	private $media;
	
	function __construct() {  
		parent::__construct();
		$this->need = "ggol_needs";
		$this->category = "ggol_category";
		$this->tag = "ggol_tag";
		$this->media = "ggol_media";
		$this->media_need = "ggol_media_has_ggol_needs";
		$this->donation = "ggol_donations";
		$this->story = "ggol_stories";
		$this->load->model('data_sanitizer_model','sanitizer');
	}
	
	function getCategories(){
		$result = $this->db->get($this->category);
		return $result->result_array();
	}
	
	function getCompletedNeeds(){
		$id = $this->session->userdata('ID');
		
		/* get the ggol_userID using the ssion ID
		$id = $id;  change the ID to ggol userID.
		end of getting of ggol userID*/
		$q = "SELECT UserID FROM ggol_users WHERE SFLUserID= $id";
		$r = $this -> db -> query($q);
		$theresult = $r -> result();
		$uid = $theresult[0] -> UserID;
				
		$this->db->select('mn.*,n.*,m.*,n.Description as dsc');
		$this->db->from('ggol_media_has_ggol_needs as mn');
		$this->db->join('ggol_needs as n','n.NeedID = mn.NeedID','right');
		$this->db->where('n.IsApproved',1);
		$this->db->where('n.UserID',$uid);
		$this->db->join('ggol_media as m','m.MediaID = mn.MediaID','left');
		$this->db->where('m.MediaType','cover photo');
		$this->db->order_by('mn.DateCreated','desc');
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result->result_array();
	}
	
	function countCompletedNeeds(){
		$id = $this->session->userdata('ID');
		
		// get the ggol_userID using the ssion ID
		$q = "SELECT UserID FROM ggol_users WHERE SFLUserID= $id";
		$r = $this -> db -> query($q);
		$theresult = $r -> result();
		$uid = $theresult[0] -> UserID;
		// end of getting of ggol userID
		
		//echo $this->db->last_query();
		$this->db->where('UserID',$uid);
		$result = $this->db->get($this->need);
		return $result->row_array();
	}

	function getDonations($nid){
		$this->db->select_sum('DonatedAmount');
		$this->db->where('NeedID',$nid);
		$result = $this->db->get($this->donation);
		return $result->row_array();
	}
	
	function saveNeed($data){	
		$DateNeeded = date("Y-m-d H:i:s",strtotime($data['DateNeeded']));
		$IsApproved = 0;
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");
		$UserID = $data['UserId'];
		
		// find the ggol_user id using the Userid
		$q = "SELECT UserID FROM ggol_users WHERE SFLUserID= $UserID";
		$r = $this->db->query($q);
		$theresult = $r->result();
		$UserID = $theresult[0]->UserID;
		// of finding the ggol_user
		
		$need = array(
			'Title' => $data['Title'],
			'Beneficiary' => $data['Beneficiary'],
			'Description' => $data['Description'],
			'RequiredFunds' => $data['RequiredFunds'],
			'DescriptionFunds' => $data['DescriptionFunds'],
			'DateNeeded' => $DateNeeded,
			'IsApproved'=> $IsApproved,
			'DateCreated' => $DateCreated,
			'DateModified' => $DateModified,
			'CategoryID' => $data['CategoryID'],
			'UserID' => $UserID
		);
		
		$result = $this->db->insert($this->need,$need);
		if($result){
			$nid = $this->db->insert_id();
			return $nid;
		}else{ return false; }
	}
	
	function saveTag($data,$nid){
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");
		
		$tagArray = explode(',',$data);
		
		foreach ($tagArray as $tagdata) {
			$tag = array(
				'NeedID' => $nid,
				'ggol_tagcol' => $tagdata,
				'DateCreated' => $DateCreated,
				'DateModified' => $DateModified
			);
			$result = $this->db->insert($this->tag,$tag);
		}
		
		return $result;
	}
	
	function generateSerialNumber(){
		$template   = 'XX99-XX99-99XX-99XX';
		    $k = strlen($template);
		    $sernum = '';
		    for ($i=0; $i<$k; $i++)
		    {
		        switch($template[$i])
		        {
		            case 'X': $sernum .= chr(rand(65,90)); break;
		            case '9': $sernum .= rand(0,9); break;
		            case '-': $sernum .= '-';  break; 
		        }
		    }
		    return $sernum;
		/*$length = 4;
		$characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
		$first = '';
		$second = '';
		$third = '';
		$fourth = '';

	  	for ($f=0; $f<$length; $f++) {
	        $first .= $characters[rand(0, strlen($characters))];
			$second .= $characters[rand(0, strlen($characters))];
			$third .= $characters[rand(0, strlen($characters))];
			$fourth .= $characters[rand(0, strlen($characters))];
	    }
		
		return $first.'-'.$second.'-'.$third.'-'.$fourth;*/
	}
	
	function saveSerialNumber($serial,$nid){
		$data = array(
			'SerialNumber' => $serial,
			'NeedID' => $nid
		);
		$result = $this->db->insert('ggol_serialnumbers',$data);
		return $result;
	}
	
	function isFoundStory($nid){
		$this->db->where('NeedID',$nid);
		$result = $this->db->get($this->story);
		if($result->num_rows() == 1){
			return true;
		}else{
			return false;
		}
	}
	
    function _generate_image_name(){
    	return rand().'_'.rand(1,1000000).rand(1111111,9999999).'_'.time().'_n';            
    }   
    
   	function uploadPhoto($nid){
    	$config = array(
    		'allowed_types' => 'jpg|jpeg|gif|png',
            'upload_path' => realpath(APPPATH .'../images/needs'),
            'file_name' => $this->_generate_image_name(),
            'max_size' => 20000000
        );

        ini_set("max_execution_time", "3600");
		ini_set("max_input_time", "3600");
		ini_set("memory_limit", "1024M");
		ini_set("post_max_size", "16M");
		ini_set("upload_max_filesize", "16M");
            
        $this->load->library('upload', $config);
        if($this->upload->do_upload()){
        	$image_data = $this->upload->data();
            
			//Upload and resize for banner 
			/*
			$config = array();    
			$config['source_image'] = $image_data['full_path'];
			$config['new_image'] = 'images/needs/banners';
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 710;
			$config['height'] = 488;
          	$this->load->library('image_lib', $config);     
            $this->image_lib->resize();			
			$this->image_lib->clear();
			
			
			//Upload and resize for featured image
			$config = array();
			$config['source_image'] = $image_data['full_path'];
			$config['new_image'] = 'images/needs/featured';
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 449;
			//$config['height'] = 222;			
			$this->image_lib->initialize($config);
            $this->image_lib->resize();			
			$this->image_lib->clear();
			
			
			//Upload and resize for default thumbs
			$config = array();
			$config['source_image'] = $image_data['full_path'];
			$config['new_image'] = 'images/needs/thumbs';
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 120;
			$config['height'] = 88;			
			$this->image_lib->initialize($config);
			$this->image_lib->resize()
          	$this->image_lib->clear();
			*/
			
			//banner 710x488
			$this->_cropForMe($image_data['full_path'], APPPATH . '../images/needs/banners/'.$image_data['file_name'], 710, 488);
			//featured 440x222
			$this->_cropForMe($image_data['full_path'], APPPATH . '../images/needs/featured/'.$image_data['file_name'], 440, 222);
			//thumb 120x88
			$this->_cropForMe($image_data['full_path'], APPPATH . '../images/needs/thumbs/'.$image_data['file_name'], 120, 88);		
			
            $result = $this->saveCoverPhoto($image_data['file_name']);
			if($result){
				$this->saveHasMediaNeed($nid,$result);
				return $result;
			}
			
			return false;
			
        } else {
        	return false;
        }
	}
	
	function getImageTypes(){
		return array('.jpg','.jpeg','.gif','.png');
	}
	
	function escapeImageTypes($data){
		$data = str_replace($this->getImageTypes(), '', $data);
		return $data;
	}

	function notifyEmail(){
		$this->load->library('email');
		$message = "A new Need has been submitted. Please click this link to view ".base_url()."admin/";
		$this->email->from('admin@GivetheGiftofLiberty.org', 'Give the Gift of Liberty');
		$this->email->to('thomas@themarketaces.com'); 
		$this->email->subject('Need Notification');
		$this->email->message($message);			
		$this->email->send();
	}

    function saveCoverPhoto($image_path){
    	$MediaType = "cover photo";
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");
		
    	$data = array(
    		'MediaType' => $MediaType,
        	'MediaName'=> $image_path,
        	'MediaURL' => $image_path,
        	'DateCreated' => $DateCreated,
        	'DateModified' => $DateModified
       	);
                    
        $result = $this->db->insert($this->media,$data);
        if($result){
        	$mid = $this->db->insert_id();
			return $mid;
   	    }else{ return false; }
	}
	
	function saveHasMediaNeed($nid,$mid){
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");
		
		$data = array(
			'MediaID' => $mid,
			'NeedID' => $nid,
			'DateCreated' => $DateCreated,
			'DateModified' => $DateModified			
		);
		
		$result = $this->db->insert($this->media_need,$data);
		return $result;
	}
	
	function getOriginalPhoto($mid){
		$this->db->where('MediaID',$mid);
		$result = $this->db->get($this->media);
		return $result->row_array();
	}

	function resizeThumbnailImage($data){
		$thumb_image_name = realpath(APPPATH .'../images/needs')."/thumbs/".$data['thumb_name'];

		$imageurl = str_replace($this->config->base_url(), realpath(APPPATH).'/../', $data['image_name']);
		$image = $imageurl; //$data['image_name']; 

		$width = $data['w'];
		$height = $data['h'];
		$start_width = $data['x1'];
		$start_height = $data['y1'];
		$scale = $data['thumb_width']/$start_width;
		
		$scaleX = 120 /$width;
		$scaleY = 88 /$height;
		
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		
		$newImageWidth = round($width * $scaleX);
		$newImageHeight = round($height * $scaleY);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType){
			case "image/gif":
				$source=imagecreatefromgif($image); 
				break;
		    case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image); 
				break;
		    case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image); 
				break;
	  	}
		
		imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);

		switch($imageType) {
			case "image/gif":
		  		imagegif($newImage,$thumb_image_name); 
				break;
	      	case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
		  		imagejpeg($newImage,$thumb_image_name,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$thumb_image_name);  
				break;
	    }
		chmod($thumb_image_name, 0777);
		return $thumb_image_name;
	}

	function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}

	function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}
	
	private function _cropForMe($source, $destination, $w, $h){
		list($width, $height) = getimagesize($source);
        $ext = pathinfo($source);
        $ext = strtolower($ext['extension']);
        if ($ext == 'jpg' or $ext == 'jpeg') {
            $source = imagecreatefromjpeg($source);
        } elseif ($ext == 'gif') {
            $source = imagecreatefromgif($source);
        } elseif ($ext == 'png') {
            $source = imagecreatefrompng($source);
        }

        //Create preview
        $width_p = $width;
        $height_p = $height;

        $preview_width = $w;
        $preview_height = $h;

        if ($width_p > ($height_p * ($preview_width / $preview_height))) {
            $pro = ($preview_height / $height_p);
            $width_p*=$pro;
            $height_p*=$pro;
        } else {
            $pro = ($preview_width / $width_p);
            $width_p*=$pro;
            $height_p*=$pro;
        }

        $preview = imagecreatetruecolor($preview_width, $preview_height);

        $white = imagecolorallocate($preview, 255, 255, 255);
        imagefill($preview, 0, 0, $white);

        imagecopyresampled($preview, $source, -($width_p - $preview_width) / 2, -($height_p - $preview_height) / 2, 0, 0, $width_p, $height_p, $width, $height);
        //End of preview creation

        imagejpeg($preview, $destination);	
	}
}