<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class successstories_model extends CI_Model {
	
	private $stories;
	private $media;
	private $media_stories;
	
	function __construct(){
		parent::__construct();
		$this->stories = "ggol_stories";
		$this->media = "ggol_media";
		$this->media_stories = "ggol_media_has_ggol_needs";
	}	
	
	function getSuccessStories(){
		$this->db->select('s.*,n.Beneficiary,n.Title,n.RequiredFunds,n.Description,m.MediaURL');
		$this->db->from('ggol_stories as s');
		$this->db->where('s.IsActive',1);
		$this->db->where('n.IsActive',1);
		$this->db->join('ggol_media_has_ggol_needs as ms','ms.StoryID = s.StoryID');
		$this->db->join('ggol_needs as n','n.NeedID = ms.NeedID','right');
		$this->db->join('ggol_media as m','m.MediaID = ms.MediaID','left');
		$this->db->where('m.MediaType','success stories');
		$this->db->order_by('s.DateCreated','desc');
		$this->db->group_by('n.NeedID');
		$this->db->limit(12);
		$result = $this->db->get();
		return $result->result_array();
	}
	
	function page($offset){
		$this->db->select('s.*,n.Beneficiary,n.Title,n.RequiredFunds,n.Description,m.MediaURL');
		$this->db->from('ggol_stories as s');
		$this->db->where('s.IsActive',1);
		$this->db->join('ggol_media_has_ggol_needs as ms','ms.StoryID = s.StoryID');
		$this->db->join('ggol_needs as n','n.NeedID = ms.NeedID','right');
		$this->db->join('ggol_media as m','m.MediaID = ms.MediaID','left');
		$this->db->where('m.MediaType','success stories');
		$this->db->order_by('s.DateCreated','desc');
		$this->db->limit(12,$offset);
		$result = $this->db->get();
		return $result->result_array();
	}
	
	function cntSuccessStories(){
		$this->db->select('s.*,n.Beneficiary,n.Title,n.RequiredFunds,n.Description,m.MediaURL');
		$this->db->from('ggol_stories as s');
		$this->db->where('s.IsActive',1);
		$this->db->join('ggol_media_has_ggol_needs as ms','ms.StoryID = s.StoryID');
		$this->db->join('ggol_needs as n','n.NeedID = ms.NeedID','right');
		$this->db->join('ggol_media as m','m.MediaID = ms.MediaID','left');
		$this->db->where('m.MediaType','success stories');
		return $this->db->count_all_results();
	}
	
	function _generate_image_name(){
    	return rand().'_'.rand(1,1000000).rand(1111111,9999999).'_'.time().'_n';            
    }   
    
   	function uploadPhoto(){
    	$config = array(
    		'allowed_types' => 'jpg|jpeg|gif|png',
            'upload_path' => realpath(APPPATH .'../images/success-stories'),
            'file_name' => $this->_generate_image_name(),
            'max_size' => 2000
        );
            
         $this->upload->initialize($config);
        if($this->upload->do_upload('userfile')){
        	$image_data = $this->upload->data();
                
            $config = array(
            	'source_image' => $image_data['full_path'],
                'new_image' => 'images/success-stories/primary-photo',
                'create_thumb'=>FALSE,
                'maintain_ratio' => FALSE,
                'width' => 220,
                'height' => 93
            );
            
            $this->load->library('image_lib', $config);     
            $this->image_lib->initialize($config);
                
            if (!$this->image_lib->resize()){
            	unlink('images/success-stories/'.$image_data['file_name']);
                return FALSE;
            }else{
                $MediaID = $this->saveCoverPhoto($image_data['file_name']);
				if($MediaID){
					return $MediaID;
				}
                unlink('images/success-stories/'.$image_data['file_name']);
            }       
        }else{
        	return FALSE;
        }

	}

	function uploadReceipt(){	
		$config = array(
    		'allowed_types' => 'pdf',
            'upload_path' => realpath(APPPATH .'../files/needs/receipts'),
            'file_name' => $this->_generate_image_name()
       	);
		$this->upload->initialize($config);
		if($this->upload->do_upload('userfile1')){
			$pdf_data = $this->upload->data();
			$MediaID['id'] = $this->saveReceipt($pdf_data['file_name']);
			$MediaID['filename'] = $pdf_data['file_name'];
			if($MediaID['id']){
				return $MediaID;
			}
		}else{
			return FALSE;
		}
	}
	
	function uploadExpense(){
		$config = array(
    		'allowed_types' => 'pdf',
            'upload_path' => realpath(APPPATH .'../files/needs/expenses'),
            'file_name' => $this->_generate_image_name()
       	);
		$this->upload->initialize($config);
		if($this->upload->do_upload('userfile2')){
			$pdf_data = $this->upload->data();
			$MediaID['id'] = $this->saveExpense($pdf_data['file_name']);
			$MediaID['filename'] = $pdf_data['file_name'];
			if($MediaID['id']){
				return $MediaID;
			}
		}else{
			return FALSE;
		}
	}
	
	function saveReceipt($file_path){
		if(isset($file_path) or !empty($file_path)){
			$MediaType = "receipt pdf";
			$DateCreated = date("Y-m-d H:i:s");
			$DateModified = date("Y-m-d H:i:s");
			
	    	$data = array(
	    		'MediaType' => $MediaType,
	        	'MediaName'=> $file_path,
	        	'MediaURL' => $file_path,
	        	'DateCreated' => $DateCreated,
	        	'DateModified' => $DateModified
	       	);
	                    
	        $result = $this->db->insert($this->media,$data);
	        if($result){
	        	$mid = $this->db->insert_id();
				return $mid;
	   	    }else{ return false; }
		}
	}
	
	function saveExpense($file_path){
		if(isset($file_path) or !empty($file_path)){
			$MediaType = "expense pdf";
			$DateCreated = date("Y-m-d H:i:s");
			$DateModified = date("Y-m-d H:i:s");
			
	    	$data = array(
	    		'MediaType' => $MediaType,
	        	'MediaName'=> $file_path,
	        	'MediaURL' => $file_path,
	        	'DateCreated' => $DateCreated,
	        	'DateModified' => $DateModified
	       	);
	                    
	        $result = $this->db->insert($this->media,$data);
	        if($result){
	        	$mid = $this->db->insert_id();
				return $mid;
	   	    }else{ return false; }
		}
	}
	
    function saveCoverPhoto($image_path){
    	$MediaType = "success stories";
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");
		
    	$data = array(
    		'MediaType' => $MediaType,
        	'MediaName'=> $image_path,
        	'MediaURL' => $image_path,
        	'DateCreated' => $DateCreated,
        	'DateModified' => $DateModified
       	);
                    
        $result = $this->db->insert($this->media,$data);
        if($result){
        	$mid = $this->db->insert_id();
			return $mid;
   	    }else{ return false; }
	}
	
	function saveYoutubeLink($data){
		if(!empty($data['youtube_link'])){
			$MediaType = "embed";
			$DateCreated = date("Y-m-d H:i:s");
			$DateModified = date("Y-m-d H:i:s");
			
			$youtube = array(
				'MediaType' => $MediaType,
				'MediaName' => $data['youtube_link'],
				'MediaURL' => $data['youtube_link'],
				'DateCreated' => $DateCreated,
				'DateModified'=> $DateModified
			);
			
			$result = $this->db->insert($this->media,$youtube);
	        if($result){
	        	$mid = $this->db->insert_id();
				return $mid;
	   	    }else{ return false; }
		}
	}
	
	function saveStories($data,$mid){
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");
		$IsActive = 0;
		
		$stories = array(
			'MediaID' => $mid,
			'IsActive' => $IsActive,
			'DateCreated' => $DateCreated,
			'DateModified' => $DateModified,
			'NeedID' => $data['NeedID'],
			'thankYouLetter' => $data['thankYouLetter']
		);
		$result = $this->db->insert($this->stories,$stories);
		if($result){
			$sid = $this->db->insert_id();
			return $sid;
		}else{ return false; }
	}

	function saveHasMediaStories($nid,$mid,$sid){
		if(!empty($mid)){
			$DateCreated = date("Y-m-d H:i:s");
			$DateModified = date("Y-m-d H:i:s");
			
			$data = array(
				'MediaID' => $mid,
				'NeedID' => $nid,
				'StoryID' => $sid,
				'DateCreated' => $DateCreated,
				'DateModified' => $DateModified			
			);
			
			$result = $this->db->insert($this->media_stories,$data);
			return $result;
		}
	}
}
	