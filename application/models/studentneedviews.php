<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Studentviews Class Model
 *
 * 
 * Models that student need views 
 * 
 * @author Alvin Comahig
 * @version 1.0
 * Datas
  'UserID', 'int(11)', 'NO', '', NULL, ''
  'TotalDonation', 'decimal(32,0)', 'YES', '', NULL, ''
  'Title', 'varchar(255)', 'YES', '', NULL, ''
  'RequiredFunds', 'decimal(10,0)', 'YES', '', NULL, ''
  'NeedID', 'int(11)', 'NO', '', '0', ''
  'IsApproved', 'tinyint(1)', 'YES', '', NULL, ''
  'IsActive', 'tinyint(1)', 'NO', '', '1', ''
  'DescriptionFunds', 'text', 'YES', '', NULL, ''
  'Description', 'text', 'YES', '', NULL, ''
  'DateNeeded', 'datetime', 'YES', '', NULL, ''
  'DateModified', 'datetime', 'YES', '', NULL, ''
  'DateCreated', 'datetime', 'YES', '', NULL, ''
  'CategoryID', 'int(11)', 'NO', '', NULL, ''
  'Beneficiary', 'varchar(255)', 'YES', '', NULL, ''
  'ApprovalDate', 'datetime', 'YES', '', NULL, ''
 */
class Studentneedviews extends CI_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}

	function getAlmostDueNeed($condition = "") {
		$q = "SELECT * FROM almostdueneeds $condition";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getLatestNeed() {
		$q = "SELECT * FROM latestneeds";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getData($id, $field) {
		$q = "SELECT $field FROM almostdueneeds WHERE NeedID = $id";
		$r = $this->db->query($q);
		$t = '';
		if ($r->num_rows() > 0) {
			foreach ($r->result() as $row) {
				$t = $row->Title;
			}
			return $t;
		}
		else {
			return 0;
		}
		
	}

	function getNeed($NeedID) {
		$q = "SELECT * FROM almostdueneeds where NeedID = $NeedID";
		$r = $this->db->query($q);
		if ($r->num_rows() > 0) {
			foreach ($r->result() as $row) {}
			return $row;
		}
		else {
			return 0;
		}
	}

	function getProfile($profileID) {
		$q = "SELECT * FROM userprofileview where UserID = $profileID";
		$r = $this->db->query($q);
		if ($r->num_rows() > 0) {
			foreach ($r->result() as $row) {}
			return $row;
		}
		else {
			return 0;
		}
	}
	function getProfileByEmail($email) {
		$q = "SELECT * FROM userprofileview where email = '$email'";
		$r = $this->db->query($q);
		if ($r->num_rows() > 0) {
			foreach ($r->result() as $row) {}
			return $row;
		}
		else {
			return 0;
		}
	}

	function getDonors($NeedID) {
		$q = "SELECT UserID, Sum(DonatedAmount) TotalDonation, DonateDate FROM donationsview WHERE NeedID = $NeedID group by UserID";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getStatus($NeedID) {
		$q = "SELECT * FROM ggol_needstatus WHERE NeedID = $NeedID ORDER BY DateCreated DESC";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getDonateComments($NeedID) {
		$q = "
			select 
				dv.NeedID,
				dv.DonorID,
				dv.DonateReason,
				dv.DonateDate,
				(select firstname from userprofileview where UserID = dv.UserID) as firstname,
				(select lastname from userprofileview where UserID = dv.UserID) as lastname,
				(select username from userprofileview where UserID = dv.UserID) as username
			from donationsview dv where NeedID = $NeedID and PublishName = 1
			";
		$r = $this->db->query($q);
		return $r->result();
	}

	/* Note: update the view if you need more functions */

	function getStories($UserID) {
		$q = "SELECT * FROM storyview where UserID = $UserID";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getTopDonator() {
		$q = "SELECT UserID, (SELECT CONCAT(lastname, ', ', firstname) as Fullname FROM userprofileview as upv where upv.UserID = dv.UserID) as Fullname, Sum(DonatedAmount) TotalDonation, DonateDate FROM donationsview as dv  group by UserID order by TotalDonation DESC";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getTopNeeds() {
		$q = "SELECT *, (SELECT gc.CategoryName FROM ggol_category as gc where gc.CategoryID = an.CategoryID) as Category FROM almostdueneeds as an ORDER BY an.TotalDonation DESC";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getUnapprovedNeeds() {
		$q = "SELECT * FROM ggol_needs where IsApproved = 1";
		$r = $this->db->query($q);
		return $r->result();
	}

	function approveNeed($NeedID) {

		$q = "UPDATE ggol_needs SET IsApproved = 1 WHERE NeedID = '$NeedID' ";
		$r = $this->db->query($q);
		return $r->num_rows();
	}

	function getAllDonations() {
		$q = "select *,(SELECT CONCAT(lastname, ', ', firstname) as Fullname FROM userprofileview as upv where upv.UserID = dv.UserID) as Fullname, (select Title from ggol_needs where NeedID = dv.NeedID) as Title from donationsview as dv";
		$r = $this->db->query($q);
		return $r->result();
	}

	function getFeatured() {
		$q = "	SELECT 
					`gn`.*,
					(SELECT SUM(`ggol_donations`.`DonatedAmount`) FROM `ggol_donations` WHERE (`ggol_donations`.`NeedID` = `gn`.`NeedID`)) AS `TotalDonation`,
					(select 
                `needmediaview`.`MediaURL`
            from
                `needmediaview`
            where
                ((`needmediaview`.`MediaType` = 'cover photo')
                    and (`needmediaview`.`NeedID` = `gn`.`NeedID`))) AS `Photo`
				FROM 
					`ggol_needs` AS `gn` 
				WHERE 
					`gn`.`IsApproved` = 1 AND `gn`.`Featured` = 1 LIMIT 1";
		$result = $this->db->query($q);
		return $result->row_array();
	}
	function getMedia($mediatype) {
		$q = "select * from needmediaview where mediatype = '$mediatype'";
		$r = $this->db->query($q);
		return $r->result();
	}
	function checkStory($needid) {
		$q = "select * from ggol_stories where NeedID = $needid";
		$r = $this->db->query($q);
		
		return $r->num_rows();
	}
}