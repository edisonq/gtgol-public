<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminCmsNeed_model extends CI_Model{
	
	private $needs;
 
	 
	function __construct(){
    	parent::__construct();
		$this->needs='ggol_needs';
		$this->category='ggol_category';
		$this->wpusers='wp_users';
  	}
	
	function getPendingNeeds(){
		$this->db->select('n.NeedID,n.Beneficiary,n.Title,n.Description,n.RequiredFunds,n.DescriptionFunds,n.DateNeeded,n.IsApproved,n.DateCreated,n.DateModified,c.CategoryName,u.user_login,s.SerialNumber, media.MediaURL');
		$this->db->from('ggol_needs as n');
		$this->db->where('n.IsApproved',0);
		$this->db->join('ggol_category as c','c.CategoryID = n.CategoryID');
		$this->db->join('ggol_users as g','g.UserID = n.UserID');
		$this->db->join('wp_users as u','u.ID = g.SFLUserID');
		$this->db->join('ggol_serialnumbers as s','s.NeedID = n.NeedID');
		$this->db->join('ggol_media_has_ggol_needs as mhas','mhas.NeedID = n.NeedID');
		$this->db->join('ggol_media as media','media.MediaID = mhas.MediaID');
		$this->db->order_by('n.DateCreated','desc');

		$result = $this->db->get();
		//echo $this->db->last_query(); 

		return $result->result_array();
	}
	
	function getCategory($cid){
		$query = $this -> db -> get_where($this->category, array('CategoryID' => $cid));
		$result = $query -> row_array();
		$categoryName = $result['CategoryName'];
		return $categoryName;
	}
	
	function getWpUsers($uid){
	 $query = $this->db->get_where($this->wpusers,array('ID' => $uid));
	 $result = $query->row_array();
	 return $result['display_name'];
	}
	
	function approvePendingNeeds($nid){
		$ApprovalDate = date("Y-m-d H:i:s");
		$IsApproved = 1;
		$data = array('IsApproved' => $IsApproved, 'ApprovalDate' => $ApprovalDate);
		$this->db->where('NeedID',$nid);
		$result = $this->db->update($this->needs,$data);
		return $result;
	}
	
	function declinePendingNeeds($nid){
		$IsApproved = 2;
		$data = array('IsApproved' => $IsApproved);
		$this->db->where('NeedID',$nid);
		$result = $this->db->update($this->needs,$data);
		return $result;
	}
	
	function notifyCreator($email,$msg){
		$this->load->library('email');
		$this->email->from('admin@GivetheGiftofLiberty.org', 'Give the Gift of Liberty');
		$this->email->to($email); 
		$this->email->subject('Need Notification');
		$this->email->message($msg);			
		$this->email->send();
	}
	
	function getUserEmail($nid){
		$this->db->where('NeedID',$nid);
		$r = $this->db->get($this->needs);
		$nUser = $r->row_array();
		
		$this->db->where('UserID',$nUser['UserID']);
		$r1 = $this->db->get('ggol_users');
		$gUser = $r1->row_array();
		
		$this->db->where('ID',$gUser['SFLUserID']);
		$r2 = $this->db->get($this->wpusers);
		return $r2->row_array();
	}
	
	function author($login){
		$this->db->where('user_login',$login);
		$r1 = $this->db->get('wp_users');
		return $r1->row_array();
	}
	
	function downloadAllNeeds(){
		 $this->load->helper('csv');
		
		header("Content-type: text/csv; charset=utf-8");
		header("Content-Disposition: attachment; filename=needs.csv");
		header("Pragma: no-chache");
		header("Expires: 0");
	
		$output = fopen('php://output','w');
		
		fputcsv($output,array('NeedID','Beneficiary','Title','Description','Required Funds',"Funds' Description ",'Date Needed','Status','Date Created','Date Modified','Category','Featured','Created by','Serial Number','Active','With Success Story','Not Funded'));
		
		$this->db->select('n.NeedID,n.Beneficiary,n.Title,n.Description,n.RequiredFunds,n.DescriptionFunds,n.DateNeeded,n.IsApproved,n.DateCreated,n.DateModified,c.CategoryName,n.Featured,u.user_login,s.SerialNumber, n.IsActive');
		$this->db->from('ggol_needs as n');
		$this->db->join('ggol_category as c', 'c.CategoryID = n.CategoryID');
		$this->db->join('ggol_users as g', 'g.UserID = n.UserID');
		$this->db->join('wp_users as u', 'u.ID = g.SFLUserID');
		$this->db->join('ggol_serialnumbers as s', 's.NeedID = n.NeedID');
		$this->db->where('n.IsActive', '1');
		$this->db->order_by('n.DateCreated', 'desc');
		$result = $this->db->get();
		$needs = $result->result_array();
		
		foreach($needs as $n){
			if($n['IsApproved'] == 1){$approved = "Approved";}else{$approved = "Pending";}
			if($n['Featured'] == 0){$featured = "No";}else{$featured = "Yes";}
			if($n['IsActive'] == 1){$active = "Yes";}else{$active = "No";}
			
			$rStory = $this->db->get_where('ggol_stories',array('NeedID' => $n['NeedID']));
			$rStories = $rStory->result_array();
			if($rStories){$stories = "Yes";}else{$stories = "No";}
			
			$fNeed=$this->db->get_where('ggol_donations',array('NeedID' => $n['NeedID']));
			$fNeeds = $fNeed->result_array();
			if($fNeeds){$funded = "Yes";}else{$funded = "No";}
			
			fputcsv($output,array(
				$n['NeedID'],$n['Beneficiary'],$n['Title'],$n['Description'],$n['RequiredFunds'],$n['DescriptionFunds'],$n['DateNeeded'],$approved,$n['DateCreated'],$n['DateModified'],$n['CategoryName'],$featured,$n['user_login'],$n['SerialNumber'],$active,$stories,$funded
			)); 
		}
		
		fclose($output);
		exit;
	}
	function downloadDonations(){
		$this->db->select('d.DonationID,d.TransactionNumber,d.DonationReason, d.DonatedAmount, d.DateDonated, wpusers.user_login as "Donor UserName", wpusers.user_email as "Donors Email", needs.Title as "Donation Title", needs.Beneficiary, serial.SerialNumber as "Donation SerialNumber"');
		$this->db->from('ggol_donations as d');
		$this->db->join('ggol_donor as donor', 'donor.DonorID = d.DonorID');
		$this->db->join('ggol_users as user', 'user.UserID = donor.UserID');
		$this->db->join('wp_users as wpusers', 'wpusers.ID = user.SFLUserID');
		$this->db->join('ggol_needs as needs', 'needs.NeedID = d.NeedID');
		$this->db->join('ggol_serialnumbers as serial', 'serial.NeedID = needs.NeedID');
		$this->db->where('d.IsActive', '1');
		$this->db->order_by('d.DateCreated', 'desc');
		$donation = $this->db->get();
		// $result = $this->db->last_query(); 
		$this->load->helper('csv');
		$result = query_to_csv($donation, TRUE, 'donation.csv');
		return $result;
		
	}
}
