<?php
class data_sanitizer_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function get_unallowed_characters(){
		return array('"','^','&','--','<','>','*',';','=',"'",'%','�','�','$','!');
	}
	
	function sanitize($data){
		$data = str_replace($this->get_unallowed_characters(), '', $data);
		return $data;
	}
	
	function sanitize_data_array($data_array){
		$sanitize_data_array=array();
		
		foreach($data_array as $key => $data){
			$data = $this->sanitize($data);
			$sanitize_data_array[$key]=$data;
		}
		
		return $sanitize_data_array;
	}
	
	function get_script_tag_array(){
		return array('<script>','</script>','window.location=');
	}
	
	function sanitize_script_tag($data){
		$data = str_replace($this->get_script_tag_array(), '', $data);
		return $data;
	}
		
	function script_tag($data){
		$data = str_replace("<script>", "", $data);
		$data = str_replace("</script>", "", $data);
		return $data;
	}
	
	function single_quote($data){
		$data = str_replace("'", "", $data);
		return $data;
	}
	
	function semicolon($data){
		$data = str_replace(";", "", $data);
		return $data;
	}
	
	function double_dash($data){
		$data = str_replace("--", "", $data);
		return $data;
	}
	
	function window_location($data){
		$data = str_replace("window.location=", "", $data);
		return $data;
	}
	
	function unwanted_characters($data){
		$data = (filter_var($data, FILTER_SANITIZE_STRIPPED));
		return $data;
	}
	
	function letters_numbers_only($data){
		$data = preg_replace('#\W#', '', $data);
		return $data;
	}
	
}
