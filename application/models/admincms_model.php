<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminCMS_model extends CI_Model{
	
	private $cms_page;
	private $cms_menu;
	private $cms_menu_page;
	private $needs;
	private $category;
    private $wpusers; 
	 
	function __construct(){
    	parent::__construct();
        $this->cms_page='ggol_cms_page';
		$this->cms_menu='ggol_cms_menus';
		$this->cms_menu_page='ggol_cms_menus_has_ggol_cms_page';	
		$this->needs='ggol_needs';
		$this->category="ggol_category";
  	}
	
	function addPage($data){
		$dateCreated = date("Y-m-d H:i:s");    
		$dateModified = date("Y-m-d H:i:s");    
		$pageTemplate = 1;

		$page = array(
			'PageTitle' => $data['PageTitle'],
			'PageName' => $data['PageName'],
			'PageDescription' => $data['PageDescription'],
			'IsActive' => $data['IsActive'],
			'PageHTMLContent' => $data['PageHTMLContent'],
			'DateCreated' => $dateCreated,
			'DateModified' => $dateModified,
			'PageTemplateID' => $pageTemplate
		);
		
		$result = $this->db->insert($this->cms_page,$page);
		if($result){
			$id = $this->db->insert_id();
			return $id;
		}else{
			return false;
		}
	}
	
	function showAllPages(){
		$result = $this->db->get($this->cms_page);
		return $result->result_array();
	}
	
	function showSinglePage($id){
		$this->db->where('PageID',$id);
		$result = $this->db->get($this->cms_page);
		return $result->row_array();
	}
	
	function deletePage($id){
		$this->db->where('PageID',$id);
		$result = $this->db->delete($this->cms_page);
		return $result;
	}
	
	function updatePage($data){
		$dateModified = date("Y-m-d H:i:s");    

		$page = array(
			'PageTitle' => $data['PageTitle'],
			'PageName' => $data['PageName'],
			'PageDescription' => $data['PageDescription'],
			'IsActive' => $data['IsActive'],
			'PageHTMLContent' => $data['PageHTMLContent'],
			'DateModified' => $dateModified
		);
		
		$this->db->where('PageID',$data['PageID']);
		$result = $this->db->update($this->cms_page,$page);
		return $result;	
	}
	
	function addMenu($data){
		$dateCreated = date("Y-m-d H:i:s");    
		$dateModified = date("Y-m-d H:i:s");   
		$menuType = "Default";
		
		$menu = array(
			'MenuName' => $data['MenuName'],
			'MenuType' => $menuType,
			'DateCreated' => $dateCreated,
			'DateModified' => $dateModified
		);
		
		$result = $this->db->insert($this->cms_menu,$menu);
		return $result;
	}
	
	function deleteMenu($mid){
		$this->db->where('MenuID',$mid);
		$result = $this->db->delete($this->cms_menu);
		return $result;
	}
	
	function getAllMenu(){
		$result = $this->db->get($this->cms_menu);
		return $result->result_array();
	}
	
	function getSingleMenu($mid){
		$this->db->where('MenuID',$mid);
		$result = $this->db->get($this->cms_menu);
		return $result->row_array();
	}
	
	function getMenuHeader(){
		$this->db->where('MenuType','Header');
		$result = $this->db->get($this->cms_menu);	
		return $result->result_array();
	}

	function checkMenuPages($data){
		$this->db->where('PageID',$data['PageID']);
		$this->db->where('MenuID',$data['MenuID']);
		$result = $this->db->get($this->cms_menu_page);
		if($result->num_rows() == 1){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function addMenuPages($data){
		$menu_pages = array(
			'MenuID' => $data['MenuID'],
			'PageID' => $data['PageID']
		);
		$result = $this->db->insert($this->cms_menu_page,$menu_pages);
		return $result;
	}
	
	function deleteMenuPages($mid){
		$this->db->where('MenuID',$mid);
		$result = $this->db->delete($this->cms_menu_page);
		return $result;
	}
	
	function removeMenuPage($id){
		$this->db->where('PageHasMenu',$id);
		$result = $this->db->delete($this->cms_menu_page);
		return $result;
	}
	
	function getMenuPages($mid){
		$this->db->select('pm.*,p.*');
		$this->db->from('ggol_cms_menus_has_ggol_cms_page as pm');
		$this->db->where('pm.MenuID',$mid);
		$this->db->join('ggol_cms_page as p','p.PageID = pm.PageID');
		$result = $this->db->get();
		return $result->result_array();
	}
	
	function getAvailableMenuHeader(){
		$this->db->select('p.*');
		$this->db->from('ggol_cms_menus_has_ggol_cms_page as pm');
		$this->db->join('ggol_cms_page as p','p.PageID = pm.PageID');
		$this->db->join('ggol_cms_menus as m','m.MenuID = pm.MenuID');
		$this->db->where('m.MenuType','header');
		$result = $this->db->get();
		return $result->result_array();
	}
	
	function getAvailableMenuFooter(){
		$this->db->select('p.*');
		$this->db->from('ggol_cms_menus_has_ggol_cms_page as pm');
		$this->db->join('ggol_cms_page as p','p.PageID = pm.PageID');
		$this->db->join('ggol_cms_menus as m','m.MenuID = pm.MenuID');
		$this->db->where('m.MenuType','footer');
		$result = $this->db->get();
		return $result->result_array();
	}
	
	function getDefaultMenu(){
		$this->db->where('MenuType','Default');
		$result = $this->db->get($this->cms_menu);
		return $result->result_array();
	}
	
	function updateGroupMenu($data){
		$menu = array('MenuType' => $data['MenuType']);
		$this->db->where('MenuID',$data['MenuID']);
		$result = $this->db->update($this->cms_menu,$menu);
		return $result;
	}
	
	function checkGroupMenu($data){
		$this->db->where('MenuType',$data['MenuType']);
		$result = $this->db->get($this->cms_menu);
		if($result->num_rows() == 1){
			return $result->row_array();
		}else{
			return false;
		}
	}
	
	function updateCheckGroupMenu($mid){
		$menu = array('MenuType' => 'Default');
		$this->db->where('MenuID',$mid);
		$result = $this->db->update($this->cms_menu,$menu);
		return $result;
	}
	
	function getMenuGroup($group){	
		$this->db->select('pm.*,m.*');
		$this->db->from('ggol_cms_menus_has_ggol_cms_page as pm');
		$this->db->join('ggol_cms_menus as m','m.MenuID = pm.MenuID');
		$this->db->where('m.MenuType',$group);
		$result = $this->db->get();
		return $result->row_array();
	}
	
	
	function removeGroupMenu($mid){
		$menu = array('MenuType' => 'Default');
		$this->db->where('MenuID',$mid);
		$result = $this->db->update($this->cms_menu,$menu);
		return $result;
	}
	
	function getApprovedNeeds() {
		$this->db->select('n.NeedID,n.Beneficiary,n.Title,n.Description,n.RequiredFunds,n.DescriptionFunds,n.DateNeeded,n.IsApproved,n.DateCreated,n.DateModified,c.CategoryName,n.Featured,u.user_login,s.SerialNumber');
		$this->db->from('ggol_needs as n');
		$this->db->where('n.IsApproved', 1);
		$this->db->where('n.IsActive', '1');
		$this->db->join('ggol_category as c', 'c.CategoryID = n.CategoryID');
		$this->db->join('ggol_users as g', 'g.UserID = n.UserID');
		$this->db->join('wp_users as u', 'u.ID = g.SFLUserID');
		$this->db->join('ggol_serialnumbers as s', 's.NeedID = n.NeedID');
		$this->db->order_by('n.DateCreated', 'desc');
		$result = $this->db->get();
		return $result->result_array();
	}

	function getAllNeeds() {
		$this->db->select('n.NeedID,n.Beneficiary,n.Title,n.Description,n.RequiredFunds,n.DescriptionFunds,n.DateNeeded,n.IsApproved,n.DateCreated,n.DateModified,c.CategoryName,n.Featured,u.user_login,s.SerialNumber, n.IsActive');
		$this->db->from('ggol_needs as n');
		$this->db->join('ggol_category as c', 'c.CategoryID = n.CategoryID');
		$this->db->join('ggol_users as g', 'g.UserID = n.UserID');
		$this->db->join('wp_users as u', 'u.ID = g.SFLUserID');
		$this->db->join('ggol_serialnumbers as s', 's.NeedID = n.NeedID');
		$this->db->where('n.IsActive', '1');
		$this->db->order_by('n.DateCreated', 'desc');
		$result = $this->db->get();
		return $result->result_array();
	}

	function getSingleNeeds($nid) {
		$this->db->select('n.NeedID,n.Beneficiary,n.Title,n.Description,n.RequiredFunds,n.DescriptionFunds,n.DateNeeded,n.IsApproved,n.DateCreated,n.DateModified,n.CategoryID,n.Featured,c.CategoryName,u.user_login');
		$this->db->from('ggol_needs as n');
		$this->db->where('n.NeedID', $nid);
		$this->db->join('ggol_category as c', 'c.CategoryID = n.CategoryID');
		$this->db->join('ggol_users as g', 'g.UserID = n.UserID');
		$this->db->join('wp_users as u', 'u.ID = g.SFLUserID');
		$this->db->order_by('n.DateCreated', 'desc');
		$result = $this->db->get();
		return $result->row_array();
	}

	function setFeatured($NeedID) {
		$this->db->where('Featured', 1);
		$this->db->update($this->needs, array('Featured' => 0));
		
		$this->db->where('NeedID', $NeedID);
		$this->db->update($this->needs, array('Featured' => 1));
	}

	function getAllCategory() {
		$result = $this->db->get($this->category);
		return $result->result_array();
	}

	function updateNeed($data) {
		$DateNeeded = date("Y-m-d H:i:s", strtotime($data['DateNeeded']));
		$DateModified = date("Y-m-d H:i:s");

		$need = array(
			'Beneficiary' => $data['Beneficiary'],
			'Title' => $data['Title'],
			'Description' => $data['Description'],
			'RequiredFunds' => $data['RequiredFunds'],
			'DescriptionFunds' => $data['DescriptionFunds'],
			'DateNeeded' => $DateNeeded,
			'DateModified' => $DateModified,
			'CategoryID' => $data['CategoryID'],
			'Featured' => $data['Featured'],
			'IsActive' => $data['IsActive']
		);

		$this->db->where('NeedID', $data['NeedID']);
		$result = $this->db->update($this->needs, $need);
		return $result;
	}
}