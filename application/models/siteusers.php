<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Siteusers Class Model
 *
 * 
 * Models that involve user management and functions 
 * 
 * @author Alvin Comahig
 * @version 1.0
 */
class Siteusers extends CI_Model {
	function __construct() { 
        // Call the Model constructor
        parent::__construct();
    }
	/*
	* @Function - Checks if user_email or user_login exists dependeng on specified @Parameters
	* @Tables wp_users
	* @Parameters 
	*	$val  = Value of either Login or email
	* 	$checktype = login or email
	* @Return Boolean
	*
		*/
	function check_user($val, $checktype) {
		$result = 0;
		if(isset($checktype)) {
			if($checktype == 'login' || $checktype == 'email') {
				if(isset($val)) {
					$query = $this->db->query("select ID as IsExist from wp_users  where user_$checktype = '$val' limit 0, 1");
					$result = $query->num_rows();
				}
			}
		}
        return $result;
    }
	function verify_exist($email, $login) {
		$result = 0;
		if(isset($email) && isset($login)) {
			$emailquery = $this->db->query("select ID as IsExist from wp_users  where user_email = '$email' limit 0, 1");
			$loginquery = $this->db->query("select ID as IsExist from wp_users  where user_login = '$login' limit 0, 1");
			
			$result = $emailquery->num_rows() + $loginquery->num_rows();
		}
		return $result;
	}
	function generate_password( $length = 12, $special_chars = true, $extra_special_chars = false ) {
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		if ( $special_chars )
			$chars .= '!@#$%^&*()';
		if ( $extra_special_chars )
			$chars .= '-_ []{}<>~`+=,.;:/?|';

		$password = '';
		for ( $i = 0; $i < $length; $i++ ) {
			$password .= substr($chars, rand (0, strlen($chars) - 1), 1);
		}
		return $password;
	}
	/*
		# CreateStudentAccount
		#	IN UserName VARCHAR(60),
		#	IN UserPass VARCHAR(64),
		#	IN UserEmail VARCHAR(100),
		#	IN UserFN LONGTEXT,
		#	IN UserLN LONGTEXT,
		#	IN ActivateKey VARCHAR(60),
		#	IN UserTypeID INT(11)
	*/
	function create_user($userdata) {
		$username = $userdata['username'];
		$email = $userdata['email'];
		$firstname = $userdata['firstname'];
		$lastname = $userdata['lastname'];
		$password = $userdata['password'];
		$hashedpw = $userdata['hashedpw'];
		
		$s = "call CreateStudentAccount('$username','$hashedpw','$email','$firstname','$lastname','','2')";
		$q = $this->db->query($s); 
		$r = $q->num_rows();
		if($r > 0) 
			$ret = 1;
		else 
			$ret = 0;
		return $this->db->affected_rows();
	}
	function check_login_admin($username, $password) {
		$quser = "SELECT 
					user_pass, 
					ID, 
					(SELECT UserID FROM ggol_users as gu where SFLUserID = wu.ID) as SFLUserID,
					(SELECT UserTypeID FROM ggol_users as gu where SFLUserID = wu.ID) as Usertype
				FROM wp_users as wu where user_login = '$username' 
				AND (SELECT UserTypeID FROM ggol_users as gu where SFLUserID = wu.ID) = 1";
		
		$ruser = $this->db->query($quser);
		if($ruser->num_rows() > 0) {
			foreach($ruser->result() as $result) {
				$hashpw = $result->user_pass;
				$id =  $result->ID;
			}
			if($this->phpass->check($password, $hashpw)) {
				
				$return['status'] = 1;
				$return['ID'] = $id;
				
			}
			else {
				$return['status'] = 0;//"Password";
			}
		}
		else {
			$return['status'] = 0;//"Username";
		}
		return $return;
	}
	/*
		
	*/
	function check_login($username, $password) {
		$quser = "SELECT user_pass, ID,
					(SELECT UserID FROM ggol_users as gu where SFLUserID = wu.ID) as SFLUserID
				FROM wp_users as wu where user_login = '$username' limit 0,1";
		/* OLD
			$quser = "SELECT user_pass, ID FROM wp_users where user_login = '$username' limit 0,1";
		*/
		$ruser = $this->db->query($quser);
	
		if($ruser->num_rows() > 0) {
			foreach($ruser->result() as $result) {
				$hashpw = $result->user_pass;
				$id =  $result->ID;
			}
			if($this->phpass->check($password, $hashpw)) {
				$query = "select * from ggol_users where SFLUserID = ".$id;
				$results = $this->db->query($query);
				
				if($results->num_rows() > 0) {
					/* Will put something here in the near future */
					$return['SID'] = $result->SFLUserID;
				}
				else {
					/* Adds the user to the GGOL Database */
					$add_ggol_user_query = "INSERT INTO ggol_users(`SFLUserID`,`UserTypeID`, `DateCreated`, `DateModified`)VALUES ($id,2, NOW(), NOW())";
					$add_user_results = $this->db->query($add_ggol_user_query);
					$return['SID'] = $this->db->insert_id();
				}
				$return['status'] = 1;
				$return['ID'] = $id;
				
			}
			else {
				$return['status'] = 0;//"Password";
			}
		}
		else {
			$return['status'] = 0;//"Username";
		}
		return $return;
		
	}
	function fetch_data($username, $password) {
		
	}
}
