<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dynamic_model extends CI_Model{
	
	private $cms_pages;
	
	function __construct(){
    	parent::__construct();
		$this->cms_pages = "ggol_cms_page";
	}
	
	function dynamic_pages($page_name){
		$this->db->where('PageName',$page_name);
		$result = $this->db->get($this->cms_pages);
		return $result->row_array();
	}
	
}

