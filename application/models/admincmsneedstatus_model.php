<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminCmsNeedStatus_model extends CI_Model{
	
	private $needs;
	private $needstatus;

	 
	function __construct(){
    	parent::__construct();
		$this->needs='ggol_needs';
		$this->needstatus='ggol_needstatus';
  	}
	
	function getPendingNeedStatus(){
		$result = $this->db->order_by('DateCreated','desc')->get_where($this->needstatus,array('IsActive' => 0));
		return $result->result_array();
	}
	
	function approvePendingStat($sid,$nid){
	/*since there are no IsApproved and ApprovalDate fields in ggol_needstatus table 
	  we just set the IsActive as the IsApproved and DateModified as the ApprovalDate field*/
		$ApprovalDate = date("Y-m-d H:i:s");
		$IsApproved = 1;
		$data = array('IsActive' => $IsApproved, 'DateModified' => $ApprovalDate);
		$result = $this->db->where(array('StatusID' => $sid,'NeedID' => $nid))->update($this->needstatus,$data);
		return $result;
	}
	
}
