<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_model extends CI_Model{
	
	private $story;
	
	function __construct(){
		parent::__construct();
		$this->story = "ggol_stories";
	}
	
	function getPendingStories(){
		$this->db->where('IsActive',0);
		$this->db->order_by('DateCreated','desc');
		$result = $this->db->get($this->story);
		return $result->result_array();
	}
	
	function approveStories($sid){
		$story = array('IsActive' => 1);
		$this->db->where('StoryID',$sid);
		$result = $this->db->update($this->story,$story);
		return $result;
	}
 
}
	