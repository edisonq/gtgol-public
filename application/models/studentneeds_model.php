<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class studentNeeds_model extends CI_Model {

	private $needs;
	private $donor;
	private $donation;
	private $status;
	private $serial;
	private $tag;

	function __construct() {
		parent::__construct();
		$this->needs = "ggol_needs";
		$this->donor = "ggol_donor";
		$this->donation = "ggol_donations";
		$this->status = "ggol_needstatus";
		$this->serial = "ggol_serialnumbers";
		$this->tag = "ggol_tag";
		$this->load->library("pagination");
	}

	function countNeed() {
		$this->db->where('IsApproved', 1);
		$result = $this->db->count_all_results($this->needs);
		return $result;
	}

	function getStory($nid) {
		$this->db->select('s.*,n.Beneficiary,n.Title,n.RequiredFunds,n.Description,m.MediaURL');
		$this->db->from('ggol_stories as s');
		$this->db->where('s.IsActive', 1);
		$this->db->where('s.NeedID', $nid);
		$this->db->join('ggol_media_has_ggol_needs as ms', 'ms.StoryID = s.StoryID');
		$this->db->join('ggol_needs as n', 'n.NeedID = ms.NeedID', 'right');
		$this->db->join('ggol_media as m', 'm.MediaID = ms.MediaID', 'left');
		$this->db->where('m.MediaType', 'success stories');
		$result = $this->db->get();
		return $result->row_array();
	}
	
	function getStoryPhotos($NeedID){
		$SQL = "SELECT * FROM `ggol_media_has_ggol_needs` AS `a` LEFT JOIN `ggol_media` AS `b` ON (`a`.`MediaID`=`b`.`MediaID`) WHERE `a`.`NeedID`='$NeedID' AND `b`.`MediaType`='success stories'";
		$result = $this->db->query($SQL);
		return $result->result_array();
	}

	function getNeeds() {
		$this->db->select('mn.*,n.*,m.MediaURL,n.Description as dsc');
		$this->db->from('ggol_media_has_ggol_needs as mn');
		$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'right');
		$this->db->where('n.IsApproved', 1);
		//$this->db->limit($config['per_page'],$this->uri->segment(3));
		$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'left');
		$this->db->where('m.MediaType', 'cover photo');
		$this->db->order_by('mn.DateCreated', 'desc');
		$this->db->limit(5);
		$result = $this->db->get();
		return $result->result_array();
	}

	function page($offset) {
		$this->db->select('mn.*,n.*,m.MediaURL,n.Description as dsc');
		$this->db->from('ggol_media_has_ggol_needs as mn');
		$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'right');
		$this->db->where('n.IsApproved', 1);
		$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'left');
		$this->db->where('m.MediaType', 'cover photo');
		$this->db->order_by('mn.DateCreated', 'desc');
		$this->db->limit(5, $offset);
		$result = $this->db->get();
		return $result->result_array();
	}

	function cntNeeds() {
		$this->db->select('mn.*,n.*,m.MediaURL,n.Description as dsc');
		$this->db->from('ggol_media_has_ggol_needs as mn');
		$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'right');
		$this->db->where('n.IsApproved', 1);
		$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'left');
		$this->db->where('m.MediaType', 'cover photo');
		return $this->db->count_all_results();
	}

	function getSingleNeed($nid) {
		$this->db->select('mn.*,n.*,m.MediaURL,m.MediaName,n.Description as dsc');
		$this->db->from('ggol_media_has_ggol_needs as mn');
		$this->db->where('mn.NeedID', $nid);
		$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'right');
		//$this->db->where('n.IsApproved',1);
		$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'left');
		$this->db->order_by('mn.DateCreated', 'desc');
		$result = $this->db->get();
		return $result->row_array();
	}

	function getStoryImage($nid) {
		$this->db->select('m.MediaURL as sURL');
		$this->db->from('ggol_media as m');
		$this->db->where('m.MediaType', 'cover photo');
		$this->db->join('ggol_media_has_ggol_needs as mn', 'mn.MediaID = m.MediaID');
		$this->db->where('mn.NeedID', $nid);
		$result = $this->db->get();
		return $result->row_array();
	}

	function getUserNeeds($nid) {
		$this->db->select('w.display_name as name');
		$this->db->from('ggol_needs as n');
		$this->db->where('n.NeedID', $nid);
		$this->db->join('ggol_users as u', 'u.UserID = n.UserID');
		$this->db->join('wp_users as w', 'w.ID = u.SFLUserID');
		$result = $this->db->get();
		return $result->row_array();
	}

	/* function getDonor($nid){
	  $this->db->select('u.user_nicename as donor_name');
	  $this->db->from('ggol_donor as d');
	  $this->db->join('ggol_donations as dn','dn.DonorID = d.DonorID','right');
	  $this->db->where('dn.NeedID',$nid);
	  $this->db->join('wp_users as u','u.ID = d.UserID');
	  $result = $this->db->get();
	  return $result->row_array();
	  } */

	function getNeedStatus($nid) {
		$this->db->order_by('DateCreated', 'desc');
		$result = $this->db->get_where($this->status, array('NeedID' => $nid, 'IsActive' => 1));
		return $result->result_array();
	}

	function getDonations($nid) {
		$this->db->select_sum('DonatedAmount');
		$this->db->where('NeedID', $nid);
		$result = $this->db->get($this->donation);
		return $result->row_array();
	}

	function countDonor($nid) {
		$this->db->select('COUNT(*) as ctr');
		$this->db->from('ggol_donations');
		$this->db->where('NeedID', $nid);
		$result = $this->db->get();
		return $result->row_array();
	}

	function addDonor($uid) {
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");
		$donor = array(
			'DateCreated' => $DateCreated,
			'DateModified' => $DateModified,
			'UserID' => $uid
		);
		$result = $this->db->insert($this->donor, $donor);
		if ($result) {
			$did = $this->db->insert_id();
			return $did;
		} else {
			return false;
		}
	}

	function addDonations($data, $did, $ispublish, $trans) {
		$DateDonated = date("Y-m-d H:i:s");
		$DateCreated = date("Y-m-d H:i:s");
		$DateModified = date("Y-m-d H:i:s");

		$donation = array(
			'PhoneNumber' => $data['PhoneNumber'],
			'DonationReason' => $data['DonationReason'],
			'DonatedAmount' => $data['DonatedAmount'],
			'IsPublishDonorsName' => $ispublish,
			'TransactionNumber' => $trans,
			'DateDonated' => $DateDonated,
			'DateModified' => $DateModified,
			'DateCreated' => $DateCreated,
			'NeedID' => $data['NeedID'],
			'DonorID' => $did
		);

		$result = $this->db->insert($this->donation, $donation);
		return $result;
	}

	function searchStudentNeeds($data) {
		if (!empty($data['sort'])) {
			$this->db->select('mn.*,n.*,m.MediaURL,n.Description as dsc');
			$this->db->from('ggol_media_has_ggol_needs as mn');
			if (!empty($data['schoolName'])) {
				$this->db->like('n.Beneficiary', $data['schoolName']);
			}
			if (!empty($data['needType'])) {
				$this->db->where('n.CategoryID', $data['needType']);
			}
			$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'inner');
			$this->db->where('n.IsApproved', 1);
			$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'inner');
			$this->db->where('m.MediaType', 'cover photo');
			if ($data['sort'] == 'deadline') {
				$this->db->order_by('n.DateNeeded', 'asc');
			} else if ($data['sort'] == 'created') {
				$this->db->order_by('n.DateCreated', 'desc');
			}
			$this->db->limit(5);
			$result = $this->db->get();
			return $result->result_array();
		}
	}

	function pageSSN($offset, $data) {
		if (!empty($data['sort'])) {
			$this->db->select('mn.*,n.*,m.MediaURL,n.Description as dsc');
			$this->db->from('ggol_media_has_ggol_needs as mn');
			if (!empty($data['schoolName'])) {
				$this->db->like('n.Beneficiary', $data['schoolName']);
			}
			if (!empty($data['needType'])) {
				$this->db->where('n.CategoryID', $data['needType']);
			}
			$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'inner');
			$this->db->where('n.IsApproved', 1);
			$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'inner');
			$this->db->where('m.MediaType', 'cover photo');
			if ($data['sort'] == 'deadline') {
				$this->db->order_by('n.DateNeeded', 'asc');
			} else if ($data['sort'] == 'created') {
				$this->db->order_by('n.DateCreated', 'desc');
			}
			$this->db->limit(5, $offset);
			$result = $this->db->get();
			return $result->result_array();
		}
	}

	function cntSearchStudentNeeds($data) {
		if (!empty($data['sort'])) {
			$this->db->select('mn.*,n.*,m.MediaURL,n.Description as dsc');
			$this->db->from('ggol_media_has_ggol_needs as mn');
			if (!empty($data['schoolName'])) {
				$this->db->like('n.Beneficiary', $data['schoolName']);
			}
			if (!empty($data['needType'])) {
				$this->db->where('n.CategoryID', $data['needType']);
			}
			$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'inner');
			$this->db->where('n.IsApproved', 1);
			$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'inner');
			$this->db->where('m.MediaType', 'cover photo');
			if ($data['sort'] == 'deadline') {
				$this->db->order_by('n.DateNeeded', 'asc');
			} else if ($data['sort'] == 'created') {
				$this->db->order_by('n.DateCreated', 'desc');
			}
			return $this->db->count_all_results();
		}
	}

	function searchTag($tag, $nid) {
		$this->db->where('NeedID', $nid);
		$this->db->like('ggol_tagcol', $tag);
		$result = $this->db->get('ggol_tag');
		if ($result->num_rows() > 0 or $result->num_rows == 1) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	function customSearchSerial($data) {
		$this->db->where('SerialNumber', $data['keyword']);
		$res = $this->db->get($this->serial);
		if ($res->num_rows() == 1) {
			return $res->row_array();
		} else {
			return false;
		}
	}

	function customSearchTags($data) {
		$this->db->like('ggol_tagcol', $data['keyword']);
		$res = $this->db->get($this->tag);
		if ($res->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function customSearchProjects($k) {
		$this->db->select('mn.*,n.*,m.MediaURL,n.Description as dsc');
		$this->db->from('ggol_media_has_ggol_needs as mn');
		$this->db->like('n.Title', $k);
		$this->db->join('ggol_needs as n', 'n.NeedID = mn.NeedID', 'inner');
		$this->db->where('n.IsApproved', 1);
		$this->db->join('ggol_media as m', 'm.MediaID = mn.MediaID', 'inner');
		$this->db->where('m.MediaType', 'cover photo');
		$this->db->order_by('n.DateCreated', 'desc');
		$result = $this->db->get();
		return $result->result_array();
	}

	function recordView($nid) {
		$ip = $this->input->ip_address();

		$data = array(
			'IPaddress' => $ip,
			'NeedID' => $nid,
			'users_ID' => $this->session->userdata('ID')
		);
		$result = $this->db->insert('ggol_needsViews', $data);
		return $result;
	}

	function getPasts($limit) {
		$SQL = "SELECT 
					`n`.*,
					(SELECT SUM(`d`.`DonatedAmount`) FROM `" . $this->donation . "` AS `d` WHERE `d`.`NeedID`=`n`.`NeedID`) AS `raised`,
					`m`.`MediaURL` AS `file` 
				FROM 
					`" . $this->needs . "` AS `n`
				INNER JOIN 
					`needmediaview` AS `m` ON (`n`.`NeedID` = `m`.`NeedID` and `m`.`MediaType` = 'cover photo')		
				WHERE 
				`n`.`IsApproved` = '1' AND NOW() > `n`.`DateNeeded` ORDER BY `n`.`DateNeeded` DESC LIMIT " . $limit . "";
		$result = $this->db->query($SQL);
		return $result->result_array();
		//$result = $result->result_array();
		//echo '<pre>';
		//die(print_r($result));
	}

	function USA() {
		$array = array(
			'Alaska',
			'Alabama',
			'Arkansas',
			'American Samoa',
			'Arizona',
			'California',
			'Colorado',
			'Connecticut',
			'D.C.',
			'Delaware',
			'Florida',
			'Micronesia',
			'Georgia',
			'Guam',
			'Hawaii',
			'Iowa',
			'Idaho',
			'Illinois',
			'Indiana',
			'Kansas',
			'Kentucky',
			'Louisiana',
			'Massachusetts',
			'Maryland',
			'Maine',
			'Marshall Islands',
			'Michigan',
			'Minnesota',
			'Missouri',
			'Marianas',
			'Mississippi',
			'Montana',
			'North Carolina',
			'North Dakota',
			'Nebraska',
			'New Hampshire',
			'New Jersey',
			'New Mexico',
			'Nevada',
			'New York',
			'Ohio',
			'Oklahoma',
			'Oregon',
			'Pennsylvania',
			'Puerto Rico',
			'Palau',
			'Rhode Island',
			'South Carolina',
			'South Dakota',
			'Tennessee',
			'Texas',
			'Utah',
			'Virginia',
			'Virgin Islands',
			'Vermont',
			'Washington',
			'Wisconsin',
			'West Virginia',
			'Wyoming',
			'Military Americas',
			'Military Europe/ME/Canada',
			'Military Pacific'
		);

		return $array;
	}

}
