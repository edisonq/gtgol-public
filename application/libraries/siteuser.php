<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Siteuser Class Library
 *
 * 
 * Library that involve user management and functions 
 * 
 * @author Alvin Comahig
 * @version 1.0
 */
class Siteuser {
	function __construct() {
       $this->CI = &get_instance();
	   $this->CI->load->model('Siteusers');
    }
	public function CheckUserEmail($email='') {
		return $this->Siteusers->check_user($email, 'email');
	}
	public function CheckUserLogin($login='') {
		echo $login;
		return $this->Siteusers->check_user($login,'login');
	}
}