<!DOCTYPE HTML>
<html>
    <head>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

        <!-- Open Graph Meta -->
        <meta property="og:image" content="https://givethegiftofliberty.org/fb.png"/>
        <meta property="og:url" content="https://givethegiftofliberty.org/">
        <meta property="og:title" content="Give the Gift of Liberty - A Students For Liberty Project"/>
        <meta property="og:description" content="Give the Gift of Liberty - Most students do not have the financial resources to carry out their group's projects introducing the ideas of liberty to more young people and educating themselves on the intellectual foundations of a free society.  This website is to help them find other individuals who are willing to give them that support."/>
        <!-- Open Graph Meta -->

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $site_name ?></title> 
        <!-- -->
        <?php
        if ($this->uri->segment(0) == 'how-it-works') {
            echo "<!--<link rel='stylesheet'   href='http://givethegiftofliberty.com/style/how-it-works.css'     type='text/css'     media='screen'    />-->";
        }
        /* Page css render for each page */
        if (isset($styles)) {
            foreach ($styles as $style) {
                echo '<link rel="stylesheet" href="' . base_url() . 'style/' . $style . '.css" type="text/css" media="screen" />' . "\n";
            }
        }
        ?>
        <link href='//fonts.googleapis.com/css?family=Crete+Round:400,400italic' rel='stylesheet' type='text/css'>
        <link rel='stylesheet' href="<?php echo base_url(); ?>style/main.css" type="text/css" media="screen" />
        <link rel='stylesheet' href="<?php echo base_url(); ?>style/story.css" type="text/css" media="screen" />
        <script type='text/javascript'>var js_baseurl = '<?php echo base_url(); ?>'; </script>
        <script type='text/javascript' src='<?php echo base_url(); ?>script/jquery-1.8.2.min.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>script/main.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>script/search.js'></script>

        <?php 
            if ($this->uri->segment(1) == 'donation') {
                /*?><script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><?php*/
            }
        ?>
        

        <script type="text/javascript">stLight.options({publisher: "87934fe3-9d83-406a-96b8-d147128b4a07"});</script>
		<?php
		/* Page javascript render for each page */
		if (isset($scripts)) {
			foreach ($scripts as $script) {
				echo '<script type="text/javascript" src="' . base_url() . 'script/' . $script . '.js"></script>' . "\n";
			}
		}
		
		?>
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-37021047-1']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </head>
    <body>
        <header>
            <div id="headerdark">
                <div id="headerdarkContent"></div>
            </div>
            <div id="headerLight">
                <span id="ribbonleft">&nbsp;</span>
                <a id="logo" href="<?= base_url(); ?>" title="Give the Gift of Liberty">&nbsp;</a>
                <span id="ribbon">&nbsp;</span>
                <div id="upperRight">
                    <div id="#loginarea">
						<?php
						if ($this->session->userdata('logged_in') == 1) {
							echo 'Welcome <a href="#">' . $this->session->userdata('username') . '</a> | <a href="' . base_url('logout') . '">Logout</a> | <a href="' . base_url() . 'my-account/my-needs" class="createManageNeeds">Create &amp; Manage Needs</a>';
						} else {
							echo '<a href="' . base_url('login') . '">Login</a> | <a href="' . base_url('create-account') . '">Create Account</a>';
						}
						?>
                    </div>
                    
                    <form style="margin-top: 7px;" action="<?= base_url('custom-search') ?>">
                        <!--a href="" id="facebook" class="socialButton">&nbsp;</a>
                        <a href="" id="twitter" class="socialButton">&nbsp;</a>
                        <a href="" id="googleplus" class="socialButton">&nbsp;</-->
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style " style="float: left;">
                            <a class="addthis_button_facebook_like" fb:like:layout="button_count" addthis:url="https://givethegiftofliberty.org"></a>
                            <a class="addthis_button_tweet" addthis:url="https://givethegiftofliberty.org"></a>
                            <div class="g-plusone" data-size="tall" data-annotation="none" data-href="https://givethegiftofliberty.org" style="float: left;"></div>
                            <script type="text/javascript">
                            (function() {
                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                            po.src = 'https://apis.google.com/js/plusone.js';
                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                            })();
                            </script>
                        </div>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50c9cdba7a2069a6"></script>
                        <!-- AddThis Button END -->
                        <?php 
                        /*if ($this->uri->segment(1) == 'donation') { 
                        ?>
                        <span class='st_facebook_hcount' displayText='Facebook'></span>
                        <span class='st_twitter_hcount' displayText='Tweet'></span>
                        <span class='st_googleplus_hcount' displayText='Google +'></span> 
                        <?php
                        }*/ ?>
                        <input value="Search" id="websiteSearchinput" type="text" name="keyword" class="searchField" /><input id="searchButton" type="submit" value="Go!">
                    </form>

                </div>
                <div id="linkContainer">
                    <ul>
						<?php
						$CI = & get_instance();
						$CI->load->model('admincms_model', 'menu');
						$menuHeader = $CI->menu->getAvailableMenuHeader();
						if (!isset($menuHeader) or empty($menuHeader)):
							?>
							<li><a href="<?= base_url() ?>" class="withoutborder">Home</a></li>
							<li><a href="<?= base_url('how-it-works') ?>" class="withborder">How it Works</a></li>
							<li><a href="<?= base_url('why-give') ?>" class="withborder">Why Give</a></li>
							<li><a href="<?= base_url('student-needs') ?>" class="withborder">Student Needs</a></li>
							<li><a href="<?= base_url('success-stories') ?>" class="withborder">Success Stories</a></li>
							<li><a href="<?= base_url('about-sfl') ?>" class="withborder">About SFL</a></li>
						<?php else: ?>
							<li><a href="<?= base_url(); ?>" class="withoutborder">Home</a></li>
							<li><a href="<?php echo base_url('student-needs') ?>" class="withborder">Student Needs</a></li>
							<li><a href="<?php echo base_url('success-stories') ?>" class="withborder">Success Stories</a></li>
							<?php foreach ($menuHeader as $row): ?>					
								<li><a href="<?= base_url() . $row['PageName'] ?>" class="withborder"><?= $row['PageTitle'] ?></a></li>
							<?php endforeach; ?>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </header>
		<?php echo $this->template->message(); ?>    
		<?php echo $this->template->yield(); ?>
        <footer>
            <div id="footerContent">
                <span id="footerRibbon">&nbsp;</span>

                <div id="footerLink">
                    <ul>
						<?php
						$menuFooter = $CI->menu->getAvailableMenuFooter();
						if (!isset($menuFooter) or empty($menuFooter)):
							?>
							<li><a href="<?= base_url(); ?>">Home</a></li>
							<li><a href="<?= base_url('how-it-works') ?>" class="footerLinkLine">How it Works</a></li>
							<li><a href="<?= base_url('why-give') ?>" class="footerLinkLine">Why Give</a></li>
							<li><a href="<?= base_url('student-needs') ?>" class="footerLinkLine">Student Needs</a></li>
							<li><a href="<?= base_url('success-stories') ?>" class="footerLinkLine">Success Stories</a></li>
							<li><a href="<?= base_url('about-sfl') ?>" class="footerLinkLine">About SFL</a></li>
							<li><a href="<?= base_url('login') ?>" class="footerLinkLine">Student Login</a></li>
							<!--<li><a href="<?= base_url('site-map') ?>" class="footerLinkLine">Site Map</a></li>-->
						<?php else: ?>
							<li><a href="<?= base_url(); ?>">Home</a></li>
							<li><a href="<?= base_url('student-needs') ?>" class="footerLinkLine">Student Needs</a></li>
							<li><a href="<?= base_url('success-stories') ?>" class="footerLinkLine">Success Stories</a></li>
							<?php foreach ($menuFooter as $row): ?>		
								<li><a href="<?= base_url() . $row['PageName'] ?>" class="footerLinkLine"><?= $row['PageTitle'] ?></a></li>
								<?php
							endforeach;
						endif;
						?>
                    </ul>
                    <div style="clear:both;"></div>
                </div>
                <div id="footerColumn1">
                    <a href="#" id="footerLogo">&nbsp;</a>
                    <p>
                        <a href="<?= base_url('privacy-policy'); ?>">Privacy Policy</a>
                        | <a href="<?= base_url('terms-of-service'); ?>">Terms of Service</a>
                    </p>
                </div>
                <div id="footerColumn2">
                    <img src="<?= base_url('/images/Gift_of_Giving.gif'); ?>" width="119" height="119" alt="Give the Gift of Liberty"></div>
                <div id="footerColumn3">
                    <p>Copyright 2012.  Students For Liberty
                        <br>All Rights Reserved</p>
                    <p>Web design by <a href="http://themarketaces.com/" target="_blank">Market Aces</a><p>
                </div>
            </div>
            <div id="footerDark">
            </div>
			<span class="staticMessageBar" style="border-left: 1px solid #2F2F2F; border-top: 1px solid #2F2F2F; border-top-left-radius: 2px; bottom: 0; position: fixed; right: 0; z-index: 9999;">
                <span style="background: #1D1D1D; filter: alpha(opacity=90); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;" id="staticMessageBar">
                    <span style="color: #D3992D; padding: 0px 20px;"><a style="text-decoration: underline;" target="_blank" href="<?= base_url('script/feedback.html') ?>">Welcome to our new website.  For feedback and bug reporting, click here</a>.</span>
                </span>
            </span>
        </footer>
    </body>
</html>
