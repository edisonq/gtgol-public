<!DOCTYPE html>
<html>
<head>
	<title>Please Login</title>
	<link href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" type= "text/css" rel="stylesheet" />
	<link href="<?php echo base_url();?>style/main.css" type= "text/css" rel="stylesheet" />
	<link href="<?php echo base_url();?>style/admin.css" type= "text/css" rel="stylesheet" />
	<script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>script/jquery-1.8.2.min.js"></script>
	<script type="text/javascript">var js_base_url = '<?php echo base_url();?>';</script>
	<script type="text/javascript" src="<?php echo base_url();?>script/adminlogin.js"></script>
	
</head>
<body>	
	<div class="container">
	<div class="span6" style="margin: 150px 265px auto;"><a id="logo" href="<?php echo base_url();?>" title="Give the Gift of Liberty" style="background-position: 0px 0px; margin:0px; padding: 0px;">&nbsp;</a></div>
	<div class="span4 well" style="margin: 0px 294px auto;">
		<?php echo $this->template->message(); ?>    
		<?php echo $this->template->yield(); ?>
		
		<!--<div class="form-signin">
			<h2 class="form-signin-heading">Please sign in</h2>
			<div id="errmessage"></div>
			<input type="text" id="uname" class="input-block-level username" placeholder="Username">
			<input type="password" id="pwd" class="input-block-level password" placeholder="Password">
			<label class="checkbox">
			<input type="checkbox" value="remember-me"> Remember me
			</label>
			<button id="btnlogin" class="btn btn-large btn-primary" type="submit">Sign in</button>
		</div>-->
	</div>
    </div>
</body>
</html>