<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <link href="<?php echo base_url(); ?>style/siteadmin.css" type= "text/css" rel="stylesheet" />
        <title>Administrator: <?php echo $site_name ?></title>
        <script type="text/javascript" src="<?php echo base_url(); ?>script/jquery-1.8.2.min.js"></script>	
        <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" type= "text/css" rel="stylesheet" />
        <script type="text/javascript">var base_url = "<?php echo base_url(); ?>admin";</script>
        <script type="text/javascript" src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>bootstrap/js/bootstrap-dropdown.js"></script>
    </head>
    <body>
        <div id="wrap">
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container-fluid">
                        <a href="<?php echo base_url(); ?>admin" class="brand">Give the Gift of Liberty</a>	
                        <div class="nav-collapse collapse">
                            <p class="navbar-text pull-right">
                                Welcome <a class="navbar-link" href="#">
                                    <span class="label label-info"><?php echo @$username; ?></span></a>
                                | <a href="<?php echo base_url() . 'admin/logout'; ?>">Logout</a>
                            </p>
                            <ul class="nav">
                                <li class="active"><a href="<?php echo base_url(); ?>admin">Dashboard</a></li>
                                <li><a href="<?php echo base_url(); ?>admin/needs">Needs</a></li> 
                                <li><a href="<?php echo base_url(); ?>admin/donations">Donations</a></li> 
                                <li><a href="<?php echo base_url(); ?>admin/stories">Stories</a></li>
                                <li><a href="<?php echo base_url(); ?>admin/media">Media</a></li>
                                <li><a href="<?php echo base_url(); ?>admin/status">Need Status</a></li>
                                <li><a href="<?php echo base_url(); ?>admin/dashboard">CMS</a></li>						
                            </ul>
                        </div>			
                    </div>	
                </div>
            </div>
			<?php echo $this->template->message(); ?>    
			<?php echo $this->template->yield(); ?>
        </div>
    </body>
</html>