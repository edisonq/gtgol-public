<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  <div id="content">
  	<div id="contentHeader">
    	<h1>Student Login</h1>
    </div>
    <p>&nbsp;</p>
    <h2>Log in with your SFL Account</h2>
	<div id="errmessage2">Username already taken.</div>
    <div id="loginbox">
    	<p>
        <label>
        Username:<br>
        <input name="username" type="text" class="logintext username" maxlength="60">
        </label>
        </p>
        <p>
        <label>
        Password:<br>
        <input name="username" type="password" class="logintext password">
        </label>
        <br>
        <a href="https://studentsforliberty.org/wp-login.php?action=lostpassword">Forgot Username/Password?</a>
        <p>
        <label class="smalltext" >
        <input name="rememberMe" type="checkbox" value="rememberMe">Remember Me</label>
        <input name="login" type="submit" value="Login" id="loginbutton">
        </p>
    </div>
    <div>&nbsp;</div>
    <h2>Don't have an SFL Account? Create one!</h2>
	<div id="errmessage">Username already taken.</div>
    <div id="signupPage">
    	<p>
        <label class="lblusername">
        Username<span>*</span>:<br>
        <input name="username" type="text" class="logintext username" maxlength="60">
        </label>
        </p>
        <p>
        <label class="lblemail">
        Email<span>*</span>:
        <input name="email" type="text" class="logintext email" maxlength="100">
        </label>
        </p>
        <p>
        <label class="lblfirstname">
        First Name<span>*</span>:
        <input name="firstname" type="text" class="logintext firstname">
        </label>
        </p>
        <p>
        <p>
        <label class="lbllastname">
        Last Name<span>*</span>:
        <input name="lastname" type="text" class="logintext lastname">
        </label><br>
        <span class="smalltext">A password will be sent to your email address</span>
        </p>
        <p>
        	&nbsp;<input name="Signup" type="submit" value="Sign Up" id="signupButton">
        </p>
    </div>
    <div class="divspacer">&nbsp;</div>
  </div>
</div>
<?php if(isset($scroll)){ ?>
	<script>$('body,html').animate({scrollTop:$('body').height() }, 800);</script>
<?php }?>