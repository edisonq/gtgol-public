
<br />
<div id="contentArea">
  <div id="content">
    <div class="socialMedia socialMedia--2">
      <!--ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul-->
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAside--2">
        <div class="advertise1">
			<h1><a href="http://studentsforliberty.org/webinar-program/" target="_blank"><img src="<?php echo base_url('images/ads/1.jpg') ?>" alt="" /></a></h1>
		</div>        
        <div class="myNeeds">
          <ul>
          	<li><a href="<?php echo base_url();?>my-account/my-needs">My Needs</a></li>
            <li class="active"><a href="<?php echo base_url();?>my-account/create-need">Create a Need</a></li>
            <li><a href="<?php echo base_url();?>my-account/completed-need">Completed Needs</a></li>
            <li><a href="#">Log Out</a></li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/tabling-kit-request/" target="_blank"><img src="<?php echo base_url('images/ads/2.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-international-sfl-conference/" target="_blank"><img src="<?php echo base_url('images/ads/3.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-european-students-for-liberty-conference/" target="_blank"><img src="<?php echo base_url('images/ads/4.jpg') ?>" alt="" /></a></h1>
		</div>                    
      </aside> 
      <section id="createNeeds">
        <h1>Welcome to your Give the Give of Liberty Account!</h1>
        <span>Get started by filling out this simple form to create your Need.</span>

        <div class="box boxCneeds">
        	<form action="<?php echo base_url();?>my-account/save-need" method="post" id="create-needs" enctype="multipart/form-data">
	            <h2>Title of Project:</h2>
	            <input type="text" name="Title">
	            <div class="clear"></div>
	            <h2>Student/Organization to Benefit:</h2>
	            <input type="text" name="Beneficiary">            
	            <div class="clear"></div>
	            <h2>Description of Need:</h2>
	            <textarea name="Description"></textarea>
	            <div class="clear"></div>            
	            <h2>Target Goal:</h2>
	            <input class="targetGoal" type="text" placeholder="$" onkeypress="return isNumberKey(event)" name="RequiredFunds">
	            <div class="clear"></div>
	            <h2>Description of what funds will go towards (i.e. budget):</h2>
	            <textarea class="descFunds" name="DescriptionFunds"></textarea>
	            <div class="clear"></div> 
	            <h2>Date funds are needed by:</h2>
	            <input class="monthDay" type="text" placeholder="mm/dd/yy" name="DateNeeded" id="datepicker">
	            <!--<select class="selectYear" name="Year">
	              <option value="0">yyyy</option>
	              <option value="2012">2012</option>
	            </select>  -->
	            <div class="clear"></div>    
	            <h2>Need Type:</h2>
	            <select class="selectNeed" name="CategoryID">
	              <option value="">Select a need type</option>
	              <?php foreach($categories as $row){?>
		              <option value="<?php echo $row['CategoryID'];?>"><?php echo $row['CategoryName'];?></option>
		          <?php } ?>
	            </select>  
	            <div class="clear"></div>
	            <div class="uploadPhoto">
	              <h2>Upload a Cover Photo:</h2>     
	              <span class="tagsKeyword">"Please upload images that are 710 pixels by 488 pixels."</span><br />
	              <div class="clear"></div>       
	                <input type="file" name="userfile"/><br>
	            </div>
	            <div class="clear"></div>
	            <a href="javascript:void(0)" style="display:none;"id="buttones">testlang</a>
	            <h2 class="t2">Tags/Keywords:</h2>
	            <span class="tagsKeyword">Type in keywords that relate to your Need (e.g. scholarship, travel, books, etc...) use commas to separate tags.</span>
	            <input class="inputTags" type="text" name="ggol_tagcol"/> 
	            <a href="javascript:void(0);" class="submitNeedBtn">Submit Need</a>
	            <span id="uploader" style="color: #222;"></span>
	            <input type="submit" value="Submit Need" style="display:none;" class="submit"/>        
	            <div class="clear"></div>
	            <input type="hidden" value="<?php echo $this->session->userdata('ID');?>" id="UserId" name="UserId" />     
            </form>    
        </div>
            
      </section>
      <div class="clear"></div>       
    </section>
  </div>
</div>
	<div id="popupContact">
		
		<div id="innerWindow">
			<div id="popup-label">
		
			</div>
        <a id="popupContactClose">x</a>
		<h1>Thank You For Your Submission!</h1><br />
		<p id="contactArea">
			You have successfully submitted a Need for your group or organization. Once SFL approves your need, it will be publicly listed on the website
			for donors to look at and contribute. You can check the status of your Need or update it by clicking the "My Needs" link in the sidebar.
		</p>
		<div id="popup-buttons">
			<div id="anchor-crop"></div>
			<input type="hidden" id="media-id"/>
		</div>
        </div>
	</div>
	<div id="backgroundPopup"></div>
<script type="text/javascript">
	$("#datepicker").datepicker({
    	  changeMonth: true,
          changeYear: true,
          yearRange: "-5:+25",
    });
</script>