<div class="container-fluid" style="margin-top: 60px;">
	<div class="well">
		<div class="navbar">
			<div class="navbar-inner">
				<a class="brand" href="#">Need Management</a>
				<ul class="nav">
					<li><a href="<?php echo base_url(); ?>admin/needs">All Needs</a></li>
					<li><a href="<?php echo base_url(); ?>admin/needs/pending">Pending Needs</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>admin/needs/approved">Approved Needs</a></li>
				</ul>
			</div>
		</div>
		<?php if (!empty($approve)): ?>
			<table id="admin-table" class="table table-striped">
				<thead>
					<tr>
						<th><small>NeedID</small></th>
						<th><small>Serial Number</small></th>
						<th><small>Title</small></th>
						<th><small>Beneficiary</small></th>
						<!--th><small>Description</small></th-->
						<th><small>Required Funds</small></th>
						<!--th><small>Fund Description</small></th-->
						<th><small>Deadline</small></th>
						<th><small>Date Created</small></th>
						<th><small>Category</small></th>
						<th><small>Featured</small></th>
						<th><small>Created by</small></th>
						<th><small>Status</small></th>
						<th><small>Actions</small></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($approve as $row): ?>
						<tr>
							<td><?php echo $row['NeedID']; ?></td>
							<td><?php echo $row['SerialNumber']; ?></td>
							<td><?php echo $row['Title']; ?></td>
							<td><?php echo $row['Beneficiary']; ?></td>
							<!--td><?php echo $row['Description']; ?></td-->
							<td>$ <?php echo number_format($row['RequiredFunds']); ?></td>
							<!--td><?php echo $row['DescriptionFunds']; ?></td-->
							<td><?php echo date('m/d/y', strtotime($row['DateNeeded'])); ?></td>
							<td><?php echo date('m/d/y', strtotime($row['DateCreated'])); ?></td>
							<td><?php echo $row['CategoryName']; ?></td>
							<td><?= $row['Featured'] == 1 ? 'Yes' : '<a class="confirm-featured" href="' . base_url() . 'admin/needs/setfeatured?NeedID=' . $row['NeedID'] . '">No</a>' ?></td>
							<td><a href="<?= base_url('admin/needs/author?author=' . $row['user_login']) ?>"><?= $row['user_login']; ?></a></td>
							<td><?php
				if ($row['IsApproved'] == 0):
					echo "Pending";
				else:
					echo "Active";
				endif;
						?>
							</td>
							<td><a href="<?php echo base_url(); ?>admin/needs/edit?nid=<?php echo $row['NeedID']; ?>">Update</a></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php
		else:
			echo "<h4>No Approved Needs Available</h4>";
		endif;
		?>
	</div>
</div>
<script type="text/javascript">
	$(".confirm-featured").click(function(){
		return confirm("Are You sure you want to set this NEED as Featured?");
	});
</script>