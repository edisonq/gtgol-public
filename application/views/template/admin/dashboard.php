<script style="text/javascript" src="<?php echo base_url();?>script/adminapproval.js"></script>
<div class="container-fluid" style="margin-top: 60px;">
	<div class="row-fluid">
		<div class="span4">
			<!--Popular needs-->
			<div class="well sidebar-nav">
				<ul class="nav nav-list">
					<li class="nav-header">POPULAR NEEDS</li>
					<li class="active">
						<table class="table">
							<tr><th><small>Title</small></th><th><small>Category</small></th><th><small>Gained</small></th></tr>
							<?php
								foreach($topneeds as $needs) {
							?>
								<tr>
									<td><small><a href="<?php echo base_url();?>admin/needs/edit?nid=<?php echo $needs->NeedID; ?>"><?php echo $needs->Title; ?></a></small></td>
									<td><small><span class="label label-success"><?php echo $needs->Category; ?></span></small></td>
									<td><small>$<?php echo ($needs->TotalDonation)? $needs->TotalDonation: "0"; ?></small></td>
								</tr>
								<?php
									}
								?>
						</table>
					</li>
					<li><small><a href="<?php echo base_url();?>admin/needs">View all needs</a></small></li>
				</ul>
			</div>
			<!--End of Popular needs-->
			<!--Top Donors-->
			<div class="well sidebar-nav">
				<ul class="nav nav-list">
					<li class="nav-header">TOP DONORS</li>
					<li class="active">
						<table class="table">
							<tr><th><small>Name</small></th><th><small>Donated</small></th></tr>
							<?php /* foreach($topdonors as $donors) { ?>
								<tr>
									<td><small><a href="#"><?php echo $donors->Fullname; ?></a></small></td>
									<td><small>$<?php echo $donors->TotalDonation; ?></small></td>
								</tr>
							<?php } */ ?>
						</table>
					</li>
					<li><small><a href="<?php echo base_url();?>admin/donations">View all donations</a></small></li>
				</ul>
			</div>
			<!--End of Top Donors-->
		</div>
		<!--Pending-->
			<div class="span8">
				<div class="well">
					<h3>Pending Needs</h3>
					<div class="row-fluid">
						<?php /*<div style="display: none;">
							<pre>
								<?php var_dump($unapprovedneeds); ?>
							</pre>
						</div>*/ ?>
						<ul class="thumbnails">
						<?php 
							$i = 0;
							foreach($unapprovedneeds as $uneeds) {
							
							if($i < 3) {
						?>
							<li class="span4">
								<div class="thumbnail">
									<img alt="" src="<?php echo base_url().'images/needs/thumbs/thumbnail_'.$uneeds['MediaURL']; ?>">
									<div class="caption">
										<h3><?php echo substr($uneeds["Title"],0,15).'...';?></h3>
										<p><?php echo substr($uneeds["Description"],0,80).'<a href="'.base_url().'admin/needs/edit?nid='.$uneeds['NeedID'].'">[...]</a>';?></p>
										<form id="approveNeed<?php echo $uneeds['NeedID'];?>" action="<?php echo base_url();?>admin-approval/pending-need?nid=<?php echo $uneeds['NeedID'];?>" method="GET">
										<p><input  type="submit" style="display:none" id="submitApprove<?php echo $uneeds['NeedID'];?>" onclick="approveDashPendingNeeds(<?php echo $uneeds['NeedID'];?>);"/><a class="btn btn-primary" href="javascript:void(0)" onclick="clickSubmit('need',<?php echo $uneeds['NeedID'];?>)">Approve</a> <a class="btn btn-danger" href="#">Decline</a></p>
										</form>
									</div>
								</div>
							</li>
							<?php }
								$i++;
							} ?>
							  
							</ul>
							<a href="<?php echo base_url();?>admin/needs/pending">More pending needs here</a>
						  </div>				
					</div>
			</div>
			<!--End of Pending-->
	</div>
</div>