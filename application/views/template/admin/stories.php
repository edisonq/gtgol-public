<div class="container-fluid" style="margin-top: 60px;">
	<div class="well">
		<h3>Stories</h3>
		<?php if(!empty($pending)):?>
		<table id="admin-table" class="table">
			<thead>
				<tr>
					<th>Story ID</th>
					<th>Thank You Letter</th>
					<th>Date Created</th>
					<th>Date Modified</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($pending as $row):?>
					<tr>
						<td><?php echo $row['StoryID'];?></td>
						<td><?php echo $row['thankYouLetter']; ?></td>
						<td><?php echo date('m/d/y',strtotime($row['DateCreated']));?></td>
						<td><?php echo date('m/d/y',strtotime($row['DateModified']));?></td>
						<td><?php 
							if($row['IsActive'] == 0):
								echo "Pending";
							else:
								echo "Active";
							endif;
							?>
						</td>
						<td><a href="<?php echo base_url();?>admin/stories/approve?sid=<?php echo $row['StoryID'];?>">Approve</a></td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<?php
				else:
					echo "<h4>No Pending Stories Available</h4>";
			endif;
		?>
	</div>
</div>