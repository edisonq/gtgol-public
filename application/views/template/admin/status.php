<?php 
		$CI =& get_instance();
		$CI->load->model('admincms_model','acms');
?>
<script style="text/javascript" src="<?php echo base_url();?>script/adminapproval.js"></script>
<div class="container-fluid" style="margin-top: 60px;">
	<div class="well">
			<h3>Pending for approval needs.</h3>
			<table class="table">
				<thead>
					<tr>
						<th>Status ID</th>
						<th>Need ID</th>
						<th>Status Message</th>
						<th>Date Created</th>
						<th>Approved</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($pendingStatus)): ?>
						<?php foreach($pendingStatus as $pStatus): ?>
						<?php 
							$statusMessageLength = strlen($pStatus['StatusMessage']);
							$statusMessage = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />',$pStatus['StatusMessage']);
						?>
					<form id="approveStatus<?php echo $pStatus['StatusID'];?>" action="<?php echo base_url();?>admin-approval/pending-status?sid=<?php echo $pStatus['StatusID'];?>&nid=<?php echo $pStatus['NeedID'];?>" method="GET">
					<tr id="status<?php echo $pStatus['StatusID'];?>">
						<td><?php echo $pStatus['StatusID'];?></td>
						<td><?php echo $pStatus['NeedID'];?></td>
						<td >
						<?php echo substr($statusMessage,0,205);?>
							<?php if($statusMessageLength > 205): ?>
									...<a href="javascript:void(0);" onclick="readMore(<?php echo $pStatus['StatusID'];?>);">more</a>
							<?php endif; ?>
						</td>
						<td><?php echo date('d',strtotime($pStatus['DateCreated'])).'/'.date('m',strtotime($pStatus['DateCreated'])).'/'.date('Y',strtotime($pStatus['DateCreated']));?></td>
						<td><input  type="submit" style="display:none" id="submitApproveStatus<?php echo $pStatus['StatusID'];?>" onclick="approvePendingStatus(<?php echo $pStatus['StatusID'];?>);"/><a href="javascript:void(0)" onclick="clickSubmit('status',<?php echo $pStatus['StatusID'];?>)">Yes</a>&nbsp;|&nbsp;<a href="#no">No</a></td>
					</tr>	
					</form>
					<?php endforeach; ?>	
					<?php else: ?>
						<?php echo '<strong>No pending status!</strong>';?>
					<?php endif; ?>
				</tbody>
			</table>

	</div>
</div>