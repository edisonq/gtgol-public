<div class="container-fluid" style="margin-top: 60px;">
	<div class="well">
		<h3>Donations <a class="btn btn-primary btn-simple" href="<?php echo base_url();?>admin/download-donation" download="donation.csv">Download CSV</a></h3>

		<table class="table">
			<tr>
				<td>Donor ID</td>
				<td>Date Donated</td>
				<td>Donated Amount</td>
				<td>Donate Reason</td>
				<td>Benefactor</td>
				<td>Need Donated</td>
			<tr>
		<?php foreach($donations as $d):?>
			<tr>
				<td><?php echo $d->DonorID;?></td>
				<td><?php echo $d->DonateDate;?></td>
				<td><?php echo $d->DonatedAmount;?></td>
				<td><?php echo $d->DonateReason;?></td>
				<td><?php echo $d->Fullname;?></td>
				<td><?php echo $d->Title;?></td>
			<tr>
		<?php endforeach;?>
	</div>
</div>