<div class="form-signin">
			<h2 class="form-signin-heading">Please sign in</h2>
			<div id="errmessage"></div>
			<input type="text" id="uname" class="input-block-level username" placeholder="Username">
			<input type="password" id="pwd" class="input-block-level password" placeholder="Password">
			<label class="checkbox">
			<input type="checkbox" value="remember-me"> Remember me
			</label>
			<button id="btnlogin" class="btn btn-large btn-primary" type="submit">Sign in</button>
		</div>