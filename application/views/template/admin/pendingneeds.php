<?php 
		$CI =& get_instance();
		$CI->load->model('admincmsneed_model','acn');
?>

<script style="text/javascript" src="<?php echo base_url();?>script/adminapproval.js"></script>
<div class="container-fluid" style="margin-top: 60px;">
	<div class="well">
		<div class="navbar">
		<div class="navbar-inner">
		<a class="brand" href="#">Need Management</a>
		<ul class="nav">
		<li><a href="<?php echo base_url();?>admin/needs">All Needs</a></li>
		<li class="active"><a href="<?php echo base_url();?>admin/needs/pending">Pending Needs</a></li>
		<li><a href="<?php echo base_url();?>admin/needs/approved">Approved Needs</a></li>
		</ul>
		</div>
		</div>
			<table id="admin-table" class="table">
				<thead>
					<tr>
						<th><small>Need ID</small></th>
						<th><small>Serial Number</small></th>
						<th><small>Title</small></th>
						<th><small>Beneficiary</small></th>
						<th><small>Description</small></th>
						<th><small>Required funds</small></th>
						<th><small>Desription of funds need</small></th>
						<th><small>Date Needed</small></th>
						<th><small>Date Created</small></th>
						<th><small>Category</small></th>
						<th><small>Author</small></th>
						<th><small>Approve</small></th>
					</tr>
				</thead>
				<tbody>
					<?php 
		$ctr = 1;
		if(!empty($pendingNeeds)){
			foreach($pendingNeeds as $row){
			$DescriptionLength = strlen($row['Description']);
			$DescriptionFundsLength = strlen($row['DescriptionFunds']);
			$needDescription = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />',$row['Description']);
			$needDescriptionFunds = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />',$row['DescriptionFunds']);
		?>
		<form id="approveNeed<?php echo $row['NeedID'];?>" action="<?php echo base_url();?>admin-approval/pending-need?nid=<?php echo $row['NeedID'];?>" method="GET">
		<tr id="need<?php echo $row['NeedID'];?>">
			<td><?php echo $row['NeedID'];?></td>
			<td><small><?php echo $row['SerialNumber'];?></small></td>
			<td><?php echo $row['Title'];?></td>
			<td><?php echo $row['Beneficiary'];?></td>
			<td >
			<?php echo substr($needDescription,0,205);
				if($DescriptionLength > 205){ ?>
						...<a href="#needDesc<?php echo $row['NeedID'];?>" role="button" class="btn" data-toggle="modal">more</a>
			<?php } ?>
			<div style="padding: 20px; display: block; height: 400px; overflow-y: scroll;" id="needDesc<?php echo $row['NeedID'];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<h3>Description</h3><small><p style='color: red'>press esc key to close</p></small>
				<pre><?php echo $row['Description'];?></pre>
				<small><p style='color: red'>press esc key to close</p></small>
			</div>
			</td>
			<td><?php echo $row['RequiredFunds'];?></td>
			<td>
			<?php echo substr($needDescriptionFunds,0,205);
				if($DescriptionFundsLength > 205){ ?>
						...<a href="#needDescF<?php echo $row['NeedID'];?>" role="button" class="btn" data-toggle="modal">more</a>
			<?php } ?>
			<div style="padding: 20px; display: block; height: 400px; overflow-y: scroll;" id="needDescF<?php echo $row['NeedID'];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<h3>Description Funds</h3><small><p style='color: red'>press esc key to close</p></small>
				<pre><?php echo $row['DescriptionFunds'];?></pre>
				<small><p style='color: red'>press esc key to close</p></small>
			</div>
			</td>
			<td><?php echo date('d',strtotime($row['DateNeeded'])).'/'.date('m',strtotime($row['DateNeeded'])).'/'.date('Y',strtotime($row['DateNeeded']));?></td>
			<td><?php echo date('d',strtotime($row['DateCreated'])).'/'.date('m',strtotime($row['DateCreated'])).'/'.date('Y',strtotime($row['DateCreated']));?></td>
			<td><?php echo $row['CategoryName'];?></td>
			<td><a href="<?= base_url('admin/needs/author?author=' . $row['user_login']) ?>"><?= $row['user_login']; ?></a></td>
			<td>
				<input  type="submit" style="display:none" id="submitApprove<?php echo $row['NeedID'];?>" onclick="approvePendingNeeds(<?php echo $row['NeedID'];?>);"/><a href="javascript:void(0)" onclick="clickSubmit('need',<?php echo $row['NeedID'];?>)"><small>Yes</small></a>&nbsp;|&nbsp;
				<a href="<?php echo base_url();?>admin-approval/pending-need/decline?nid=<?php echo $row['NeedID'];?>" class="enter-message"><small>No</small></a>
			</td>
		</tr>	
		</form>
		<?php 
			}
		}else{
			echo '<strong>No pending needs!</strong>';
		}?>
				</tbody>
			</table>
	</div>
</div>
<script type="text/javascript">
$(function(){
	
	$('.enter-message').click(function(){
		url = $(this).attr('href');
		message = prompt("Reason for declining:","");
		if(message != null && message != ""){
			$.post(url, { message: message }, function(){ location.reload(); });			
			return false;
		} else {			
			if(confirm('You must provide a reason for declining, do You still want to decline the need?'))
				$(this).trigger('click');
			else
				return false;
		}
	});	
}); 
</script>