<script type="text/javascript">
	$(document).ready(
		function() {
			$("#IsActiveToggle").click(
				function() {
					if($('#IsActiveToggleV').val() == 1) {
						var answer = confirm("You really want to delete this need?");
						if (answer){
							
							$('#IsActiveToggle').is(":checked") ? $('#IsActiveToggleV').val(0): $('#IsActiveToggleV').val(1);
							alert("Need will be deleted during update");
						}
					}
					else 
						$('#IsActiveToggle').is(":checked") ? $('#IsActiveToggleV').val(0): $('#IsActiveToggleV').val(1);
				}
			);
			
		}
	);
</script>
<div class="container-fluid" style="margin-top: 60px;">
	<div class="well">
		<div id="admin-form">
			<h3>Update Need</h3>
			<form action="<?php echo base_url();?>admin/needs/update" method="post">
				<table>
					<tr>
						<td>Need ID: </td>
						<td>
							<strong><?php echo $row['NeedID'];?></strong>
							<input type="hidden" name="NeedID" value="<?php echo $row['NeedID'];?>">
						</td>
					</tr>
					<tr>
						<td>Title: </td>
						<td>
							<input type="text" name="Title" value="<?php echo $row['Title'];?>"/>
						</td>
					</tr>
					<tr>
						<td>Beneficiary: </td>
						<td>
							<input type="text" name="Beneficiary" value="<?php echo $row['Beneficiary'];?>"/>
						</td>
					</tr>
					<tr>
						<td>Description: </td>
						<td>
							<textarea name="Description"><?php echo $row['Description'];?></textarea>
						</td>
					</tr>
					<tr>
						<td>Required Funds: </td>
						<td>
							<input type="text" name="RequiredFunds" value="<?php echo $row['RequiredFunds'];?>"/>
						</td>
					</tr>
					<tr>
						<td>Fund Description: </td>
						<td>
							<textarea name="DescriptionFunds"><?php echo $row['DescriptionFunds'];?></textarea>
						</td>
					</tr>
					<tr>
						<td>Deadline: </td>
						<td>
							<input type="text" name="DateNeeded" value="<?php echo date('m/d/y',strtotime($row['DateNeeded']));?>"/>
							
						</td>
					</tr>
					<tr>
						<td>Deleted: </td>
						<td>
							<input type="checkbox" id="IsActiveToggle">
							<input type="hidden" id="IsActiveToggleV" name="IsActive" value="1" />
						</td>
					</tr>
					<tr>
						<td>Category: </td>
						<td>
							<select name="CategoryID">
								<option selected="selected" value="<?php echo $row['CategoryID'];?>"><?php echo $row['CategoryName'];?></option>
								<?php foreach($ctgry as $c):?>
								<option value="<?php echo $c['CategoryID'];?>"><?php echo $c['CategoryName'];?></option>
								<?php endforeach;?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Created by:  </td>
						<td>
							<strong><?php echo $row['user_login'];?></strong>
						</td>
					</tr>
				</table>
				<input type="submit" value="Update" class="btn btn-primary"/>
				
			</form>
			
		</div>
	</div>
</div>