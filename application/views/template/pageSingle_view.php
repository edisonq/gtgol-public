<link rel='stylesheet' href="<?php echo base_url();?>style/cms.css" type="text/css" media="screen" />
<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
	    	&nbsp;
	    </div>
     	<p>&nbsp;</p>
     	<div class="breadlink"><i><a href="<?php echo base_url();?>admin/pages">< back</a></i></div>
		<div id="fillform">
			<h2><?php echo $page['PageTitle'];?></h2>
			<fieldset class="form">
				 <legend>Details</legend>
				 <p>
				 	<label>
				 		ID: <strong><?php echo $page['PageID'];?></strong>
				 	</label>
				 </p>
				 <p>
				 	<label>
				 		Permalink: <strong><?php echo base_url()."".$page['PageName'];?></strong>
				 	</label>			 	
				 </p>
				 <p>
				 	<label>
				 		Title: <strong><?php echo $page['PageTitle'];?></strong>
				 	</label>	 			 	
				 </p>
				 <p>
				 	<label>
				 		Description: <strong><?php echo $page['PageDescription'];?></strong>
				 	</label> 
				 </p>
				 <p>
				 	<label>
				 		Status: <strong><?php if($page['IsActive']){ echo "Live"; }else{ echo "Draft"; }?></strong>
				 	</label>	 	
				 </p>
				 <p>
				 	<label>
				 		Page Created: <strong><?php echo date('F d, Y', strtotime($page['DateCreated']));?></strong>
				 	</label> 	
				 </p>
				 <p>
				 	<label>
				 		Page Modified: <strong><?php echo date('F d, Y', strtotime($page['DateModified']));?></strong>		
				 	</label>		 	
				 </p>
				 <p>
				 	<label>
				 		<a class="opt-button" href="<?php echo base_url();?>admin/pages/edit?pid=<?php echo $page['PageID'];?>">Edit</a>&nbsp;
				 		<a class="opt-button" href="<?php echo base_url();?>admin/pages/delete?pid=<?php echo $page['PageID'];?>">Delete</a>
				 	</label>
				 </p>
			</fieldset><br />
			
		</div>
		<p>&nbsp;</p>
	</div>
</div>