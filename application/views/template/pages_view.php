<div class="container-fluid" style="margin-top: 60px;">
  	<div class="row-fluid">
  		<div class="breadlink"><i><a href="<?php echo base_url();?>admin/dashboard">< back</a></i></div>
		<div class="span4">
			<p>&nbsp;</p>
			<div class="well sidebar-nav">
				<ul class="nav nav-list">
					<li class="nav-header"><strong style="font-size: 16px;">Pages</strong><a style="float:right;" href="<?php echo base_url();?>admin/pages/create"> Add Page</a></li>
					<?php foreach($pages as $row){?>
						<li><a href="<?php echo base_url();?>admin/pages/show?pid=<?php echo $row['PageID'];?>"><?php echo $row['PageTitle'];?></a></li>
					<?php }?>
				</ul>
			</div>
		</div>
	</div>
</div>

