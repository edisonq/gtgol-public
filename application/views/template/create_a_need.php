<!DOCTYPE HTML>
<html>
<head>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<meta charset="utf-8">
<title>Gift of Giving</title>
<link href='http://fonts.googleapis.com/css?family=Crete+Round:400,400italic' rel='stylesheet' type='text/css'>
<link rel='stylesheet' href="style/main.css" type="text/css" media="screen" />
<link rel='stylesheet' href="style/completed_needs.css" type="text/css" media="screen" />
<link rel='stylesheet' href="style/addtional-main.css" type="text/css" media="screen" />
<link rel='stylesheet' href="style/login.css" type="text/css" media="screen" />
<link rel='stylesheet' href="style/create_a_need.css" type="text/css" media="screen" />
<script type='text/javascript' src='script/jquery-1.8.2.min.js'></script>
<script type='text/javascript' src='script/main.js'></script>
</head>

<body>
<header>
  <div id="headerdark">
    	<div id="headerdarkContent"> </div>
 </div>
  <div id="headerLight">
        <span id="ribbonleft">&nbsp;</span>
        <a id="logo" href="" title="Give the Gift of Liberty">&nbsp;</a>
        <span id="ribbon">&nbsp;</span>
        <div id="upperRight">
        	<a href="">Student Login</a> | <a href="">Create Account</a>
             <form style="margin-top: 7px;" ><a href="" id="facebook" class="socialButton">&nbsp;</a>
            <a href="" id="twitter" class="socialButton">&nbsp;</a>
            <a href="" id="googleplus" class="socialButton">&nbsp;</a>
         <input value="Search" id="websiteSearchinput" type="text" class="searchField" /><input id="searchButton" name="searchGo" type="submit" value="Go!"></form>
            
        </div>
        <div id="linkContainer">
        	<ul>
           	  <li><a href="" class="withoutborder">Home</a></li>
              <li><a href="" class="withborder" >How it Works</a></li>
              <li><a href="" class="withborder">Why Give</a></li>
              <li><a href="" class="withborder">Student Needs</a></li>
              <li><a href="" class="withborder">Success Stories</a></li>
              <li><a href="" class="withborder">About SFL</a></li>
            </ul>
        </div>
  </div>
</header>
<div id="contentArea">
  <div id="content">
    <div class="socialMedia socialMedia--2">
      <ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul>
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAside--2">
        <div class="advertise">
          <h1>Ad Goes Here 200 x ...</h1>
        </div>
        <div class="searchBtn searchBtn--2">
          <p><a href="#">Search Needs</a></p>
        </div>
        <div class="myNeeds">
          <h1>My Needs</h1>
          <ul>
            <li class="active"><a href="#">Create a Need</a></li>
            <li><a href="#">Completed Needs</a></li>
            <li><a href="#">Log Out</a></li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="advertise advertise--2">
          <h1>Ad Goes Here 200 x ...</h1>
        </div>
        <div class="advertise advertise--2">
          <h1>Ad Goes Here 200 x ...</h1>
        </div>
        <div class="advertise advertise--2">
          <h1>Ad Goes Here 200 x ...</h1>
        </div>                    
      </aside> 
      <section id="createNeeds">
        <h1>Welcome to your Give the Give of Liberty Account!</h1>
        <span>Get started by filling out this simple form to create your Need.</span>

        <div class="box boxCneeds">
            <h2>Title of Project:</h2>
            <input type="text" name="name">
            <div class="clear"></div>
            <h2>Student/Organization to Benefit:</h2>
            <input type="text" name="name">            
            <div class="clear"></div>
            <h2>Description of Need:</h2>
            <textarea name="desc"></textarea>
            <div class="clear"></div>            
            <h2>Target Goal:</h2>
            <input class="targetGoal" type="text" placeholder="$" name="name">
            <div class="clear"></div>
            <h2>Description of what funds will go towards (i.e. budget):</h2>
            <textarea class="descFunds" name="Address"></textarea>
            <div class="clear"></div> 
            <h2>Date funds are needed by:</h2>
            <input class="monthDay" type="text" placeholder="mm/dd" name="name">
            <select class="selectYear">
              <option value="volvo">yyyy</option>
              <option value="saab">Select 1</option>
              <option value="opel">Select 2</option>
            </select>  
            <div class="clear"></div>    
            <h2>Need Type:</h2>
            <select class="selectNeed">
              <option value="volvo">Select a need type</option>
              <option value="saab">Select 1</option>
              <option value="opel">Select 2</option>
            </select>  
            <div class="clear"></div>
            <div class="uploadPhoto">
              <h2>Upload a Cover Photo:</h2>     
              <form method="post" enctype="multipart/form-data">
                <input type="file" name="imgfile"><br>
              </form>  
            </div>
            <div class="clear"></div>
            <h2 class="t2">Tags/Keywords:</h2>
            <span class="tagsKeyword">Type in keywords that relate to your Need (e.g. scholarship, travel, books, etc...) use commas to separate tags.</span>
            <input class="inputTags" type="text" name="name"> 
            <a href="#" class="submitNeedBtn">Submit Need</a>
            <div class="clear"></div>         
        </div>
            
      </section>
      <div class="clear"></div>       
    </section>

  </div>
</div>
<footer>
	<div id="footerContent">
    <span id="footerRibbon">&nbsp;</span>
    
	<div id="footerLink">
    	<ul>
          <li><a href="">Home</a></li>
          <li><a href="" class="footerLinkLine">How it Works</a></li>
          <li><a href="" class="footerLinkLine">Why Give</a></li>
          <li><a href="" class="footerLinkLine">Student Needs</a></li>
          <li><a href="" class="footerLinkLine">Success Stories</a></li>
          <li><a href="" class="footerLinkLine">About SFL</a></li>
          <li><a href="" class="footerLinkLine">Student Login</a></li>
          <li><a href="" class="footerLinkLine">Site Map</a></li>
        </ul>
    </div>
    <div id="footerColumn1">
    	<a href="" id="footerLogo">&nbsp;</a>
        <p>
        <a href="">Privacy Policy</a>
    	| <a href="">Terms of Service</a>
        </p>
    </div>
    <div id="footerColumn2">
   	  <img src="images/Gift_of_Giving.gif" width="119" height="119" alt="Give the Gift of Liberty"></div>
    <div id="footerColumn3">
    	<p>Copyright 2012.  Students For Liberty
    	<br>All Rights Reserved</p>
        <p>Web design by <a href="">Market Aces</a><p>
    </div>
    </div>
    <div id="footerDark">
    </div>
    
</footer>
</body>
</html>