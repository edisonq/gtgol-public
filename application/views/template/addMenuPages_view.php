<link rel='stylesheet' href="<?php echo base_url();?>style/cms.css" type="text/css" media="screen" />
<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
	    	&nbsp;
	   </div>
     	<p>&nbsp;</p>
		<div id="fillform">
			<h3>Add Pages to <?php echo $smenu['MenuName'];?></h3>
			
			<form action="<?php echo base_url();?>admin/menu/pages/save" method="post">
				<p>
					<label>
						Pages</span>
						<select name="PageID">
							<option>- Select -</option>
							<?php foreach($pages as $row){?>
								<option value="<?php echo $row['PageID'];?>"><?php echo $row['PageName'];?></option>
							<?php }?>
						</select>
						<input type="hidden" name="MenuID" value="<?php echo $smenu['MenuID'];?>"/>
					</label>
				</p>
				<p>
					<label>
						<input type="submit" value="Save" id="fillformbutton"/>&nbsp;
						<a href="<?php echo base_url();?>admin/menu">Cancel</a>
					</label>
				</p>
			</form>
		</div>
	</div>
</div>