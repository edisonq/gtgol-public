<link href="<?php echo base_url(); ?>style/admin.css" rel="stylesheet" type="text/css"/>
<div id="wrapper">
	<a href="<?= base_url('admin/needs'); ?>">back</a>
	<div id="admin-form">
		<h3>User Information</h3>

		<div class="form_row">
			<span class="label">User Login: </span>
			<?php echo $author['user_login']; ?>
		</div>
		<div class="form_row">
			<span class="label">User Email: </span>
			<a href="mailto:<?php echo $author['user_email']; ?>" target="_blank"><?php echo $author['user_email']; ?></a>
		</div>
		<div class="form_row">
			<span class="label">Registration Date: </span>
			<?php echo $author['user_registered']; ?>
		</div>		
	</div>
</div>