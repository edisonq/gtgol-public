<link rel='stylesheet' href="<?php echo base_url();?>style/cms.css" type="text/css" media="screen" />
<div class="container-fluid" style="margin-top: 60px;">
  	<div class="row-fluid">
     	<p>&nbsp;</p>
     	<div class="breadlink"><i><a href="<?php echo base_url();?>admin/dashboard">< back</a></i></div>
		<div id="menu">
			<div id="menu-header">
				<strong>Menus</strong>
				<a class="add-button" href="<?php echo base_url();?>admin/menu/create">Add Menu</a>
			</div>
			<div id="menu-body">
				<?php foreach($menu as $row){?>
					<fieldset>
						<legend><?php echo $row['MenuName'];?></legend>
						<div style="clear:both;"></div>
						<div>
							<a class="add-button" href="<?php echo base_url();?>admin/menu/delete?mid=<?php echo $row['MenuID'];?>">Delete</a>
							<a class="add-button" href="<?php echo base_url();?>admin/menu/pages/create?mid=<?php echo $row['MenuID'];?>">Add Pages</a>
						</div>				
						<ul class="nav nav-list">
							<li>Home <a class="add-button" href="javascript:void(0)">Default</a></li>
							<li>Student Needs <a class="add-button" href="javascript:void(0)">Default</a></li>
							<li>Success Stories <a class="add-button" href="javascript:void(0)">Default</a></li>
							<?php 
								$CI =& get_instance();
								$CI->load->model('adminCMS_model','cms');
								$menu_pages = $CI->cms->getMenuPages($row['MenuID']);
								foreach($menu_pages as $mp){
							?>
							<li><?php echo $mp['PageTitle'];?><a class="add-button" href="<?php echo base_url();?>admin/menu/pages/remove?id=<?php echo $mp['PageHasMenu'];?>">Remove Page</a></li>
							<?php }?>
						</ul>
					</fieldset>
				<?php }?>
			</div>		
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
		 