<link rel='stylesheet' href="<?php echo base_url(); ?>style/success-stories.css" type="text/css" media="screen" />
<div id="contentArea">
	<div id="socialSpace">
		&nbsp;
    </div>
	<div id="content">
		<div id="contentHeader">
			<h1>Success Stories</h1>
		</div>
		<div class="clear"></div>
		<section class="mainContent">
			

				<?php $CI = & get_instance(); ?>
				<?php $this->load->view('layouts/search_form') ?>

				
			<section id="pageSection">
				<?php
				if (!empty($stories)) {
					foreach ($stories as $row) {
						$DescriptionLength = strlen($row['thankYouLetter']);
						$ssDescription = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/', '<br />', $row['thankYouLetter']);
						?>
						<input id="description<?php echo $row['StoryID']; ?>" type="hidden" value="<?php echo $ssDescription; ?>"/>
						<a href="<?php echo base_url(); ?>student-needs/view/<?php echo $row['NeedID']; ?>">
							<div class="page-box">
								<div class="page-box-image">
									<img src="<?php echo base_url(); ?>images/success-stories/primary-photo/<?php echo $row['MediaURL']; ?>"/>	        				
								</div>
								<div class="page-box-content1">
									<?php echo $row['Beneficiary']; ?><br />
									<?php echo $row['Title']; ?><br />
									Raised: <strong style="color: #333;"><strike>S</strike><?php echo number_format($row['RequiredFunds']); ?></strong>
								</div>
								<div class="page-box-content2">		        				
									<span id="ssDescription<?php echo $row['StoryID']; ?>">
										<?php echo "<span style='color: #666;font-family: arial;'>" . substr($ssDescription, 0, 50) . "</span>";
										if ($DescriptionLength > 50) {
											?>
											...<a href="<?php echo base_url(); ?>student-needs/view/<?php echo $row['NeedID']; ?>">more</a>										
										<?php } ?>									
									</span>
								</div>
							</div>
						</a>        		
					<?php
					}
				} else {
					echo "No success stories yet.";
				}
				?>
				<div class="clear"></div>
<?php if ($cntsstories > 12): ?>
					<div id="pagination">
						<ul><?php if($cntsstories%12==0):?>
								<?php $numPage = $cntsstories / 12;?>
							<?php else:?>
								<?php $numPage = ($cntsstories / 12) + 1;?>
							<?php endif;?>
							<?php
							$a = 12;
							$offset = ($numPage - 1) * 12;
							$nextOffset = $pagenum * 12;
							$previousOffset = ($pagenum - 2) * 12;
							$nextPage = $pagenum + 1;
							$previousPage = $pagenum - 1;
							?>
							<?php if ($pagenum < 12): ?>
								<?php if ($numPage > 12): ?>
									<?php $b = 12; ?>
								<?php else: ?>
									<?php $b = $numPage; ?>
								<?php endif; ?>
								<?php for ($i = 1; $i <= $b; $i++): ?>
									<?php $o = ($i - 1) * 12; ?>
									<?php if ($i == 1): ?>
										<?php $href = base_url() . "success-stories"; ?>
									<?php else: ?>
										<?php $href = base_url() . "success-stories/stories?page=" . $i . "&offset=" . $o; ?>
									<?php endif; ?>
									<li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo $href; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $i; ?></a></li>
								<?php endfor; ?>
								<?php if ($numPage > 13 && $pagenum != ($numPage - 12) && $pagenum != ($numPage - 1) && $pagenum != $numPage): ?>
									...<li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo base_url(); ?>success-stories/stories?page=<?php echo $numPage; ?>&offset=<?php echo $offset; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $numPage; ?></a></li>	&nbsp;&nbsp;
								<?php endif; ?>
							<?php elseif ($pagenum % 12 == 0 || $pagenum > 12): ?>
								<?php $a+=12; ?>
								<?php for ($i = 1; $i <= $a; $i++): ?>
									<?php $o = ($i - 1) * 12; ?>
									<?php if ($i == 1): ?>
										<?php $href = base_url() . "success-storiess"; ?>
									<?php else: ?>
										<?php $href = base_url() . "success-stories/stories?page=" . $i . "&offset=" . $o; ?>
									<?php endif; ?>
									<li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo $href; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $i; ?></a></li>
								<?php endfor; ?>
								<?php if ($numPage > 12 && $pagenum != ($numPage - 12) && $pagenum != ($numPage - 1) && $pagenum != $numPage): ?>
									...<li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo base_url(); ?>success-stories/stories?page=<?php echo $numPage; ?>&offset=<?php echo $offset; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $numPage; ?></a></li>	&nbsp;&nbsp;
								<?php endif; ?>
							<?php endif; ?>
							<?php if ($pagenum > 1): ?>
								<?php if ($pagenum == 2): ?>
									<?php $href1 = base_url() . "success-stories"; ?>
								<?php else: ?>
									<?php $href1 = base_url() . "success-stories/stories?page=" . $previousPage . "&offset=" . $previousOffset; ?>
								<?php endif; ?>
								<a id="previous" href="<?php echo $href1; ?>">< Previous </a>&nbsp;
							<?php endif; ?>
							<?php if ($pagenum < ($numPage - 1) && $pagenum > 1): ?>
								<a id="next" href="<?php echo base_url(); ?>success-stories/stories?page=<?php echo $nextPage; ?>&offset=<?php echo $nextOffset; ?>">Next ></a>
	<?php endif; ?>
						</ul>

					</div>  
<?php endif; ?>   
				<div class="clear"></div>      
			</section>
			<div class="clear"></div>
		</section>
		<p>&nbsp;</p>
	</div>
</div>
<script type="text/javascript">
	$('.search-needs').click(function(){
		$('.submit').click();
	});
</script>