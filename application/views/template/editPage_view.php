<link rel='stylesheet' href="<?php echo base_url();?>style/cms.css" type="text/css" media="screen" />
<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
	   		<p>&nbsp;</p>
	    </div>
		<div id="fillform">
			<h2><?php echo $page['PageTitle'];?></h2>
			<form action="<?php echo base_url();?>admin/pages/update" method="post">
				<p>
					<label>
						Title: <br />
						<input type="hidden" name="PageID" value="<?php echo $page['PageID'];?>"/>
						<input type="text" name="PageTitle" class="formtext" value="<?php echo $page['PageTitle'];?>"/>
					</label>
				</p>				
				<p>
					<label>
						Permalink: 
						<?php echo base_url();?><input type="text" name="PageName" class="formtext" value="<?php echo $page['PageName'];?>">
					</label>
				</p>			
				<p>
					<label>
						Description: <br />
						<textarea name="PageDescription" class="formtextarea"><?php echo $page['PageDescription'];?></textarea>
					</label>
				</p>			
				<p>
					<label>
						Status: <br />
						<select name="IsActive">
					 		<option value="<?php echo $page['IsActive'];?>" selected="selected"><?php if($page['IsActive']){ echo "Live"; }else{ echo "Draft"; }?></option>
					 	</select>	
					</label>
				</p>					
				<p>
					<label>
						HTML Content: <br />
						<textarea name="PageHTMLContent" class="htmlcontent"><?php echo $page['PageHTMLContent'];?></textarea>
					</label>
				</p>			
				<p>
					<label>
						<span>&nbsp;</span>
						<input type="submit" value="Update" id="fillformbutton"/>	
						<a href="<?php echo base_url();?>admin/pages/show?pid=<?php echo $this->input->get('pid');?>">Cancel</a>
					</label> 
				</p>
			</form>
		</div>
		<p>&nbsp;</p>
	</div>
</div>