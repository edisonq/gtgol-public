<link rel='stylesheet' href="<?php echo base_url();?>style/cms.css" type="text/css" media="screen" />
<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
	    	&nbsp;
	    </div>
     	<p>&nbsp;</p>
		<div id="fillform">
			<h3>Add/Edit Menu</h3>
			
			<form action="<?php echo base_url();?>admin/group/menu/update" method="post">
				<p>
					<label>
						Menus
						<select name="MenuID">
							<option>- Select -</option>
							<?php foreach($menu as $row){?>
								<option value="<?php echo $row['MenuID'];?>"><?php echo $row['MenuName'];?></option>
							<?php }?>
						</select>
						<input type="hidden" name="MenuType" value="<?php echo $this->input->get('group'); ?>"
					</label>
				</p>
				<p>
					<label>
						<span>&nbsp;</span>
						<input type="submit" value="Save" id="fillformbutton"/>
						<a href="<?php echo base_url();?>admin/group">Cancel</a>
					</label>
				</p>
			</form>
		</div>
	</div>
</div>