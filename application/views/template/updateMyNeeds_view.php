
<br />
<div id="contentArea">
  <div id="content">
    <div class="socialMedia socialMedia--2">
      <!--ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul-->
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAside--2">
        <div class="advertise1">
			<h1><a href="http://studentsforliberty.org/webinar-program/" target="_blank"><img src="<?php echo base_url('images/ads/1.jpg') ?>" alt="" /></a></h1>
		</div>        
        <div class="myNeeds">
          <ul>
          	<li  class="active"><a href="<?php echo base_url();?>my-account/my-needs">My Needs</a></li>
            <li><a href="<?php echo base_url();?>my-account/create-need">Create a Need</a></li>
            <li><a href="<?php echo base_url();?>my-account/completed-need">Completed Needs</a></li>
            <li><a href="#">Log Out</a></li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/tabling-kit-request/" target="_blank"><img src="<?php echo base_url('images/ads/2.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-international-sfl-conference/" target="_blank"><img src="<?php echo base_url('images/ads/3.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-european-students-for-liberty-conference/" target="_blank"><img src="<?php echo base_url('images/ads/4.jpg') ?>" alt="" /></a></h1>
		</div>                    
      </aside> 
      <section id="createNeeds">
        <h1>Update Need</h1>
        <span>Fill out this simple form to update your Need.</span>
		<?php 
				function strReplace($data){
					$result = str_replace("\r\n", "\n",$data);
					return $result;
				}
		
				$thumbnails = $this->my_needs->getThumbnails($need['NeedID']);
		?>
        <div class="box boxCneeds">
        	<form action="<?php echo base_url();?>my-needs/update" method="post" id="update-need" enctype="multipart/form-data">
	            <h2>Title of Project:</h2>
	           	<input type="hidden" name="NeedID" value="<?php echo $this->input->get('nid');?>"/>
	            <input type="text" name="Title" value="<?php echo $need['Title'];?>"/>
	            <div class="clear"></div>
	            <h2>Student/Organization to Benefit:</h2>
	            <input type="text" name="Beneficiary" value="<?php echo $need['Beneficiary'];?>"/>            
	            <div class="clear"></div>
	            <h2>Description of Need:</h2>
	            <textarea name="Description" ><?php echo strReplace($need['Description']);?></textarea>
	            <div class="clear"></div>            
	            <h2>Target Goal:</h2>
	            <input class="targetGoal" type="text" placeholder="$" name="RequiredFunds" onkeypress="return isNumberKey(event)" value="<?php echo $need['RequiredFunds'];?>"/>
	            <div class="clear"></div>
	            <h2>Description of what funds will go towards (i.e. budget):</h2>
	            <textarea class="descFunds" name="DescriptionFunds"><?php echo strReplace($need['DescriptionFunds']);?></textarea>
	            <div class="clear"></div> 
	            <h2>Date funds are needed by:</h2>
	            <input class="monthDay" type="text" placeholder="mm/dd/yy" name="DateNeeded" id="datepicker" value="<?php echo date('m',strtotime($need['DateCreated'])).'/'.date('d',strtotime($need['DateCreated'])).'/'.date('Y',strtotime($need['DateCreated']));?>"/>
				<div class="clear"></div>    
	            <h2>Need Type:</h2>
	            <select class="selectNeed" name="CategoryID">
	              <?php foreach($categories as $row){
	              		if($row['CategoryID']==$need['CategoryID']){
	              	?>
		              <option selected="selected" value="<?php echo $row['CategoryID'];?>"><?php echo $row['CategoryName'];?></option>
		          <?php }else{ ?>
		          	<option value="<?php echo $row['CategoryID'];?>"><?php echo $row['CategoryName'];?></option>
				<?php 
		          	} 
				  }
		          ?>
	            </select>  
	            <div class="clear"></div>
	            <div class="uploadPhoto">
	              <h2>Upload a Cover Photo:</h2>     
	              	<input type="hidden" name="MediaID" value="<?php echo $pic[0]->MediaID; ?>"/>
	                <input type="file" name="userfile"/><br>
	            </div>
	            <div class="banner-photo">
	            	<img src="<?php echo base_url();?>images/needs/banners/<?php echo $pic[0]->MediaName; ?>"/>
	            </div>
	            <br />
	            <h2 class="t2">Tags/Keywords:</h2>
				<div id="tag-cloud">
					<?php foreach($tags as $tag){ ?>
							<span class="<?php echo $tag['TagID'];?>"><strong class="name-tag"><?php echo $tag['ggol_tagcol'] ?></strong><strong class='delete-tag' name="<?php echo $tag['TagID'];?>">X</strong></span>
					<?php } ?>				
				</div>
				<a href="javascript:void(0);" class="add-more-tags">Add more tags?</a>
				<br />   
				<div id="add-tag">
					<span class="tagsKeyword">Type in keywords that relate to your Need (e.g. scholarship, travel, books, etc...) use commas to separate tags.</span>  
					<input class="inputTags" type="text" name="ggol_tagcol"/> 
				</div>           
	            <div class="clear"></div>
	            <a href="javascript:void(0);" class="submitNeedBtn">Update Need</a>
	            <span id="uploader" style="color: #222;"></span>
	            <input type="hidden" value="<?php echo $this->session->userdata('ID'); ?>" id="UserId" name="UserId" />     
	            <input type="submit" value="Submit Need" style="display:none;" class="submit"/>        
	            <div class="clear"></div>   
	            
            </form>    
        </div>
            
      </section>
      <div class="clear"></div>       
    </section>
  </div>
</div>
<script type="text/javascript">
	$("#datepicker").datepicker({
    	  changeMonth: true,
          changeYear: true,
          yearRange: "-5:+25",
    });
</script>