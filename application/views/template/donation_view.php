<br />
<script type="text/javascript">
	$(document).ready(
		function() {
			jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
				phone_number = phone_number.replace(/\s+/g, ""); 
				return this.optional(element) || phone_number.length > 9 &&
					phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
			}, "Please specify a valid phone number");
		}
	);
</script>
<div id="contentArea">
	<div id="wait-ui">
	<p>Please wait while we process your transaction...</p>
	<img src="<?php echo base_url(); ?>images/ajax-loader-2.gif" />
	</div>
	<div id="modal-ui">
		<div class="top">
			<div class="logo"></div>
			<div class="close"></div>
		</div>
		<div class="bot">
			<h1>Your Donation Has Been Submitted!</h1>
			<p>You have successfully Donated to a Need! Thank you very much! A confirmation and a receipt of your donation has been emailed to you.</p>
			<p class="par2"><a href="<?php echo base_url();?>student-needs">Review More Student Needs</a> | <a href="<?php echo base_url();?>">Go to SFL's Main Website</a></p>
		</div>
		<div class="bot2">
			<h1 style="color: red;">Transaction Rejected</h1>
			<p>It seems that our payment system declined your donation request due to invalid payment information, please check your payment information and try again.</p>
			<p class="par2"><a href="<?php echo base_url();?>student-needs">Review More Student Needs</a> | <a href="<?php echo base_url();?>">Go to SFL's Main Website</a></p>
		</div>
	</div>
	<div id="dialog-ui">
		
	</div>
  <div id="content">
    <div class="socialMedia socialMedia--2">&nbsp;
      <!--ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul-->
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAsideDonation">
        <p><span style="color: #cf8428;">Thank you for your donation!</span> Every penny counts. Once you fill out your payment information, your credit card will be charged immediately  for the specified donation amount, upon confirmation.</p>
        <a href="#"><img class="cardsList" src="<?php echo base_url();?>images/img-cards.jpg" alt="Give the Gift of Liberty"></a>
      </aside> 
      <section id="createNeeds">
        <h1>Thank You For Your Support!</h1>
        <span>Please fill out this form to complete your donation proccess.</span>
        <h2><?php echo $title;?></h1>
		
        <div class="clear"></div>
        <div class="box boxDonation">
			<div><span class='raised'>Amount Raised: $<?php echo ($need->TotalDonation > 0)? $need->TotalDonation: 0;?></span><span class='needed'>Amount Needed: $<?php echo ($need->RequiredFunds > 0)? $need->RequiredFunds: 0;?></span></div>
       	  <form action="<?php echo base_url(); ?>student-needs/donation/save/1" method="post" id="frmDonate">
                            <h2>Donation Amount:<label for="DonatedAmount" generated="true" class="error"></label></h2>
                            <input class="donation number required" type="text" name="DonatedAmount" placeholder="$" value="<?php echo $amount; ?>" >
                            <input type="hidden" name="NeedID" value="<?php echo $this->uri->segment(3); ?>"/>
                            <div class="clear"></div>
                            <h2>First Name*:<label for="FirstName" generated="true" class="error"></label></h2>
                            <input class="name required" type="text" name="FirstName" minlength="2">
                            <div class="clear"></div>
                            <h2>Last Name*:<label for="LastName" generated="true" class="error"></label></h2>
                            <input class="name required" type="text" name="LastName" minlength="2">
                            <div class="clear"></div>
							<div class="clear"></div>
                            <h2>Phone Number*:<label for="PhoneNumber" generated="true" class="error"></label></h2>
                            <input class="phoneUS required" type="text" name="PhoneNumber" minlength="2">
                            <div class="clear"></div>                    
                            <h2>Email Address*:<label for="EmailAddress" generated="true" class="error"></label></h2>
                            <input class="email required" type="text" name="EmailAddress">
                            <div class="clear"></div>          
                            <h2>Billing Address*:<label for="BillingAddress1" generated="true" class="error"></label></h2>
                            <input class="billing required" type="text" name="BillingAddress1">
                            <input class="billing" type="text" name="BillingAddress2">
                            <div class="clear"></div>   
                            <div class="cityArea">
                                <h2>City*:<label for="City" generated="true" class="error"></label></h2>
                                <input class="city required" type="text" name="City">
                            </div>
                            <div class="stateArea">
                                <h2>State*:<label for="State" generated="true" class="error error2"></label></h2>
                                <input class="state required" type="text" name="State">
                            </div>
                            <div class="zipArea">
                                <h2>Zip*:<label for="Zip" generated="true" class="error error3"></label></h2>
                                <input class="zip required digits" type="text" name="Zip">
                            </div>          
                            <div class="clear"></div>
                            <h2>Credit Card Information*:<label for="CreditCardInfo" generated="true" class="error"></label></h2>
                            <select class="ccInfo" name="CreditCardInfo">
                                <option value="visa">Visa</option>
                                <option value="mastercard">Mastercard</option>
                                <option value="discover">Discover</option>
                                <option value="amex">American Express</option>
                            </select>  
                            <div class="clear"></div>   
                            <div class="cardNumberArea">
                                <h2>Card Number*:<label for="CardNumber" generated="true" class="error"></label></h2>
                                <input class="cardNumber required creditcard" type="text" name="CardNumber">
                            </div>
                            <div class="expDateArea">
                                <h2>Exp. Date*:</h2>
                                <label for="Month" generated="true" class="error error4"></label><label for="Year" generated="true" class="error error5"> </label>
                                <select class="ccInfo expDate required" name="Month" >
                                    <option value="">mm</option>
                                    <?php for ($i = 1; $i <= 12; $i++): ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>

                                <select class="ccInfo expYear required" name="Year">
                                    <option value="">yyyy</option>
                                    <?php for ($i = date('Y'); $i <= date('Y')+10; $i++): ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>                
                            </div> 
                            <div class="cvCodeArea">
                                <h2>CV Code*:<label for="Code" generated="true" class="error error6"></label></h2>
                                <input class="cvCode required" minlength="3" type="text" name="Code">
                            </div>                   
                            <div class="clear"></div>        
                            <h2>Why I Gave <span style="font: 14px 'Helvetica', serif;">(optional):</span></h2>
                            <textarea class="descFunds" name="DonationReason"></textarea>
                            <div class="clear"></div>
                            <div>
                                <input type="checkbox" name="ispub" checked id="ispublish" class="normal">
                                <label for="ispublish" style="font: 14px 'Helvetica', serif;position: absolute;color: #555555;margin-left: 20px">Publish name on needs page</label>
                            </div>
                            <div class="clear"></div>
                            <a href="javascript:void(0);" class="submitNeedBtn">Checkout</a>
                            <input type="submit" class="submit" style="display:none;"/>
                            <div class="clear"></div>
                        </form>

        </div>
      </section>
      <div class="clear"></div>       
    </section>

  </div>
</div>
