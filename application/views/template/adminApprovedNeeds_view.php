<link href="<?php echo base_url();?>style/admin.css" rel="stylesheet" type="text/css"/>
<div id="wrapper">
	<a href="<?php echo base_url();?>/admin-approval/dashboard/needs">back</a>
	<h3>Approved Needs</h3>
	<?php if(!empty($approve)):?>
	<table id="admin-table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Serial Number</th>
				<th>Title</th>
				<th>Beneficiary</th>
				<th>Description</th>
				<th>Required Funds</th>
				<th>Fund Description</th>
				<th>Deadline</th>
				<th>Date Created</th>
				<th>Category</th>
				<th>Created by</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$ctr = 1;
				foreach($approve as $row):?>
				<tr>
					<td><?php echo $ctr++;?></td>
					<td><?php echo $row['SerialNumber'];?></td>
					<td><?php echo $row['Title'];?></td>
					<td><?php echo $row['Beneficiary'];?></td>
					<td><?php echo $row['Description'];?></td>
					<td>$ <?php echo number_format($row['RequiredFunds']);?></td>
					<td><?php echo $row['DescriptionFunds'];?></td>
					<td><?php echo date('m/d/y',strtotime($row['DateNeeded']));?></td>
					<td><?php echo date('m/d/y',strtotime($row['DateCreated']));?></td>
					<td><?php echo $row['CategoryName'];?></td>
					<td><?php echo $row['user_login'];?></td>
					<td><?php 
						if($row['IsApproved'] == 0):
							echo "Pending";
						else:
							echo "Active";
						endif;
						?>
					</td>
					<td></td>
				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
	<?php
			else:
				echo "<h4>No Approved Needs Available</h4>";
		endif;
	?>
</div>