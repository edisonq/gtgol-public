<?php
	$thumb_width = "120";						
	$thumb_height = "88";	

	if(!empty($row['MediaURL'])){
		$large_image_location = BASEPATH."../images/needs/banners/".$row['MediaURL'];
	}else{
		$large_image_location = "";
	}
	
	function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}

	function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}
	
	$current_large_image_width = getWidth($large_image_location);
	$current_large_image_height = getHeight($large_image_location);
	
?>
<script type="text/javascript">
function preview(img,selection){ 
	var scaleX = <?php echo $thumb_width;?> / selection.width; 
	var scaleY = <?php echo $thumb_height;?> / selection.height; 
	
	$('#prev').css({ 
		width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
		height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
	});
	
	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
} 

$(window).load(function () { 
	$('#thumbnail').imgAreaSelect({ aspectRatio: '1:<?php echo $thumb_height/$thumb_width;?>', onSelectChange: preview }); 
});
</script>

<br />
<div id="contentArea">
  <div id="content">
    <div class="socialMedia socialMedia--2">
      <!--ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul-->
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAside--2">
        <div class="advertise1">
			<h1><a href="http://studentsforliberty.org/webinar-program/" target="_blank"><img src="<?php echo base_url('images/ads/1.jpg') ?>" alt="" /></a></h1>
		</div>        
        <div class="myNeeds">
          <ul>
          	<li><a href="<?php echo base_url();?>my-account/my-needs">My Needs</a></li>
            <li class="active"><a href="<?php echo base_url();?>my-account/create-need">Create a Need</a></li>
            <li><a href="<?php echo base_url();?>my-account/completed-need">Completed Needs</a></li>
            <li><a href="#">Log Out</a></li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/tabling-kit-request/" target="_blank"><img src="<?php echo base_url('images/ads/2.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-international-sfl-conference/" target="_blank"><img src="<?php echo base_url('images/ads/3.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-european-students-for-liberty-conference/" target="_blank"><img src="<?php echo base_url('images/ads/4.jpg') ?>" alt="" /></a></h1>
		</div>                    
      </aside> 
      <section id="createNeeds">
        <h1>Welcome to your Give the Give of Liberty Account!</h1>
        <span>Now, crop your uploaded photo for your thumbnail.</span>

        <div class="box boxCneeds" style="margin-left: -30px; width: 710px; padding: 15px;">
        	<div id="orig_photo" style="overflow:hidden; margin: auto; border-bottom: 1px solid #CCC; padding-bottom: 5px;">
        		<?php if(!empty($row)){?>
        		<div style="margin: auto; display:inline-block;">
        		<img src="<?php echo base_url();?>images/needs/banners/<?php echo $row['MediaURL']; ?>" id="thumbnail" style="float: left; margin-right: 10px;"/>
        		</div>
        		<?php } ?>
        	</div>
        	<h3>Preview</h3>
        	<div style="margin: auto; display:inline-block;">
	        	<div style="border:1px #e5e5e5 solid; overflow:hidden; width:<?php echo $thumb_width;?>px; height:<?php echo $thumb_height;?>px;">
					<img id="prev" src="<?php echo base_url();?>images/needs/banners/<?php echo $row['MediaURL']; ?>" style="position: relative;" alt="Thumbnail Preview" />
				</div>
			</div>
        	<!-- <div style="margin: auto; display:inline-block;">
	        	<div style="border:1px #e5e5e5 solid; overflow:hidden; width:<?php echo $thumb_width;?>px; height:<?php echo $thumb_height;?>px;">
					<img src="<?php echo base_url();?>images/needs/thumbs/thumbnail_<?php echo $row['MediaURL']; ?>" style="position: relative;" alt="Thumbnail Preview" />
				</div>
			</div> -->
        	<form action="<?php echo base_url();?>my-account/upload-thumb" method="post" enctype="multipart/form-data" id="coords">
	           	<input type="hidden" name="x1" id="x1" />
				<input type="hidden" name="y1" id="y1" />
				<input type="hidden" name="x2" id="x2" />
				<input type="hidden" name="y2" id="y2" />
				<input type="hidden" name="w" id="w" />
				<input type="hidden" name="h" id="h" />
				<input type="hidden" name="thumb_width" value="<?php echo $thumb_width;?>"/>
				<input type="hidden" name="thumb_name" value="<?php echo $row['MediaURL']; ?>" />
				<input type="hidden" name="image_name" value="<?php echo base_url();?>images/needs/banners/<?php echo $row['MediaURL']; ?>" />
				<br />
			<div style="margin: auto; display:inline-block;">
				<input type="submit" name="upload_thumbnail" value="Save Thumbnail" id="save_thumb" style="width: 162px;" class="saveThumbnail" style="float:right;"/>			
	       </div>
	       <div class="clear"></div>   
	       <span id="uploader" style="margin: auto; display:inline-block; color: #222;"></span>
	        </form>
	        <div class="clear"></div>   
        </div>
            
      </section>
      <div class="clear"></div>       
    </section>
  </div>
</div>
