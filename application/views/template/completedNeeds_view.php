<br />
<div id="contentArea">
  <div id="content">
    <div class="socialMedia socialMedia--2">
      <!--ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul-->
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAside--2">
        <div class="advertise1">
			<h1><a href="http://studentsforliberty.org/webinar-program/" target="_blank"><img src="<?php echo base_url('images/ads/1.jpg') ?>" alt="" /></a></h1>
		</div>
        
        <div class="myNeeds">
          <ul>
          	<li><a href="<?php echo base_url();?>my-account/my-needs">My Needs</a></li>
            <li><a href="<?php echo base_url();?>my-account/create-need">Create a Need</a></li>
            <li class="active"><a href="<?php echo base_url();?>my-account/completed-need">Completed Needs</a></li>
            <li><a href="#">Log Out</a></li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/tabling-kit-request/" target="_blank"><img src="<?php echo base_url('images/ads/2.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-international-sfl-conference/" target="_blank"><img src="<?php echo base_url('images/ads/3.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-european-students-for-liberty-conference/" target="_blank"><img src="<?php echo base_url('images/ads/4.jpg') ?>" alt="" /></a></h1>
		</div>                    
      </aside> 
         
      <section id="completedNeeds">
        <div class="sortArea">
          <p>Sort by:</p>
          <select class="sort">
            <option value="recent">Most Recent</option>
            <option value="view">Most View</option>
          </select>
        </div>   
        <h1>You Have 
        	<?php 
        		$CI =& get_instance();
				$CI->load->model('myaccount_model','need');
				$ctr = 0;
        	?>
        	<span id="count"></span>
        Completed Needs</h1>
        <p>You must fill out Success Story Form to complete your need. Once your Need is completed, you will be contracted by an SFL, member to discuss how the funds will be managed and disturbed.</p>
        
        
  	    	
  	   	<?php 
  	   	if(!empty($complete)):
  	   	foreach ($complete as $row): ?>
  	   		<?php $need = $CI->need->getDonations($row['NeedID']); ?>
  	    	<?php if ($need['DonatedAmount'] >= $row['RequiredFunds']): $ctr++;?>
        <div class="box">
          <div class="photoArea">
            <img src="<?php echo base_url();?>images/needs/thumbs/thumbnail_<?php echo $row['MediaURL'];?>" alt="content image">
            <p>Created: <?php echo date('m/d/y',strtotime($row['DateCreated']));?></p>
            <p>Deadline: <?php echo date('m/d/y',strtotime($row['DateNeeded']));?></p>
          </div>
          <h1><?php echo $row['Title'];?></h1>
          <p><?php echo $row['dsc'];?></p>
          <p>Status: Completed</p>
          <div class="barArea">
            <p>$<?php echo number_format($need['DonatedAmount']);?></p>
            <span class="left">$0</span>
            <span class="right">$<?php echo number_format($row['RequiredFunds']);?></span>
            <div class="clear"></div>
          </div>
          <p class="target">Target Reached!</p>
          <div class="clear"></div>
        </div>
        <?php 
        	$isFoundStory = $CI->need->isFoundStory($row['NeedID']);
        	if($isFoundStory == false){
        ?>
        <a href="<?php echo base_url();?>complete-success-story/<?php echo $row['NeedID'];?>" class="storyBtn">Complete Success Story</a>
        <?php }?>
        <div class="clear"></div>
        <?php endif; ?>
        <?php endforeach; 
			else:
				echo "No completed needs yet.";
			endif;
        ?>
		<input type="hidden" value="<?php echo $ctr; ?>" class="ctr"/>
      </section>
      <div class="clear"></div>       
    </section>
   <div class="clear"></div>
  </div>
</div>
<script type="text/javascript">
	$("#count").text($('.ctr').val());
</script>