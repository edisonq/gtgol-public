<link rel='stylesheet' href="<?php echo base_url();?>style/additional-main.css" type="text/css" media="screen" />
<script type='text/javascript' src='<?php echo base_url();?>script/need.js'></script>
<?php
	$CI =& get_instance('model');
	$CI->load->model('myNeeds_model','mNeedsm');
?>
<br />
<div id="contentArea">
  <div id="content">
    <div class="socialMedia socialMedia--2">&nbsp;
      <!--ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul-->
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAside--2">
        <div class="advertise1">
			<h1><a href="http://studentsforliberty.org/webinar-program/" target="_blank"><img src="<?php echo base_url('images/ads/1.jpg') ?>" alt="" /></a></h1>
		</div>        
        <div class="myNeeds">
           <ul>
          <li class="active"><a href="<?php echo base_url();?>my-account/my-needs">My Needs</a></li>
            <li><a href="<?php echo base_url();?>my-account/create-need">Create a Need</a></li>
            <li><a href="<?php echo base_url();?>my-account/completed-need">Completed Needs</a></li>
            <li><a href="#">Log Out</a></li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/tabling-kit-request/" target="_blank"><img src="<?php echo base_url('images/ads/2.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-international-sfl-conference/" target="_blank"><img src="<?php echo base_url('images/ads/3.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-european-students-for-liberty-conference/" target="_blank"><img src="<?php echo base_url('images/ads/4.jpg') ?>" alt="" /></a></h1>
		</div>                    
      </aside> 
      <section id="completedNeeds">
        <div class="sortArea">
          <p>Sort by:</p>
          <select class="sort">
            <option value="recent">Most Recent</option>
            <option value="view">Most View</option>
          </select>
        </div>  
        <br /><br /> 
        <?php if(!empty($myNeeds)){
					foreach($myNeeds as $mNeeds){
						$category =  $CI->mNeedsm->getCategory($mNeeds['CategoryID']);
						$ifTargetReached = $CI->mNeedsm->ifTargetReached($mNeeds['NeedID'],$mNeeds['IsApproved'],$mNeeds['RequiredFunds']);
						$totalDonatedAmount = $CI->mNeedsm->totalDonatedAmount($mNeeds['NeedID']);
						$DescriptionLength = strlen($mNeeds['Description']);
						$needDescription = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />',$mNeeds['Description']);
						$thumbnails = $this->my_needs->getThumbnails($mNeeds['NeedID']);
						if($mNeeds['IsApproved'] == 0){
							$needStatus = 'Pending';
							$doThis = 'href="'.base_url().'my-needs/edit-need?nid='.$mNeeds['NeedID'].'"';
							$updateName = "Update Need &nbsp;";
						}else if($mNeeds['IsApproved'] == 1){
							$needStatus = 'Approved';
							$doThis ='onClick="updateButton('.$thumbnails[0]->MediaID.');" href="javascript:void(0)"';
							$updateName = "Update Status";
						}else{
							$needStatus = 'Declined';
							$doThis = 'href="'.base_url().'my-needs/edit-need?nid='.$mNeeds['NeedID'].'"';
							$updateName = "Update Need &nbsp;";
						}
						?>
						<form id="submitUpdateStatus<?php echo $mNeeds['NeedID'];?>" action="<?php echo base_url();?>my-needs/update-status" method="POST">
						<input id="description<?php echo $mNeeds['NeedID'];?>" type="hidden" value="<?php echo htmlspecialchars($needDescription);?>"/>
						<input id="needID<?php echo $mNeeds['NeedID'];?>" name="nid" type="hidden" value="<?php echo $mNeeds['NeedID'];?>"/>
						<div class="box">
						  <div class="photoArea">
						  	<?php if(!empty($thumbnails[0]->MediaName)){?>
							<?php error_reporting(0); ?>
							<?php if (getimagesize(APPPATH.'../images/needs/thumbs/'.$thumbnails[0]->MediaName)): ?>
								<img src="<?php echo base_url();?>images/needs/thumbs/<?php echo $thumbnails[0]->MediaName; ?>" width="128" height="89">
							<?php else: ?>
								<img class="featuredImage" src="<?php echo base_url();?>images/blank-need-edit.png" alt="content image">
							<?php endif; ?>
							<?php }else{ ?>
								<img src="<?php echo base_url();?>images/blank-need-edit.png" width="128" height="89"/>
							<?php }?>
							<a href="<?php echo base_url();?>my-account/crop-photos?mid=<?php echo $thumbnails[0]->MediaID;?>" class="editThumbs">Edit Thumbnail</a>
							<p>Created: <?php echo date('d',strtotime($mNeeds['DateCreated'])).'/'.date('m',strtotime($mNeeds['DateCreated'])).'/'.date('Y',strtotime($mNeeds['DateCreated']));?></p>
							<p style="display:<?php if($mNeeds['IsApproved'] == 0){echo 'none';}?>">Deadline: <?php echo date('d',strtotime($mNeeds['DateNeeded'])).'/'.date('m',strtotime($mNeeds['DateNeeded'])).'/'.date('Y',strtotime($mNeeds['DateNeeded']));?></p>
						  	<br />
						  </div>
						   
						  <h1><a href="<?php echo base_url();?>student-needs/view/<?php echo $mNeeds['NeedID'];?>"><?php echo $mNeeds['Title'];?></a></h1>
						  <p><strong>Category:</strong> <?php echo $category;?></p>
						  <p id="needDescription<?php echo $mNeeds['NeedID'];?>" class="needDescription"><?php echo substr($needDescription,0,205); 
									if($DescriptionLength > 205){ ?>
										...<a href="javascript:void(0);" onclick="readMore(<?php echo $mNeeds['NeedID'];?>);">more</a>
							  <?php } ?>
						  </p>
						  <div class="readMoreBottom">
						  <p><strong>Status:</strong> <?php echo $needStatus;?></p>
						  <?php if($totalDonatedAmount > 0){?>
							<div class="barArea">
								<p>$ <?php echo $totalDonatedAmount;?></p>
								<span class="left">$0</span>
								<span class="right">$<?php echo $mNeeds['RequiredFunds'];?></span>
								<div class="clear"></div>
							</div>
						  <p class="target" style="display:<?php if($ifTargetReached == 1){echo 'block';}else{echo 'none';}?>">Target Reached!</p>
						  <div class="clear"></div>
						  <?php } ?>
						  </div> 
						</div>
						<?php if($ifTargetReached == 1):?>
							<?php $storyBtnHref = "href='".base_url()."complete-success-story/".$mNeeds['NeedID']."'";
								  $storyBtnOnClick = "";
							?>
						<?php else:?>
							<?php $storyBtnHref = "href='javascript:void(0)'";
								  $storyBtnOnClick = "onclick='alert('Need must reach the target fund first!');'";
							?>
						<?php endif;?>
						<?php 
			        		$CI =& get_instance(); 
							$CI->load->model('myaccount_model','need');
							$isFoundStory = $CI->need->isFoundStory($mNeeds['NeedID']);
								if($isFoundStory == false):
									$displayStoryBtn = "block";
								else:
									$displayStoryBtn = "none"; 
								endif;
						?>
						<?php if($ifTargetReached == 1): ?>
						<a <?php echo $storyBtnHref;?> <?php echo $storyBtnOnClick;?> style="display:<?php echo $displayStoryBtn;?>" class="storyBtn">Complete Success Story</a>
						<?php endif ; ?>
						<a <?php echo $doThis;?> class="UstoryBtn" id="ub<?php echo $thumbnails[0]->MediaID; ?>"><?php echo $updateName;?></a>
						<div class="clear"></div>
						<div class="popupUpdateStory" id="pus<?php echo $thumbnails[0]->MediaID; ?>"> 
							<textarea class="txtareaUpdateStory" name="Status" id="txtareaUpdateStory<?php echo $mNeeds['NeedID'];?>"></textarea>
							<span class="updateStatusMessage" id="updateStatusMessage<?php echo $mNeeds['NeedID'];?>"></span>
							<input class="submitUpdateStory"  value="Submit" id="submit<?php echo $mNeeds['NeedID'];?>" type="submit" onclick="updateStatus(<?php echo $mNeeds['NeedID'];?>);return false;"/>
							<div id="arrow-down<?php echo $mNeeds['NeedID'];?>" class="arrow-down"></div>
						
						<script type="text/javascript">
							if(<?php echo $isFoundStory;?> == true){
								$('#arrow-down<?php echo $mNeeds['NeedID'];?>').css({'margin-left':'80%','margin-top':'2px'});
							}
						</script>
						</div>
						</form>
       <?php		}
				}else{
						echo "No needs created yet!";
				} ?>
      </section>
      <div class="clear"></div>       
    </section>

  </div>
</div>
<footer>