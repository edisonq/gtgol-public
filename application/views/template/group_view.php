<link rel='stylesheet' href="<?php echo base_url();?>style/cms.css" type="text/css" media="screen" />
<style>
	ul li{
		margin-left: 20px;
	}
</style>
<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
	    	&nbsp;
	    </div>
     	<p>&nbsp;</p>
     	<div class="breadlink"><i><a href="<?php echo base_url();?>admin/dashboard">< back</a></i></div>
		<div id="group">
			<div id="group-header">
				<strong>Group</strong>
			</div>
			<div id="group-body">
				<fieldset>
					<legend>Header</legend>
					<a class="add-button" href="<?php echo base_url();?>admin/group/menu/add?group=header"><?php if(empty($menuHeader)){ echo "Add Menu"; }else{ echo "Edit Menu"; }?></a>
					<ul>
						<li><?php if(!empty($menuHeader)){ echo $menuHeader['MenuName']." <a class='add-button' href='".base_url()."admin/group/menu/remove?mid=".$menuHeader['MenuID']."'>Remove Menu</a>"; }else{ echo "No assigned menu yet."; } ?></li>
					</ul>
				</fieldset>
				
				<fieldset>
					<legend>Sidebar</legend>
					<a class="add-button" href="<?php echo base_url();?>admin/group/menu/add?group=sidebar"><?php if(empty($menuSide)){ echo "Add Menu"; }else{ echo "Edit Menu"; }?></a>
					<ul>
						<li><?php if(!empty($menuSide)){ echo $menuSide['MenuName']." <a class='add-button' href='".base_url()."admin/group/menu/remove?mid=".$menuSide['MenuID']."'>Remove Menu</a>"; }else{ echo "No assigned menu yet."; } ?></li>
					</ul>
				</fieldset>
				
				<fieldset>
					<legend>Footer</legend>
					<a class="add-button" href="<?php echo base_url();?>admin/group/menu/add?group=footer"><?php if(empty($menuFooter)){ echo "Add Menu"; }else{ echo "Edit Menu"; }?></a>
					<ul>
						<li><?php if(!empty($menuFooter)){ echo $menuFooter['MenuName']."<a class='add-button' href='".base_url()."admin/group/menu/remove?mid=".$menuFooter['MenuID']."'>Remove Menu</a>"; }else{ echo "No assigned menu yet."; } ?></li>
					</ul>
				</fieldset>
			</div>
		</div>
		<div style="clear:both;"></div>
		<p>&nbsp;</p>
	</div>
</div>