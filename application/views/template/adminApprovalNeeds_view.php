<link href="<?php echo base_url();?>style/admin.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='<?php echo base_url();?>script/jquery-1.8.2.min.js'></script>
<script style="text/javascript" src="<?php echo base_url();?>script/adminapproval.js"></script>

<div id="wrapper">
	<a href="<?php echo base_url();?>/admin-approval/dashboard/needs">back</a><br />
<h4>Pending for approval needs.</h4>
<table id="admin-table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Serial Number</th>
			<th>Title</th>
			<th>Beneficiary</th>
			<th>Description</th>
			<th>Required funds</th>
			<th>Desription of funds need</th>
			<th>Date Needed</th>
			<th>Date Created</th>
			<th>Category</th>
			<th>Added by this user</th>
			<th>Approved</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$ctr = 1;
		if(!empty($pendingNeeds)){
			foreach($pendingNeeds as $row){
			$DescriptionLength = strlen($row['Description']);
			$DescriptionFundsLength = strlen($row['DescriptionFunds']);
			$needDescription = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />',$row['Description']);
			$needDescriptionFunds = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />',$row['DescriptionFunds']);
		?>
		<form id="approveNeed<?php echo $row['NeedID'];?>" action="<?php echo base_url();?>admin-approval/pending-need?nid=<?php echo $row['NeedID'];?>" method="GET">
		<tr id="need<?php echo $row['NeedID'];?>">
			<td><?php echo $ctr++;?></td>
			<td><?php echo $row['SerialNumber'];?></td>
			<td><?php echo $row['Title'];?></td>
			<td><?php echo $row['Beneficiary'];?></td>
			<td >
			<?php echo substr($needDescription,0,205);
				if($DescriptionLength > 205){ ?>
						...<a href="javascript:void(0);" onclick="readMore(<?php echo $row['NeedID'];?>);">more</a>
			<?php } ?>
			</td>
			<td><?php echo $row['RequiredFunds'];?></td>
			<td>
			<?php echo substr($needDescription,0,205);
				if($DescriptionLength > 205){ ?>
						...<a href="javascript:void(0);" onclick="readMore(,<?php echo $row['NeedID'];?>);">more</a>
			<?php } ?>
			</td>
			<td><?php echo date('d',strtotime($row['DateNeeded'])).'/'.date('m',strtotime($row['DateNeeded'])).'/'.date('Y',strtotime($row['DateNeeded']));?></td>
			<td><?php echo date('d',strtotime($row['DateCreated'])).'/'.date('m',strtotime($row['DateCreated'])).'/'.date('Y',strtotime($row['DateCreated']));?></td>
			<td><?php echo $row['CategoryName'];?></td>
			<td><?php echo $row['user_login'];?></td>
			<td><input  type="submit" style="display:none" id="submitApprove<?php echo $row['NeedID'];?>" onclick="approvePendingNeeds(<?php echo $row['NeedID'];?>);"/><a href="javascript:void(0)" onclick="clickSubmit('need',<?php echo $row['NeedID'];?>)">Yes</a>&nbsp;|&nbsp;
				<a href="<?php echo base_url();?>admin-approval/pending-need/decline?nid=<?php echo $row['NeedID'];?>">No</a></td>
		</tr>	
		</form>
		<?php 
			}
		}else{
			echo '<strong>No pending needs!</strong>';
		}?>
	</tbody>
</table>
</div>