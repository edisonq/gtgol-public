<div id="contentArea">
    <div id="socialSpace">
        &nbsp;
    </div>
    <div id="content">
        <div id="contentHeader">
            <h1>Student Needs</h1>
        </div>
        <!-- 3 columns -->
        <div class="clear"></div>
        <section class="mainContent">
           
                <?php $CI = & get_instance(); ?>
				<?php $this->load->view('layouts/search_form') ?>
				
                
            <section id="pageSection">
                <?php
                foreach ($needs as $row) {
                    $DescriptionLength = strlen($row['dsc']);
                    $sneedDescription = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />', $row['dsc']);
                    ?>
                    <input id="description<?php echo $row['NeedID'];?>" type="hidden" value="<?php echo htmlspecialchars($sneedDescription);?>"/>
					<div class="clear"></div> 
                    <div class="project">
                        <div class="project-left">
                            <div class="project-left-image">
                                <a href="<?php echo base_url(); ?>student-needs/view/<?php echo $row['NeedID']; ?>">	
                                    <?php error_reporting(0); ?>
                                    <?php if (getimagesize(APPPATH.'../images/needs/thumbs/thumbnail_'.$row['MediaURL'])): ?>
                                        <img src="<?php echo base_url(); ?>images/needs/thumbs/thumbnail_<?php echo $row['MediaURL']; ?>"/>
                                    <?php else: ?>
                                        <img class="featuredImage" src="images/blank.png" alt="content image">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="project-left-details">
                                <span>Created: <?php echo date('m/d/y', strtotime($row['DateCreated'])); ?></span><br />
                                <span>Deadline: <?php echo date('m/d/y', strtotime($row['DateNeeded'])); ?></span>
                            </div>
                        </div>
                        <div class="project-center">
                            <div class="project-center-title"><a href="<?php echo base_url(); ?>student-needs/view/<?php echo $row['NeedID']; ?>"><?php echo $row['Title']; ?></a></div>
                            <div class="project-center-body">
                                <p id="needDescription<?php echo $row['NeedID']; ?>" class="needDescription"><?php
				                echo substr($sneedDescription, 0, 205);
				                if ($DescriptionLength > 205) {
				                        ?>
                                        ...<a href="javascript:void(0);" onclick="readMore(<?php echo $row['NeedID']; ?>);">more</a>
                                    <?php } ?>
    						</p><!--<span class="more">more</span>-->
                            </div>
                            <div class="project-center-school">
                                <strong>School: </strong><?php echo $row['Beneficiary']; ?>
                            </div>
                            <div class="meter-need" id="bar-<?php echo $row['HasMediaID']; ?>" name="<?php echo $row['HasMediaID']; ?>">
                                <?php
                                
                                $need = $CI->need->getDonations($row['NeedID']);
                                if (!empty($need)) {
                                    ?>
                                    <div class="meter-inner-need">
                                        <span><strong>$<?php echo number_format($need['DonatedAmount']); ?></strong></span>				
                                    </div>           	              	
                                    <script type="text/javascript">	              		
                                        $(function(){
                                            $("#torch-"+<?php echo $row['HasMediaID']; ?>).each(function(){
                                                var width = $('#bar-'+<?php echo $row['HasMediaID']; ?>).width();
                                                var perc = $('.percent-'+<?php echo $row['HasMediaID']; ?>).val();
                        									
                                                if(perc == 1){
                                                    $(this).css({
                                                        'margin-left': '11px',
                                                    });
                                                }
                        									
                                                $(this)
                                                .data("origWidth", $(this).width())
                                                .width(0)
                                                .animate({
                                                    width: width * perc
                                                }, 1200);
                                            });
                        								
                                            $("#bar-"+<?php echo $row['HasMediaID']; ?>+" > .meter-inner-need > span").each(function() {
                                                var width = $('#bar-'+<?php echo $row['HasMediaID']; ?>).width();
                                                var perc = $('.percent-'+<?php echo $row['HasMediaID']; ?>).val();
                        									
                                                if(perc == 1){
                                                    $(this).css({
                                                        '-webkit-border-top-right-radius': '22px',
                                                        '-webkit-border-bottom-right-radius': '22px',
                                                        'background':'#41A317'                                                
                                                    });
                                                }
                        									
                                                $(this)
                                                .data("origWidth", $(this).width())
                                                .width(0)
                                                .animate({
                                                    width: width * perc
                                                }, 1200);
                                            });
                                        });
                                    </script> 	   			
                                </div>
                                <input type="hidden" class="percent-<?php echo $row['HasMediaID']; ?>" value="
                                <?php 
                                if($need['DonatedAmount'] <= $row['RequiredFunds']){
                                	echo 1 * ($need['DonatedAmount'] / $row['RequiredFunds']); 
                                }else if($need['DonatedAmount'] > $row['RequiredFunds']){
                                	echo 1 * ($row['RequiredFunds'] / $row['RequiredFunds']); 
                                }
                              
                                ?>"/>
                                <div class="torch-need" id="torch-<?php echo $row['HasMediaID']; ?>"><span class="torch-bg"></span></div>  
                            <?php } else { ?>
                                <div class="meter-inner-need">
                                    <span><strong>$0</strong></span>				
                                </div>
                            </div>
                            <input type="hidden" class="percent-<?php echo $row['HasMediaID']; ?>" value=""/>
                            <div class="torch-need" id="torch-<?php echo $row['HasMediaID']; ?>"><span class="torch-bg"></span></div>  
                        <?php } ?>		
                        <div class="days-to-go"><?php
                    $requiredDate = strtotime($row['DateNeeded']);
                    $timeleft = $requiredDate - time();
                    $daysleft = round((($timeleft / 24) / 60) / 60);

                    if ($daysleft > 1) {
                        echo $daysleft . " days to go!";
                    } else if ($daysleft < -1) {
                        $d = $daysleft * -1;
                        echo $d . " days ago";
                    } else if ($daysleft == 1) {
                        echo $daysleft . " day to go";
                    } else if ($daysleft == -1) {
                        $d = $daysleft * -1;
                        echo $d . " day ago";
                    } else if ($daysleft == 0) {
                        echo "Today";
                    }
                        ?></div>
                        <div class="clear"></div>      
                        <div class="range">
                            <span id="range-left">$0</span>
                            <span id="range-right">$<?php echo number_format($row['RequiredFunds']); ?></span>
                        </div>
                    </div>
                    <div class="project-right">
                        <div id="donatePanel">
                            <form method="post" action="<?php echo base_url(); ?>student-needs/donation/<?php echo $row['NeedID']; ?>">
                                <input type="text" name="donateValue" id="donateValue" placeholder="$" onkeypress="return isNumberKey(event)">
                                <input type="submit" name="donateButton" id="donateButton" value="DONATE">
                            </form>
                            <?php $count = $CI->need->countDonor($row['NeedID']); ?>
                            <div id="donate-donors">
                                <?php echo $count['ctr']; ?> donors
                            </div>
                            <input id="shareThis" class="sharebutton1" name="shareThis" type="button" value="Share This" rel="https://www.facebook.com/sharer.php?u=<?= urlencode(base_url(). 'student-needs/view/' . $row['NeedID'])?>" />
                        </div>
                    </div>
                    </div> 
                <?php } ?>
                <?php if ($cntneeds > 5): ?>
                    <div id="pagination">
                        <ul><?php if($cntneeds%5==0):?>
								<?php $numPage = $cntneeds / 5;?>
							<?php else:?>
								<?php $numPage = ($cntneeds / 5) + 1;?>
							<?php endif;?>
                            <?php
                            $a = 5;
                            $offset = ($numPage - 1) * 5;
                            $nextOffset = $pagenum * 5;
                            $previousOffset = ($pagenum - 2) * 5;
                            $nextPage = $pagenum + 1;
                            $previousPage = $pagenum - 1;
                            ?>
                            <?php if ($pagenum < 5): ?>
                                <?php if ($numPage > 5): ?>
                                    <?php $b = 5; ?>
                                <?php else: ?>
                                    <?php $b = $numPage; ?>
                                <?php endif; ?>
                                <?php for ($i = 1; $i <= $b; $i++): ?>
                                    <?php $o = ($i - 1) * 5; ?>
                                    <?php if ($i == 1): ?>
                                        <?php $href = base_url() . "student-needs"; ?>
                                    <?php else: ?>
                                        <?php $href = base_url() . "student-needs/needs?page=" . $i . "&offset=" . $o; ?>
                                    <?php endif; ?>
                                    <li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo $href; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $i; ?></a></li>
                                <?php endfor; ?>
                                <?php if ($numPage > 6 && $pagenum != ($numPage - 5) && $pagenum != ($numPage - 1) && $pagenum != $numPage): ?>
                                    ...<li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo base_url(); ?>student-needs/needs?page=<?php echo $numPage; ?>&offset=<?php echo $offset; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $numPage; ?></a></li>	&nbsp;&nbsp;
                                <?php endif; ?>
                            <?php elseif ($pagenum % 5 == 0 || $pagenum > 5): ?>
                                <?php $a+=5; ?>
                                <?php for ($i = 1; $i <= $a; $i++): ?>
                                    <?php $o = ($i - 1) * 5; ?>
                                    <?php if ($i == 1): ?>
                                        <?php $href = base_url() . "student-needs"; ?>
                                    <?php else: ?>
                                        <?php $href = base_url() . "student-needs/needs?page=" . $i . "&offset=" . $o; ?>
                                    <?php endif; ?>
                                    <li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo $href; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $i; ?></a></li>
                                <?php endfor; ?>
                                <?php if ($numPage > 5 && $pagenum != ($numPage - 5) && $pagenum != ($numPage - 1) && $pagenum != $numPage): ?>
                                    ...<li style="<?php if ($i == $pagenum): ?>background: #CF8428<?php endif; ?>"><a href="<?php echo base_url(); ?>student-needs/needs?page=<?php echo $numPage; ?>&offset=<?php echo $offset; ?>" style="<?php if ($i == $pagenum): ?>color:#ffffff<?php endif; ?>"><?php echo $numPage; ?></a></li>	&nbsp;&nbsp;
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if ($pagenum > 1): ?>
                                <?php if ($pagenum == 2): ?>
                                    <?php $href1 = base_url() . "student-needs"; ?>
                                <?php else: ?>
                                    <?php $href1 = base_url() . "student-needs/needs?page=" . $previousPage . "&offset=" . $previousOffset; ?>
                                <?php endif; ?>
                                <a id="previous" href="<?php echo $href1; ?>">< Previous </a>&nbsp;
                            <?php endif; ?>
                            <?php if ($pagenum <($numPage - 1) && $pagenum > 1): ?>
                                <a id="next" href="<?php echo base_url(); ?>student-needs/needs?page=<?php echo $nextPage; ?>&offset=<?php echo $nextOffset; ?>">Next ></a>
                            <?php endif; ?>
                        </ul>

                    </div>  
                <?php endif; ?> 
                <div class="clear"></div>
            </section>
            <div class="clear"></div>
        </section>
        <p>&nbsp;</p>
    </div>
</div>
<script type="text/javascript">
$(function(){
   $('.search-needs').click(function(){
        $('.submit').click();
    });
    
   $('.sharebutton1').click(function(){
       window.open($(this).attr('rel'));
   }); 
});
</script>