<link href="<?php echo base_url();?>style/admin.css" rel="stylesheet" type="text/css"/>
<div id="wrapper">
	<a href="<?php echo base_url();?>admin-approval/dashboard/needs/all">back</a>
	<div id="admin-form">
		<h3>Update Need</h3>
		<form action="<?php echo base_url();?>admin-approval/dashboard/needs/update" method="post">
			<div class="form_row">
				<span class="label">Need ID: </span>
				<strong><?php echo $row['NeedID'];?></strong>
				<input type="hidden" name="NeedID" value="<?php echo $row['NeedID'];?>">
			</div>
			<div class="form_row">
				<span class="label">Title: </span>
				<input type="text" name="Title" value="<?php echo $row['Title'];?>"/>
			</div>
			<div class="form_row">
				<span class="label">Beneficiary: </span>
				<input type="text" name="Beneficiary" value="<?php echo $row['Beneficiary'];?>"/>
			</div>
			<div class="form_row">
				<span class="label">Description: </span>
				<textarea name="Description"><?php echo $row['Description'];?></textarea>
			</div>
			<div class="form_row">
				<span class="label">Required Funds: </span>
				<input type="text" name="RequiredFunds" value="<?php echo $row['RequiredFunds'];?>"/>
			</div>
			<div class="form_row">
				<span class="label">Fund Description: </span>
				<textarea name="DescriptionFunds"><?php echo $row['DescriptionFunds'];?></textarea>
			</div>
			<div class="form_row">
				<span class="label">Deadline: </span>
				<strong></strong> 
				<input type="text" name="DateNeeded" value="<?php echo date('m/d/y',strtotime($row['DateNeeded']));?>"/>
			</div>
                        <div class="form_row">
				<span class="label">Featured: </span>
				<strong></strong>
				<select name="Featured">
					<option <?= $row['Featured'] == 1 ? 'selected="selected"':'' ?> value="1">Yes</option>
					<option <?= $row['Featured'] == 0 ? 'selected="selected"':'' ?> value="0">No</option>
				</select>
			</div>
			<div class="form_row">
				<span class="label">Category: </span>
				<select name="CategoryID">
					<option selected="selected" value="<?php echo $row['CategoryID'];?>"><?php echo $row['CategoryName'];?></option>
					<?php foreach($ctgry as $c):?>
					<option value="<?php echo $c['CategoryID'];?>"><?php echo $c['CategoryName'];?></option>
					<?php endforeach;?>
				</select>
			</div>
			<div class="form_row">
				<span class="label">Created by: </span>
				<strong><?php echo $row['user_login'];?></strong>
			</div>
			<div class="form_row">
				<span class="label">&nbsp;</span>
				<input type="submit" value="Update"/>
			</div>
		</form>
		
	</div>
</div>