
<br />
<div id="contentArea">
  <div id="content">
    <div class="socialMedia socialMedia--2">
      <!--ul>
        <li class="fb"><a href="#"></a></li>
        <li class="twiter"><a href="#"></a></li>
        <li class="plus"><a href="#"></a></li>
      </ul-->
    </div>
    <div class="clear"></div>    
  	<div id="contentHeader">
    	<h1>My Account</h1>
    </div>
    <div class="clear"></div>
    <section class="mainContent">
      <aside id="pageAside--2">
        <div class="advertise1">
			<h1><a href="http://studentsforliberty.org/webinar-program/" target="_blank"><img src="<?php echo base_url('images/ads/1.jpg') ?>" alt="" /></a></h1>
		</div>        
        <div class="myNeeds">
          <ul>
          	<li><a href="<?php echo base_url();?>my-account/my-needs">My Needs</a></li>
            <li><a href="<?php echo base_url();?>my-account/create-need">Create a Need</a></li>
            <li class="active"><a href="<?php echo base_url();?>my-account/completed-need">Completed Needs</a></li>
            <li><a href="#">Log Out</a></li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/tabling-kit-request/" target="_blank"><img src="<?php echo base_url('images/ads/2.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-international-sfl-conference/" target="_blank"><img src="<?php echo base_url('images/ads/3.jpg') ?>" alt="" /></a></h1>
		</div>
		<div class="advertise21 advertise--21">
			<h1><a href="http://studentsforliberty.org/event/2013-european-students-for-liberty-conference/" target="_blank"><img src="<?php echo base_url('images/ads/4.jpg') ?>" alt="" /></a></h1>
		</div>                    
      </aside> 
      <section class="createNeeds">
        <h1>Congratulations! You've reached Your Target Goal!</h1>
        <div class="caption"><span>Please fill out this form to create your Success Story and show your appreciation to your donors!</span></div>
		<div class="clear"></div>
		<div class="project-name">Project Name Goes Here</div>
        <div class="box boxCneeds">
        	<form action="<?php echo base_url();?>submit-complete-story" method="post" id="submit-success" enctype="multipart/form-data">
	            <h2>Thank You Letter: </h2>
	            <div class="sub-label">Please write a letter thanking your donors and explaining how your project went so <br />they know that you used their investments wisely to promote liberty. </div>
	            <textarea class="descFunds" name="thankYouLetter"></textarea>
	            <div class="clear"></div>
	            <div class="uploadPhoto1">
							<h2>Upload Photos: </h2>     
							<input type="file" class="fileselect" style="width:0px; height: 23px" name="userfiles[]"/><a href="javascript:;" id="addfileselect">+ Add More</a><br />
	            </div>
	            <input type="hidden" name="NeedID" value="<?php echo $this->uri->segment(2);?>"/>
	            <div class="clear"></div>
	            <h2 class="t2">Embed a YouTube Video <span style="font-family: arial; font-size: 14px;">(optional):</span></h2>
	            <input class="inputTags" type="text" name="youtube_link"/> 
	            
	            <div class="clear"></div>
	            <input type="hidden" value="<?php echo $this->session->userdata('ID');?>" id="UserId" name="UserId" />   
	           
        </div>
        <br />
        <div class="clear"></div>
        <div class="caption"><span>Please fill out this form to create your Success Story and show your appreciation to your donors!</span></div>
        <div class="clear"></div><br />
        <div class="box boxCneeds">
        	<div class="uploadPhoto">
	        	<h2>Upload Receipts: </h2>     
	           	<input type="file" name="userfile1"/><br>
	        </div>
        	   
	        <div class="clear"></div>
	        <div class="uploadPhoto">
	            <h2>Upload Expense Form: </h2>     
	            <input type="file" name="userfile2"/><br>
	            <input type="submit" value="Submit Story" style="display:none;" class="submit"/>     
            </form>  
	        </div>        
        </div>     
     	<div id="success-submit">
	     	<a href="javascript:void(0);" class="submitStoryBtn">Submit Story</a>
		    <span id="uploader" style="color: #222;"></span>    	     
      	</div>
      </section>
   			
      <div class="clear"></div>       
    </section>
  </div>
</div>
	<div id="backgroundPopup"></div>
<script type="text/javascript">
	$("#datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-5:+25"
    });
	
	$(function(){
		$('#addfileselect').click(function(){
			$('div .uploadPhoto1').append('<input type="file" style="width:0px; height: 23px" class="fileselect" name="userfiles[]"/><br />');
		});
	});
</script>
