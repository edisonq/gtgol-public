<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
			<h1>User Profile</h1>
		</div>
     	<p>&nbsp;</p>
		<?php
			if($IsLogin == 1) {
		?>
		<div id='profile-sidebar'>
			<div>
				<p>Profile Summary</p>
				<p>Username: Alvin</p>				
				<p>Email: Alvin@themarketaces.com</p>				
				<p>First name: Alvin</p>				
				<p>Last Name: Comahig</p>				
			</div>
			<div>
				<p>Needs</p>
				<p>Total Needs: 100</p>				
				<p>Compleded Needs: 100</p>				
				<p>Needs in progress: 100</p>			
			</div>
			<div>
				<p>Stories</p>
				<p>Total Stories: 100</p>		
			</div>
			<div>
				<p>Donations</p>
				<p>Number of Donations: 100</p>		
			</div>
		</div>
		<div id="profile">
			
			<div class='profile-content'>
				<ul class='user-menu'>
					<li class='u-info current'><span>My Profile</span></li>
					<li class='u-needs'><span>My Needs</span></li>
					<li class='u-donate'><span>My Donations</span></li>
					<li class='u-notification'><span>My Notifications</span></li>
				</ul>
				<div class='user-info ucontent udefault'>
					
					<div id="prof-info">
						<div id="pnl-user" class='pnl'>
							<div class='txt-username txt'>Username:</div>
							<div class='lbl-username lbl'>Alvin</div>
							<div class='inp-username inp'><input type="text" name="username" value="alvinucsk"/></div>
						</div>	
						<div id="pnl-user" class='pnl'>
							<div class='txt-email txt'>Email:</div>
							<div class='lbl-email lbl'>Alvin</div>
							<div class='inp-username inp'><input type="text" name="email" value="alvinucsk@gmail.com"/></div>
						</div>	
						<div id="pnl-user" class='pnl'>
							<div class='txt-firstname txt'>First name:</div>
							<div class='lbl-username lbl'>Alvin</div>
							<div class='inp-username inp'><input type="text" name="firstname" value="Alvin"/></div>
						</div>	
						<div id="pnl-user" class='pnl'>
							<div class='txt-lastname txt'>Last name:</div>
							<div class='lbl-username lbl'>Alvin</div>
							<div class='inp-username inp'><input type="text" name="lastname" value="Comahig"/></div>
						</div>	
						<div id="pnl-user" class='pnl'>
							<div class='txt-pwd txt'>Password:</div>
							<div class='lbl-username lbl'>Alvin</div>
							<div class='inp-username inp'><input type="password" name="password" value="123456"/></div>
						</div>	
						<div id="pnl-user" class='pnl'>
							<div class='txt-repwd txt'>Re-Password:</div>
							<div class='lbl-username lbl'>Alvin</div>
							<div class='inp-username inp'><input type="password" name="repassword" value="qwerty"/></div>
						</div>
						<div id="user-save">
							<input type="button" value="save" id="btn-user-save"/>
						</div>
					</div>
				</div>
				<div class='user-needs ucontent'>Needs</div>
				<div class='user-donate ucontent'>Donations</div>
				<div class='user-notification ucontent'>Notifications</div>
			</div>
		</div>
		<?php 
			} 
			else {
		?>
			<div class='loginmessage'>
				<p>You are not logged in, Click <a href='<?php echo base_url() ?>login'><strong>here</strong></a> to login.</p>
			</div>
		<?php
			}
		?>
	</div>
</div>
