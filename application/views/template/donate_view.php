<div id="contentArea">
	<div id="socialSpace">
    &nbsp;
    </div>
	<div id="content">
		<div id="contentHeader">
			<h1>Donate</h1>
		</div>
		<p>&nbsp;</p>
		<div  class="contentWrapper">
			<div id="donate-sidebar" class="divContent">
				<p style="padding-top: 125px;">
					<span class="first">Thank you for your donation!</span> Every Penny counts.
					Once you fill out your payment infomation, your
					credit card will be charged immediately for the
					specified donation amound, upon confirmation.
				</p>
				<p>
					If you would like, you can provide a short sentence
					or too on why you donated to this particular Need.
					Your comment, along with your name, will be presented
					on that project's Need Page.
				</p>
				<div id="cc-cards"></div>
			</div>
			<div id="donate-content">
				<div class="divContent acenter">
					<h1>Thank You For Your Support!</h1>
					<p class="s17">Please fill out this form to complete your donation process.</p>
				</div>
				<div class="divContent">
					<h2>Project name goes here</h2>
					<div id="donate-form">
						<div class="frst">Donation amount:</div>
						<div class="flds">
							<span>$</span><input type="text" class='logintext'>
						</div>
						<div class="ftxt">First name*:</div>
						<div class="flds">
							<input type="text" class='logintext w220'>
						</div>
						<div class="ftxt">Last name*:</div>
						<div class="flds">
							<input type="text" class='logintext w220'>
						</div>
						<div class="ftxt">Email address*:</div>
						<div class="flds">
							<input type="text" class='logintext w250'>
						</div>
						<div class="ftxt">Billing address*:</div>
						<div class="flds">
							<input type="text" class='logintext w560'>
							<div>&nbsp;</div>
							<input type="text" class='logintext w560'>
						</div>
						<div class="tri">
							<div class="ftxt left w240">City*: </div><div class="ftxt left w90">State*: </div><div class="ftxt left">ZIP*: </div>
						</div>
						<div class="flds">
							<input type="text" class='logintext w220 m-left-20'><input type="text" class='logintext w70 m-left-20'><input type="text" class='logintext w70'>
						</div>
							
					</div>
					
				</div>
				<p>&nbsp;</p>
			</div>
		</div>
	</div>
</div>