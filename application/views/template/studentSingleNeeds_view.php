<div id="contentArea">
    <div id="socialSpace">
		&nbsp;
    </div>
    <div id="content">
        <div id="contentHeader">
            <h1>Student Needs</h1>
        </div>
		<p>&nbsp;</p>

        <div class="contentWrapper">
			<div id="studentNeedsTitle">
                <div id="studentNeedsTitleleft">
                    <h2><?php echo $row['Title']; ?></h2>
					<?php
					$CI = & get_instance();
					$CI->load->model('studentNeeds_model', 'need');
					$donor = $CI->need->getUserNeeds($row['NeedID']);
					$need = $CI->need->getDonations($row['NeedID']);
					?>
                    <h4>By <?php echo @$donor['name']; ?></h4>
                </div>
                <div id="studentNeedsTitleRight">
                    <a href="<?php echo base_url(); ?>student-needs" >Back to Student Needs</a>
                </div>
            </div>
            <!-- 2 columns -->
            <div class="colmask rightmenu">
                <div class="colleft">
                    <div class="col1">
						<?php if (!empty($story)): ?>
							<div class="page-box" style="border: 1px solid #ddd; clear: both; padding: 5px; margin-right: 5px; margin-bottom:15px;">
								<h2>Thank you letter: </h2>
								<div class="page-box-image">
									<center>
										<?php foreach($photos as $photo):?>
										<a href="<?php echo base_url(); ?>images/success-stories/<?php echo $photo['MediaURL']; ?>" target="_blank">
											<img src="<?php echo base_url(); ?>images/success-stories/primary-photo/<?php echo $photo['MediaURL']; ?>"/>
										</a>
										<?php endforeach; ?>
									</center>       				      				
								</div><br />
								<div class="page-box-content2">		        				
									<p id="ssDescription<?php echo $story['StoryID']; ?>">
										<pre><?php echo $story['thankYouLetter']; ?></pre>									
									</p>
								</div>
							</div>   
						<?php endif; ?> 
						<div class="clear"><br /></div>
                        <!-- Column 1 start -->
                        <div id="needsBanner" style="background: transparent;">
							<?php $image = $CI->need->getStoryImage($this->uri->segment(3)); ?>
							<img src="<?php echo base_url(); ?>images/needs/banners/<?php echo $image['sURL']; ?>"/>
                        </div>
                        <p><strong>Created: <?php echo date('m/d/y', strtotime($row['DateCreated'])); ?></strong> | <strong>Deadline: <?php echo date('m/d/y', strtotime($row['DateNeeded'])); ?></strong></p>
                        <p><strong>School</strong>: <?php echo $row['Beneficiary']; ?></p>
                        <p><strong>My Project: </strong><?php echo $row['dsc']; ?>
                        <p><strong>Funding: </strong><?php echo $row['DescriptionFunds']; ?><br />
                  
						<p id="updates"><strong>Updates:</strong></p>

						<?php
						if (!empty($statuses)):
							foreach ($statuses as $status) {
								?>
								<div class="updates">
									<p><?php echo $status['StatusMessage']; ?></p>
									<span class="updatesDate"><strong><?php echo date('m/d/y', strtotime($status['DateCreated'])); ?></strong></span>
								</div>
								<?php
							}
						else:
							echo "No updates yet.";
						endif;
						?> 

						<!--<div class="updatesBottom">
						   <div class="updatesBottom-in">
							   <p><strong><span class="updatesBottom-in-name">John Doe</span></strong> from 
								   <span class="updatesBottom-in-place">New York</span></p>
						   </div>
						   <div class="updatesBottom-in-content"><p>"Aliquam ultrices nisl vel lacus aliquet a pharetra justo feugiat."</p>
						   </div>
						</div>
						<div class="updatesBottom">
						   <div class="updatesBottom-in">
							   <p><strong><span class="updatesBottom-in-name">John Doe</span></strong> from 
								   <span class="updatesBottom-in-place">New York</span></p>
						   </div>
						   <div class="updatesBottom-in-content"><p>"Aliquam ultrices nisl vel lacus aliquet a pharetra justo feugiat."</p>
						   </div>
						</div>
						
										 
                        <!-- Column 1 end -->

                    </div>
					<div class="col2">
                        <!-- Column 2 start -->
                        <div id="donatePanelWhite">
							<div id="donatePanel">
								<form name="form1" method="post" action="<?php echo base_url(); ?>student-needs/donation/<?php echo $row['NeedID']; ?>">
									<input type="text" name="donateValue" id="donateValue" placeholder="$" onkeypress="return isNumberKey(event)">
									<input type="submit" name="donateButton" id="donateButton" value="DONATE"><br />
								</form>
								<br>
								<?php $count = $CI->need->countDonor($row['NeedID']); ?>
								<?php
								if ($count['ctr'] == 1) {
									echo $count['ctr'] . " donor";
								} else {
									echo $count['ctr'] . " donors";
								}
								?>
								| 
								<?php
								$requiredDate = strtotime($row['DateNeeded']);
								$timeleft = $requiredDate - time();
								$daysleft = round((($timeleft / 24) / 60) / 60);

								if ($daysleft > 1) {
									echo $daysleft . " days to go!";
								} else if ($daysleft < -1) {
									$d = $daysleft * -1;
									echo $d . " days ago";
								} else if ($daysleft == 1) {
									echo $daysleft . " day to go";
								} else if ($daysleft == -1) {
									$d = $daysleft * -1;
									echo $d . " day ago";
								} else if ($daysleft == 0) {
									echo "Today";
								}
								?>
								<div class="meter-need" id="bar-<?php echo $row['HasMediaID']; ?>" name="<?php echo $row['HasMediaID']; ?>">
									<div class="meter-inner-need">
										<span style="display:block"><strong>$<?php echo number_format($need['DonatedAmount']); ?></strong></span>				
									</div> 
									<script type="text/javascript">	              		
										$(function(){
											$("#torch-"+<?php echo $row['HasMediaID']; ?>).each(function(){
												var width = $('#bar-'+<?php echo $row['HasMediaID']; ?>).width();
												var perc = $('.percent-'+<?php echo $row['HasMediaID']; ?>).val();
									
												if(perc == 1){
													$(this).css({
														'margin-left': '11px',
													});
												}
									
												$(this)
												.data("origWidth", $(this).width())
												.width(0)
												.animate({
													width: width * perc
												}, 1200);
											});
								
											$("#bar-"+<?php echo $row['HasMediaID']; ?>+" > .meter-inner-need > span").each(function() {
												var width = $('#bar-'+<?php echo $row['HasMediaID']; ?>).width();
												var perc = $('.percent-'+<?php echo $row['HasMediaID']; ?>).val();
									
												if(perc == 1){
													$(this).css({
														'-webkit-border-top-right-radius': '22px',
														'-webkit-border-bottom-right-radius': '22px',
														'background':'#41A317'	
													});
												}
									
												$(this)
												.data("origWidth", $(this).width())
												.width(0)
												.animate({
													width: width * perc
												}, 1200);
											});
										});
									</script> 	   			
								</div>
								<input type="hidden" class="percent-<?php echo $row['HasMediaID']; ?>" value="
                                <?php 
                                if($need['DonatedAmount'] <= $row['RequiredFunds']){
                                	echo 1 * ($need['DonatedAmount'] / $row['RequiredFunds']); 
                                }else if($need['DonatedAmount'] > $row['RequiredFunds']){
                                	echo 1 * ($row['RequiredFunds'] / $row['RequiredFunds']); 
                                }
                              
                                ?>"/>
								<div class="clear"></div>
								<p class="donateRate">$0</p>
								<p class="donateRate right">$<?php echo number_format($row['RequiredFunds']); ?></p>
								<div class="torch-need" id="torch-<?php echo $row['HasMediaID']; ?>"><span class="torch-bg"></span></div>  
								<div class="clear"></div>
								<input id="shareThis" name="shareThis" class="sharebutton1" type="button" value="Share This" rel="https://www.facebook.com/sharer.php?u=<?= urlencode(base_url() . 'student-needs/view/' . $row['NeedID']) ?>" />
							</div>
                        </div>
						<div id="pastNeedsArea">
							<h3><a href="">Past Needs</a></h3>
								<?php foreach ($pasts as $past): ?>
								<div class="projectinfobox">
									<div class="projectinfothumbnails">
										<img src="<?= base_url() . 'images/needs/thumbs/thumbnail_' .$past['file'] ?>" alt="" style="height: 50px; width: 50px;"/>
									</div>
	                                <div class="projectinfoText">
										<a href="<?= base_url() . 'student-needs/view/' . $past['NeedID']?>"><?= $past['Title'] ?></a><br/>
										Raised: $<?= (int)$past['raised'] ?>
									</div>
	                            </div> 
							<?php endforeach; ?>
                        </div>
						<!-- Column 2 end -->
                    </div>

                </div>
            </div>
            <div>
				&nbsp;
            </div>        
            <!-- end of 2 columns -->           
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){    
		$('.sharebutton1').click(function(){
			window.open($(this).attr('rel'));
		}); 
	});
</script>