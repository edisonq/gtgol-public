<link rel='stylesheet' href="<?php echo base_url();?>style/cms.css" type="text/css" media="screen" />
<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
	   		<p>&nbsp;</p>
	    </div>
		<div id="fillform">
			<h2>Add Page</h2>		
			<form action="<?php echo base_url();?>admin/pages/save" method="post">
				<p>
					<label>
						Title: <br />
						<input type="text" name="PageTitle" class="formtext"/>
					</label>
				</p>				
				<p>
					<label>
						Permalink: <small><?php echo base_url();?></small><br />
						<input type="text" name="PageName" class="formtext"/>
					</label>
				</p>			
				<p>
					<label>
						Description: <br />
						<textarea name="PageDescription" class="formtextarea"></textarea>
					</label>
				</p>			
				<p>
					<label>
						Status: <br />
						<select name="IsActive">
							<option>- Select -</option>
							<option value="0">Draft</option>
							<option value="1">Live</option>
						</select>
					</label>
				</p>					
				<p>
					<label>
						HTML Content: <br />
						<textarea name="PageHTMLContent" class="htmlcontent"></textarea>
					</label>
				</p>			
				<p>
					<label>
						<span>&nbsp;</span>
						<input type="submit" value="Save" id="fillformbutton"/>	
						<a href="<?php echo base_url();?>admin/pages">Cancel</a>
					</label> 
				</p>
			</form>
		</div>
		<p>&nbsp;</p>
	</div>
</div>