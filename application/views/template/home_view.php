<div style="display: none;">
    <pre><?php //var_dump($almostdue) ?></pre>
    <pre><?php //var_dump($latest) ?></pre>
</div>
<div id="contentArea">
    <div id="content">
        <section class="contentTop-wrap">
            <div class="leftHeader">
                <div class="socialMedia" style="line-height: 27px;">&nbsp;
                    <!--ul>
                        <li class="fb"><a href="#"></a></li>
                        <li class="twiter"><a href="#"></a></li>
                        <li class="plus"><a href="#"></a></li>
                    </ul-->
                </div>
                <section class="slider">
                    <div class="feat-image" style="height: 222px">
						<?php
						if (!empty($featured['Photo'])) {
							?>
							<img class="featuredImage" src="<?php echo base_url(); ?>images/needs/featured/<?php echo $featured['Photo']; ?>" alt="content image" width="440"  >
							<?php
						} else {
							?>
							NEED HAS NO IMAGE SPECIFIED
						<?php } ?>

					</div>
                    <ul>
                        <!--<li class="active"><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li class="pause"><a href="#"></a></li>-->
						<li style="list-style: none;background: none repeat scroll 0 0 transparent;">&nbsp;</li>
                    </ul>
                </section>
            </div>  
            <section class="featured">
				<?php 
					if(empty($featured)) {
						echo 'No data yet';
					}
					else {
				?>
                <div class="header">
                    <h1><?= substr($featured['Title'], 0, 30) . (strlen($featured['Title']) > 30 ? '...' : '') ?></h1>
                    <p><?= substr($featured['Description'], 0, 280) . (strlen($featured['Description']) > 280 ? '...' : '') ?></p>
                </div>
                <div class="clear"></div>  
                <div class="donate">
					<?php
					$per = $featured['TotalDonation'] / $featured['RequiredFunds'];
					$w = 385;
					$tw = $per * $w;
					if ($tw > 385)
						$tw = 385;
					?>
                    <div class="meter">      	
                        <div class="meter-inner">
							<?php
							if ($tw > 50) {
								?>
								<span <?php if($featured['TotalDonation'] >= $featured['RequiredFunds']){ echo "style='background: rgb(65, 163, 23); display:block; -webkit-border-top-right-radius: 22px; -webkit-border-bottom-right-radius: 22px;'"; }else{ echo "style='display:block;'"; }?> >
									<strong>$<?= (int) $featured['TotalDonation'] ?></strong>
								</span>
								<?php
							} else {
								?>
								<span <?php if($featured['TotalDonation'] >= $featured['RequiredFunds']){ echo "style='background: rgb(65, 163, 23); display:block;'"; }else{ echo "style='display:block;'"; }?>>
									&nbsp;
								</span>
								<strong style="position: absolute; top: 0px; left: <?php echo $tw + 5; ?>px; color: black;">$<?= (int) $featured['TotalDonation'] ?></strong>
							<?php } ?>
                        </div>
                    </div>
                    <div class="torch"><span class="torch-bg"></span></div>   
                    <div class="btn-donate">
                        <p><a href="<?php echo base_url(); ?>student-needs/donation/<?php echo $featured['NeedID']; ?>">Donate</a></p>
                    </div>

                    <div class="range">
                        <span class="left">$0</span>
                        <span class="right">$<?= $featured['RequiredFunds'] ?></span>	
                    </div>
                    <div class="clear"></div>
                    <div class="learnMore">
                        <p><a href="<?php echo base_url(); ?>student-needs/view/<?= $featured['NeedID'] ?>">Learn More</a></p>
                    </div>
					<script type="text/javascript">	
						$(".meter > .meter-inner > span").each(function() {
							$(this)
							.data("origWidth", $(this).width())
							.width(0)
							.animate({
								width: <?php echo ($per * $w  > 385)? 385: $per * $w ; ?>
							}, 1200);
							
							
						});
						
						$(".torch").each(function(){
							$(this)
							.data("origWidth", $(this).width())
							.width(0)
							.animate({
								width: <?php echo ($per * $w  > 385)? 385: $per * $w ; ?>
							}, 1200);
						});
					</script>
                    <div class="clear"></div>
                </div>
				<?php } ?>
            </section>
            <div class="clear"></div>
        </section>
        <section class="mainContent">
           
				
				<?php $CI = & get_instance();?>
				<?php $this->load->view('layouts/search_form') ?>

                                    
            </aside>
            <section id="pageSection">
                <article class="leftCol">
                    <section class="headerTitle">
                        <h1>Most Recent Student Needs</h1>
                    </section>
					<?php
					$i = 0;
					foreach ($latest as $l) {
						if($i < 5) {
						?>
						<section class="bodyContent">
							<a href="<?php echo base_url(); ?>student-needs/view/<?php echo $l->NeedID; ?>"><h2><?php echo $l->Title; ?></h2></a>
							<?php if (!empty($l->Photo)) { ?>
								<?php error_reporting(0); ?>
								<?php if (getimagesize(APPPATH.'../images/needs/thumbs/'.$l->Photo)): ?>
									<img class="featuredImage" src="<?php echo base_url(); ?>images/needs/thumbs/<?php echo $l->Photo; ?>" alt="content image">
								<?php else: ?>
									<img class="featuredImage" src="images/blank.png" alt="content image">
								<?php endif; ?>
								<?php
							} else {
								?>
								<img class="featuredImage" src="images/blank.png" alt="content image">
							<?php } ?>
							<p class="title"><?php echo substr($l->Description, 0, 280) . (strlen($l->Description) > 280 ? '...' : ''); ?></p>
							<div class="clear"></div>
							<div class="meter-small recent<?php echo $l->NeedID; ?>">	 
								<div class="meter-inner-small">	
									<?php
									 $tDonation = ($l->TotalDonation ? $l->TotalDonation : 0);
									$req = $l->RequiredFunds;
									$per = ($tDonation / $req);
									$maxwidth = 225;
									$barw = 225 * $per;
									if ($barw > 225)
										$barw = 225;
									?>
									<?php if ($barw > 40) { ?>
										<span <?php if($l->TotalDonation >= $l->RequiredFunds){ echo "style='background: rgb(65, 163, 23);'"; }?>><strong>$<?php echo $l->TotalDonation ? $l->TotalDonation : 0; ?></strong></span>					
									<?php } else { ?>
										<span>&nbsp;</span>		
										<strong style="float: left; position: absolute; top: 0px; left: <?php echo $barw + 3; ?>px; color: black;">$<?php echo $l->TotalDonation ? $l->TotalDonation : 0; ?></strong>
									<?php } ?>

								</div>  
								
								<div class="torch-small l<?php echo $l->NeedID; ?>"><span class="torch-bg"></span></div>   									
								<script type="text/javascript">
									$(".meter-small.recent<?php echo $l->NeedID; ?> > .meter-inner-small > span,.progressBar-inner-meter > span").each(function() {
										$(this)
										.data("origWidth", $(this).width())
										.width(0)
										.animate({
											width: <?php echo $barw; ?>
										}, 1200);
										
										if(<?php echo $l->TotalDonation;?> >= <?php echo $l->RequiredFunds;?> ){
											$(this).css({
                                                        '-webkit-border-top-right-radius': '22px',
                                                        '-webkit-border-bottom-right-radius': '22px',
                                                        'background-color':'#41A31'
                                                    });
										}
									});
									$(".torch-small.l<?php echo $l->NeedID; ?>").each(function(){
										$(this)
										.data("origWidth", $(this).width())
										.width(0)
										.animate({
											width: <?php echo $barw; ?>
										}, 1200);
									});	
								</script>
							</div>
							<div class="donateBtn">
								<p><a href="<?php echo base_url(); ?>student-needs/donation/<?php echo $l->NeedID; ?>">Donate</a></p>
							</div>
							<div class="clear"></div>
							<p class="donateRate">$0</p>
							<p class="donateRate right">$<?php echo $l->RequiredFunds; ?></p>
							<div class="clear"></div>
							<a href="<?php echo base_url(); ?>student-needs/view/<?php echo $l->NeedID; ?>" class="moreBtn">Learn More</a>
							<span><?php echo $l->DaysLeft; ?> Days left</span>
							<div class="clear"></div>
						</section>
						<?php
						$i++;
						}
					}
					?>					
                    <div class="clear"></div>					
                    <div class="footerbg"></div>					
                </article>
                <article class="rightCol">
                    <section class="headerTitle">
                        <h1>Upcoming Deadlines</h1>
                    </section>
					<?php
					$i = 0;
					foreach ($almostdue as $a) {
						if($i < 5) {
						?>
						<section class="bodyContent">
							<a href="<?php echo base_url(); ?>student-needs/view/<?php echo $a->NeedID; ?>"><h2><?php echo $a->Title; ?></h2></a>
							<?php if (!empty($a->Photo)) { ?>
								<?php if (!file(base_url().'images/needs/thumbs/'.$l->Photo)): ?>
									<?php error_reporting(0); ?>
									<?php if (getimagesize(APPPATH.'../images/needs/thumbs/'.$l->Photo)): ?>
										<img class="featuredImage" src="<?php echo base_url(); ?>images/needs/thumbs/<?php echo $l->Photo; ?>" alt="content image">
									<?php else: ?>
										<img class="featuredImage" src="images/blank.png" alt="content image">
									<?php endif; ?>
								<?php else: ?>
									<img class="featuredImage" src="images/blank.png" alt="content image">
								<?php endif; ?>
								<?php
							} else {
								?>
								<img class="featuredImage" src="images/blank.png" alt="content image">
							<?php } ?>
							<p class="title"><?php echo substr($a->Description, 0, 280) . (strlen($a->Description) > 280 ? '...' : ''); ?></p>

							<div class="meter-small  almost<?php echo $a->NeedID; ?>">
								<div class="meter-inner-small">
									<?php
									$tDonation = ($a->TotalDonation ? $a->TotalDonation : 0);
									$req = $a->RequiredFunds;
									$per = ($tDonation / $req);
									$maxwidth = 225;

									$barw = 225 * $per;
									if ($barw > 225)
										$barw = 225;
									?>
									<?php if ($barw > 40) { ?>
										<span <?php if($a->TotalDonation >= $a->RequiredFunds){ echo "style='background: rgb(65, 163, 23);'"; }?>><strong>$<?php echo $a->TotalDonation ? $a->TotalDonation : 0; ?></strong></span>				
									<?php } else { ?>
										<span>&nbsp;</span>		
										<strong style="float: left; position: absolute; top: 0px; left: <?php echo $barw + 3; ?>px; color: black;">$<?php echo $a->TotalDonation ? $a->TotalDonation : 0; ?></strong>
									<?php } ?>
								</div>      	
								<div class="torch-small a<?php echo $a->NeedID; ?>"><span class="torch-bg"></span></div>
								<script type="text/javascript">
									$(".meter-small.almost<?php echo $a->NeedID; ?> > .meter-inner-small > span,.progressBar-inner-meter > span").each(function() {
										$(this)
										.data("origWidth", $(this).width())
										.width(0)
										.animate({
											width: <?php echo $barw; ?>
										}, 1200);
										
										if(<?php echo $a->TotalDonation;?> >= <?php echo $a->RequiredFunds;?> ){
											$(this).css({
                                                        '-webkit-border-top-right-radius': '22px',
                                                        '-webkit-border-bottom-right-radius': '22px',
                                                        'background-color':'#41A31'
                                                    });
										}
									});
									$(".torch-small.a<?php echo $a->NeedID; ?>").each(function(){
										$(this)
										.data("origWidth", $(this).width())
										.width(0)
										.animate({
											width: <?php echo $barw; ?>
										}, 1200);
									});	
								</script>
							</div>

							<div class="donateBtn">
								<p><a href="<?php echo base_url(); ?>student-needs/donation/<?php echo $a->NeedID; ?>">Donate</a></p>
							</div>
							<div class="clear"></div>
							<p class="donateRate">$0</p>
							<p class="donateRate right">$<?php echo $a->RequiredFunds; ?></p>
							<div class="clear"></div>
							<a href="<?php echo base_url(); ?>student-needs/view/<?php echo $a->NeedID; ?>" class="moreBtn">Learn More</a>
							<span><?php echo $a->DaysLeft; ?> Days left</span>
							<div class="clear"></div>
						</section>
						<?php
						$i++;

						}
					}
					?>
                    <div class="clear"></div>
                    <div class="footerbg"></div>
                </article>
                <div class="clear"></div>
				<a href="<?= base_url('student-needs') ?>" class="viewAll" style="margin-left:100px;">View More Student Needs</a>
                <section class="sectionFooter">
                    <h1>Success Stories – Giving Liberty Changes Lives</h1>
					<?php
						if(empty($success)) {
							echo 'No stories yet';
						}
						else {
					?>
                    <ul>
                        <li>
                            <div class="footerBody">
                                <img src="<?php echo base_url(); ?>/images/success-stories/primary-photo/<?= $success[0]['MediaURL']; ?>" alt="" style="width:350px; height:150px;"/>
                                <h2><?= $success[0]['Beneficiary'] ?></h2>
                                <h2><?= $success[0]['Title'] ?></h2>
                                <h2>Amount Raised: <span>$<?= $success[0]['RequiredFunds'] ?></span></h2>								
                                <p><?= substr($success[0]['Description'], 0, 200) . (strlen($success[0]['Description']) > 200 ? '...' : '') ?></p>
                                <a href="<?= base_url('student-needs/view/' . $success[0]['NeedID']) ?>" class="moreBtn">View More</a>
                            </div>
                        </li>
                        <li>
                            <div class="footerBody">
								<?php
									if(empty($success[1])) {
										echo ' ';
									}
									else {
								?>
                                <img src="<?php echo base_url(); ?>/images/success-stories/primary-photo/<?= $success[1]['MediaURL']; ?>" alt="" style="width:350px; height:150px;"/>
                                <h2><?= $success[1]['Beneficiary'] ?></h2>
                                <h2><?= $success[1]['Title'] ?></h2>
                                <h2>Amount Raised: <span>$<?= $success[0]['RequiredFunds'] ?></span></h2>								
                                <p><?= substr($success[1]['Description'], 0, 200) . (strlen($success[1]['Description']) > 200 ? '...' : '') ?></p>
                                <a href="<?= base_url('student-needs/view/' . $success[1]['NeedID']) ?>" class="moreBtn">View More</a>
                                <div class="clear"></div>
                                <a href="<?= base_url('success-stories') ?>" class="viewAll">View all success stories</a>
								<?php } ?>
							</div>
                        </li>                
                    </ul>
					<?php } ?>
                </section>          
            </section>
            <div class="clear"></div>
        </section>
    </div>
</div>
<script type="text/javascript">
    $('.search-needs').click(function(){
        $('.submit').click();
    });
</script>