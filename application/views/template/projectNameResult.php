<div id="contentArea">
	<div id="socialSpace">
     &nbsp;
    </div>
  	<div id="content">
	  	<div id="contentHeader">
	    	<h1>Student Needs</h1>
	    </div>
<!-- 3 columns -->
		<div class="clear"></div>
		<section class="mainContent">
			<aside id="pageAside">
	          <div class="advertise">
	            <h1>Ad Goes Here 200 x ...</h1>
	          </div>
	          <div class="date">
	            <form action="<?php echo base_url();?>student-needs/search" method="get">
	              <h1>Sort By:</h1>
	              <input type="radio" name="sort" value="deadline">Deadline Date<br> 
	              <input type="radio" name="sort" value="created" checked>Date Created	            
		          </div>
		          <div class="school">
		            <p>School</p>
		            <input class="inputSchool" type="text" name="schoolName" placeholder="Type  in school name here">
		            <p>Need Type</p>	             
		              	<ul>
		                <li><input type="radio" name="needType" value="1">Travel Scholarships</li>
		                <li><input type="radio" name="needType" value="2">Protests</li>
		                <li><input type="radio" name="needType" value="3">Campus Events</li>
		                <li><input type="radio" name="needType" value="4">Books</li>
		                <li><input type="radio" name="needType" value="5">Other Group Supplies</li>
		                </ul>
		            <p>State/Country</p>
		            <select class="selectState">
		              <option selected="selected" value="volvo">MD</option>
		              <option value="saab">Saab</option>
		              <option value="vw">VW</option>
		            </select>
		            <select class="selectState">
		              <option selected="selected" value="volvo">United States of America</option>
		              <option value="saab">Philippines</option>
		              <option value="vw">Brazil</option>s
		            </select>
		            <p>Search Tags</p>
		            <input class="searchTag" type="text" name="searchTag">
		            <input type="submit" value="Submit" style="display:none;" class="submit"/>
	            </form>             
	          </div>
	          <div class="clear"></div>
	          <div class="searchBtn">
	            <p><a href="javascript:void(0);" class="search-needs">Search Needs</a></p>
	          </div>
	          <div class="clear"></div>
	          <div class="borderBottom"></div>
	          <div class="advertise advertise--2">
	            <h1>Ad Goes Here 200 x ...</h1>
	          </div>
	          <div class="advertise advertise--2">
	            <h1>Ad Goes Here 200 x ...</h1>
	          </div>
	          <div class="advertise advertise--2">
	            <h1>Ad Goes Here 200 x ...</h1>
	          </div>                    
        </aside>
        <section id="pageSection">
		<?php 
        	$CI =& get_instance();
			$CI->load->model('studentNeeds_model','need');
			
			if(!empty($needs)):
			foreach($needs as $row):
				$DescriptionLength = strlen($row['dsc']);
	            $sneedDescription = preg_replace('/([^>\r\n]?)(\r\n|\n\r|\r|\n)/','<br />', $row['dsc']);
		?>
		<input id="description<?php echo $row['NeedID'];?>" type="hidden" value="<?php echo $sneedDescription;?>"/>
        	<div class="project">
        		<div class="project-left">
        			<div class="project-left-image">
        				<img src="<?php echo base_url();?>images/needs/thumbs/thumbnail_<?php echo $row['MediaURL'];?>"/>
        			</div>
        			<div class="project-left-details">
        				<span>Created: <?php echo date('m/d/y',strtotime($row['DateCreated']));?></span><br />
        				<span>Deadline: <?php echo date('m/d/y',strtotime($row['DateNeeded']));?></span>
        			</div>
        		</div>
        		<div class="project-center">
        			<div class="project-center-title"><a href="<?php echo base_url();?>student-needs/view/<?php echo $row['NeedID'];?>"><?php echo $row['Title'];?></a></div>
        			<div class="project-center-body">
                                <p id="needDescription<?php echo $row['NeedID']; ?>" class="needDescription"><?php
				                echo substr($sneedDescription, 0, 205);
				                if ($DescriptionLength > 205) {
				                        ?>
                                        ...<a href="javascript:void(0);" onclick="readMore(<?php echo $row['NeedID']; ?>);">more</a>
                                    <?php } ?>
    						</p><!--<span class="more">more</span>-->
                            </div>
        			<div class="project-center-school">
        				<strong>School: </strong><?php echo $row['Beneficiary'];?>
        			</div>
        			<div class="meter-need" id="bar-<?php echo $row['HasMediaID'];?>" name="<?php echo $row['HasMediaID'];?>">
        			
					<?php 
						$need = $CI->need->getDonations($row['NeedID']);
						if(!empty($need)){
        			?>
		            	<div class="meter-inner-need">
							<span><strong>$<?php echo number_format($need['DonatedAmount']);?></strong></span>				
		              	</div>           	              	
		              	<script type="text/javascript">	              		
		              		$(function(){
			              		$("#torch-"+<?php echo $row['HasMediaID'];?>).each(function(){
									var width = $('#bar-'+<?php echo $row['HasMediaID'];?>).width();
									var perc = $('.percent-'+<?php echo $row['HasMediaID'];?>).val();
									
									if(perc == 1){
										$(this).css({
											'margin-left': '11px',
										});
									}
									
									$(this)
										.data("origWidth", $(this).width())
										.width(0)
										.animate({
											width: width * perc
										}, 1200);
								});
								
								$("#bar-"+<?php echo $row['HasMediaID'];?>+" > .meter-inner-need > span").each(function() {
									var width = $('#bar-'+<?php echo $row['HasMediaID'];?>).width();
									var perc = $('.percent-'+<?php echo $row['HasMediaID'];?>).val();
									
									if(perc == 1){
										$(this).css({
											'-webkit-border-top-right-radius': '22px',
											'-webkit-border-bottom-right-radius': '22px'	
										});
									}
									
									$(this)
										.data("origWidth", $(this).width())
										.width(0)
										.animate({
											width: width * perc
										}, 1200);
								});
							});
		              	</script> 	   			
					</div>
					<input type="hidden" class="percent-<?php echo $row['HasMediaID']; ?>" value="
                                <?php 
                                if($need['DonatedAmount'] <= $row['RequiredFunds']){
                                	echo 1 * ($need['DonatedAmount'] / $row['RequiredFunds']); 
                                }else if($need['DonatedAmount'] > $row['RequiredFunds']){
                                	echo 1 * ($row['RequiredFunds'] / $row['RequiredFunds']); 
                                }
                              
                                ?>"/>
					<div class="torch-need" id="torch-<?php echo $row['HasMediaID'];?>"><span class="torch-bg"></span></div>  
					<?php }else{ ?>
							<div class="meter-inner-need">
								<span><strong>$0</strong></span>				
			              	</div>
			              	</div>
							<input type="hidden" class="percent-<?php echo $row['HasMediaID'];?>" value=""/>
							<div class="torch-need" id="torch-<?php echo $row['HasMediaID'];?>"><span class="torch-bg"></span></div>  
					<?php } ?>		
					<div class="days-to-go"><?php 
						$requiredDate = strtotime($row['DateNeeded']);
						$timeleft = $requiredDate-time();
						$daysleft = round((($timeleft/24)/60)/60);
						
						if($daysleft > 1){
							echo $daysleft." days to go!";
						}else if($daysleft < -1){
							$d = $daysleft * -1;
							echo $d." days ago";
						}else if($daysleft == 1){
							echo $daysleft." day to go";
						}else if($daysleft == -1){
							$d = $daysleft * -1;
							echo $d." day ago";
						}else if($daysleft == 0){
							echo "Today";
						}
						
					?></div>
					<div class="clear"></div>      
	              	<div class="range">
	              		<span id="range-left">$0</span>
	              		<span id="range-right">$<?php echo number_format($row['RequiredFunds']);?></span>
	              	</div>
        		</div>
        		<div class="project-right">
        			<div id="donatePanel">
                    	<form method="post" action="<?php echo base_url();?>student-needs/donation/<?php echo $row['NeedID'];?>">
                        	<input type="text" name="donateValue" id="donateValue" placeholder="$">
                            <input type="submit" name="donateButton" id="donateButton" value="DONATE">
                        </form>
                        <?php $count = $CI->need->countDonor($row['NeedID']); ?>
                		<div id="donate-donors">
                			<?php echo $count['ctr'];?> donors
                		</div>
                       <input id="shareThis" name="shareThis" type="button" value="Share This">
                    </div>
        		</div>
        	</div> 
      	<?php 
				endforeach;
				else:
					echo "<strong style='color:#900;'>No results found</strong>";
			endif;
		?>
        	<div id="pagination">

        	</div>  
        	<div class="clear"></div>
        </section>
        <div class="clear"></div>
		</section>
		<p>&nbsp;</p>
	</div>
</div>
<script type="text/javascript">
	$('.search-needs').click(function(){
		$('.submit').click();
	});
</script>