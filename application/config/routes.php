<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

/* User routes */
$route['login'] = "user/login";
$route['create-account'] = "user/createAccount";
$route['welcome'] = "user/welcomeUser";
$route['logout'] = "user/LogOut";
$route['usercalls/LoginUser'] = "usercalls/LoginUser";  // how to add the ajax call
$route['usercalls/Register'] = 'usercalls/Register';
$route['usercalls/CheckUsername'] = 'usercalls/CheckUsername';
$route['usercalls/CheckEmail'] = 'usercalls/CheckEmail';

/* Search routes */
$route['search/Basic'] = "search/Basic"; // some ajax call
$route['custom-search'] = "studentNeeds/customSearch";

/* How it Works routes */
//$route['how-it-works'] = "howItWorks";

/* Why Give routes*/
//$route['why-give'] = "whyGive";

/* Student Needs*/
$route['student-needs'] = "studentNeeds";
$route['student-needs/view/(:num)'] = "studentNeeds/viewSingleNeeds/$1";
$route['student-needs/donation/(:num)'] = "studentNeeds/donation/$1";
$route['student-needs/donation/save/(:num)'] = "studentNeeds/addDonations/$1";
$route['student-needs/donation/validate'] = "studentNeeds/validateDonation";
$route['student-needs/needs'] = "studentNeeds/viewPage";
$route['student-needs/search'] = "studentNeeds/searchStudentNeeds";
$route['student-needs/search-student-needs'] = "studentNeeds/viewSSNPage";
$route['student-needs/custom-search'] = "studentNeeds/projectNameResults";

/* Success Stories */
$route['success-stories'] = "successStories";
$route['complete-success-story/(:num)'] = "successStories/completeSuccessStory/$1";
$route['submit-complete-story'] = "successStories/submitCompleteStory";
$route['success-stories/stories'] = "successStories/viewPage";

/* About SFL */
//$route['about-sfl'] = "aboutSFL";

/* Dynamic pages */
$route['(:any)'] = "dynamic/pages/$1";

/* Admin CMS main*/
$route['admin-cms'] = "adminCMS";
$route['admin/dashboard'] = "adminCMS/dashboard";

$route['admin'] = "admin";
$route['admin/logout'] = "usercalls/AdminLogout";
$route['usercalls/LoginAdmin'] = 'usercalls/LoginAdmin';
$route['admin/needs'] = "admin/dashboardAllNeeds";
$route['admin/needs/pending'] = "admin/dashboardPendingNeeds";
$route['admin/needs/edit'] = "admin/dashboardEditNeeds";
$route['admin/needs/update'] = "admin/dashboardUpdateNeeds";
$route['admin/needs/approved'] = "admin/dashboardApprovedNeeds";
$route['admin/needs/setfeatured'] = "admin/dashboardSetFeaturedNeeds";
$route['admin/needs/author'] = "adminApproval/author";
$route['admin/donations'] = "admin/dashboardDonations";
$route['admin/stories'] = "admin/dashboardStories";
$route['admin/stories/approve'] = "admin/dashboardApproveStories";
$route['admin/status'] = "admin/dashboardStatus";
$route['admin/download-allneeds'] = "admin/downloadNeeds";
$route['admin/download-donation'] = "admin/downloadDonation";
$route['admin/media'] = "admin/dashboardMedia";

/* Admin CMS pages */
$route['admin/pages'] = "adminCMS/pages";
$route['admin/pages/create'] = "adminCMS/addPage";
$route['admin/pages/save'] = "adminCMS/savePage";
$route['admin/pages/show'] = "adminCMS/showSinglePage";
$route['admin/pages/edit'] = "adminCMS/editPage";
$route['admin/pages/update'] = "adminCMS/updatePage";
$route['admin/pages/delete'] = "adminCMS/deletePage";

/* Admin CMS menu */
$route['admin/menu'] = "adminCMS/menu";
$route['admin/menu/create'] = "adminCMS/addMenu";
$route['admin/menu/save'] = "adminCMS/saveMenu";
$route['admin/menu/pages/create'] = "adminCMS/addMenuPages";
$route['admin/menu/pages/save'] = "adminCMS/saveMenuPages";
$route['admin/menu/delete'] = "adminCMS/deleteMenu";
$route['admin/menu/pages/remove'] = "adminCMS/removeMenuPage";

/* Admin CMS group */
$route['admin/group'] = "adminCMS/group";
$route['admin/group/menu/add'] = "adminCMS/addGroupMenu";
$route['admin/group/menu/update'] = "adminCMS/updateGroupMenu";
$route['admin/group/menu/remove'] = "adminCMS/removeGroupMenu";

/*SFL admin */
$route['admin-approval/dashboard'] = "adminApproval/dashboard";
$route['admin-approval/pending-need'] = "adminApproval/approvePendingNeed";
$route['admin-approval/pending-need/decline'] = "adminApproval/declinePendingNeed";
$route['admin-approval/pending-status'] = "adminApproval/approvePendingStatus";
$route['admin-approval/dashboard/needs'] = "adminApproval/dashboardNeeds";
$route['admin-approval/dashboard/stories'] = "adminApproval/dashboardStories";
$route['admin-approval/dashboard/status'] = "adminApproval/dashboardStatus";
$route['admin-approval/dashboard/needs/all'] = "adminApproval/dashboardAllNeeds";
$route['admin-approval/dashboard/needs/pending'] = "adminApproval/dashboardPendingNeeds";
$route['admin-approval/dashboard/needs/approved'] = "adminApproval/dashboardApprovedNeeds";
$route['admin-approval/dashboard/needs/edit'] = "adminApproval/dashboardEditNeeds";
$route['admin-approval/dashboard/needs/update'] = "adminApproval/dashboardUpdateNeeds";
$route['admin-approval/dashboard/approve-stories'] = "adminApproval/dashboardApproveStories";

/* My Account  */
$route['my-account/create-need'] = "myAccount/createNeed";
$route['my-account/completed-need'] = "myAccount/completedNeed";
$route['my-account/save-need'] = "myAccount/saveNeed";
$route['my-account/my-needs'] = "myAccount/myNeeds";
$route['my-account/my-needs/most-view'] = "myAccount/mostViewNeeds";
$route['my-account/crop-photos'] = "myAccount/cropPhotos";
$route['my-account/upload-thumb'] = "myAccount/resizeThumbnailImage";

/* My Needs */

//link to the update need page
$route['my-needs/edit-need'] = "myNeeds/myNeedEdit";
$route['my-needs/update-need'] = "myNeeds/myNeedUpdate";
$route['my-needs/update-need/(:num)'] = "myNeeds/updateNeed/$1";
$route['my-needs/delete-tag'] = "myNeeds/deleteTag";
$route['my-needs/update'] = "myNeeds/updateNeed";
$route['my-needs/update-status'] = "myNeeds/updateStatus";

/* End of file routes.php */
/* Location: ./application/config/routes.php */