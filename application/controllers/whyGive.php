<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class whyGive extends CI_Controller {
	
	public function index(){
		$styles = Array("how-it-works");
		$this->template->set('styles', $styles);
		$this->template->current_view = ('template/whyGive_view');
		$this->template->render();
	}
	
}
