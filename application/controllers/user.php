<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {
	function __construct() {  
		parent::__construct();
	}
	public function login() {	
		
		$scripts = Array("login");
		$styles = Array("login");
		$this->template->set('scripts', $scripts);
		$this->template->set('styles', $styles);
		$this->template->current_view = ('template/studentLogin_view');
		$this->template->render();
	}	
	
	public function createAccount(){
		$scripts = Array("login");
		$styles = Array("login");
		$this->template->set('scripts', $scripts);
		$this->template->set('styles', $styles);
		$this->template->set('scroll', "Hello World!");
		$this->template->current_view = ('template/studentLogin_view');
		$this->template->render();
	}
	
	public function welcomeUser() {
		$name = $_GET['name'];
		$this->template->set('name', $name);
		$this->template->current_view = ('template/welcomeuser_view');
		$this->template->render();
	}
	public function LogOut() {
		$userdata = array(
            'username'  => '',
            'ID'  => '',
            'logged_in' => ''
        );

		$this->session->unset_userdata($userdata);
		header('Location:'.base_url());
	}
	
	
}