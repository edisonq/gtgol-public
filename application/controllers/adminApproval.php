<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminApproval extends CI_Controller{
	
	function __construct(){
    	parent::__construct();
        $this->load->model('admincms_model','cms');
		$this->load->model('admincmsneed_model','acn');
		$this->load->model('admincmsneedstatus_model','acns');
		$this->load->model('admin_model','admin');
		$this->load->model('studentneedviews');
    }
	
	function index(){
		
	}
	
	function dashboard(){
		$this->template->current_view = ('template/adminApproval_view.php');
		$this->template->render('adminpanel');
	}
	/*function check1($nid) {
		$getNeed = $this->studentneedviews->getNeed($nid);
		echo "<pre>";
			var_dump($getNeed);
		echo "</pre>";
		echo "$getNeed->Title";
	}*/
	function approvePendingNeed(){
		$nid = $this->input->get('nid');
		$getEmail = $this->acn->getUserEmail($nid);
		$getNeed = $this->studentneedviews->getNeed($nid);
		$result = $this->acn->approvePendingNeeds($nid);
		if($result){
			$notify = $this->acn->notifyCreator($getEmail['user_email'],"Congratulations, \n$getNeed->Title has been approved.\n"."URL: ".base_url().'student-needs/view/'.$nid);
			$status = "success";
			$message = "Need: ".$nid." has been approved";
		}else{
			$status = "error";
			$message = "Need: ".$nid." has been denied";
		}
		
		$data = array('status' => $status,'message' => $message);
		echo json_encode($data);
		exit;
	}
	
	function declinePendingNeed(){
		$nid = $this->input->get('nid');
		$getEmail = $this->acn->getUserEmail($nid);
		$result = $this->acn->declinePendingNeeds($nid);
		if($result){
			$message  = 'Sorry, Your need was declined' . "\r\n"; 
			$message .= 'Reason: ' . $this->input->post('message');			
			$notify = $this->acn->notifyCreator($getEmail['user_email'], $message);
			//redirect(base_url().'admin-approval/dashboard/needs/pending');
			echo 'success';
		}else{
			echo '.!.';
		}
	}
	
	function author(){
		$this->template->set('author', $this->acn->author($this->input->get('author')));
		$this->template->current_view = ('template/adminAuthor_view.php');
		$this->template->render('adminpanel');
	}
	
	function approvePendingStatus(){
		$sid=$this->input->get('sid');
		$nid=$this->input->get('nid');
		$result=$this->acns->approvePendingStat($sid,$nid);
		if($result){
			$status='success';
			$message='Status ID: '.$sid.' has been approved';
		}else{
			$status-'error';
			$message='Status ID: '.$sid.' has been denied';
		}
		
		$data=array('status'=>$status,'message'=>$message);
		echo json_encode($data);
		exit;
	}

	function dashboardNeeds(){
		$this->template->current_view = ('template/adminNeed_view.php');
		$this->template->render('adminpanel');
	}
	
	function dashboardPendingNeeds(){
		$pendingNeeds = $this->acn->getPendingNeeds();
		$this->template->set('pendingNeeds',$pendingNeeds);
		$this->template->current_view = ('template/adminApprovalNeeds_view.php');
		$this->template->render('adminpanel');
	}
	
	function dashboardApprovedNeeds(){
		$approvedNeeds = $this->cms->getApprovedNeeds();
		$this->template->set('approve',$approvedNeeds);
		$this->template->current_view = ('template/adminApprovedNeeds_view.php');
		$this->template->render('adminpanel');
	}
	
	function dashboardAllNeeds(){
		$needs = $this->cms->getAllNeeds();
		$this->template->set('needs',$needs);
		$this->template->current_view = ('template/adminAllNeeds_view.php');
		$this->template->render('adminpanel');
	}
	
	function dashboardEditNeeds(){
	
		$nid = $this->input->get('nid');
		$ctgry = $this->cms->getAllCategory();
		$needs = $this->cms->getSingleNeeds($nid);
		$this->template->set('row',$needs);
		$this->template->set('ctgry',$ctgry);
		$this->template->current_view = ('template/adminUpdateNeed_view.php');
		$this->template->render('adminpanel');
	}

	function dashboardUpdateNeeds(){
		$data = $this->input->post();
		$result = $this->cms->updateNeed($data);
		redirect(base_url().'admin-approval/dashboard/needs/all');
	}
	
	function dashboardStories(){
		$result = $this->admin->getPendingStories();
		$this->template->set('pending',$result);
		$this->template->current_view = ('template/adminApprovalStories_view.php');
		$this->template->render('adminpanel');
	}
	
	function dashboardStatus(){
		$pendingStatus = $this->acns->getPendingNeedStatus();
		$this->template->set('pendingStatus',$pendingStatus);
		$this->template->current_view = ('template/adminApprovalStatus_view.php');
		$this->template->render('adminpanel');
	}
	
	function dashboardApproveStories(){
		$sid = $this->input->get('sid');
		$result = $this->admin->approveStories($sid);
		redirect(base_url().'admin-approval/dashboard/stories');
	}
	
}
