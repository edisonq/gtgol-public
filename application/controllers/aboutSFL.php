<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class aboutSFL extends CI_Controller {
	
	public function index(){
		$styles = Array("student-needs");
		$this->template->set('styles', $styles);
		$this->template->current_view = ('template/aboutSFL_view');
		$this->template->render();
	}
	
}
