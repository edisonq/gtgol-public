<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class myNeeds extends CI_Controller {
	
	function __construct() {  
		parent::__construct();
		$this->load->model('myAccount_model','my_account');
		$this->load->model('data_sanitizer_model','xss_clean');
		
		if (!$this->session->userdata('logged_in'))
		{
			 redirect(base_url().'login', 'refresh');
		}
		$this->load->model('myNeeds_model','my_needs');
	}
	function myNeedEdit(){
		$nid = $this->xss_clean->sanitize($this->input->get('nid'));
		$need = $this->my_needs->getNeedData($nid);
		$tags = $this->my_needs->getNeedTags($nid);
		$pic = $this->my_needs->getThumbnails($nid);
		$categories = $this->my_account->getCategories();
		$styles = array("completed_needs","additional-main","create_a_need","jquery-ui");
		$scripts = array("jquery.form","need","jquery-ui");
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('categories', $categories);
		$this->template->set('pic',$pic);
		$this->template->set('tags',$tags);
		if($need){
			$this->template->set('need',$need);
			$this->template->current_view = ('template/updateMyNeeds_view');
			$this->template->render();
		}else{
			echo "You're not allowed to access this page!";
		}
		
	}
	
	function updateNeed(){
		$data = $this->security->xss_clean($this->input->post());
		$title = $data['Title'];
		$beneficiary = $data['Beneficiary'];
		$description = $data['Description'];
		$requiredfunds = $data['RequiredFunds'];
		$descriptionfunds = $data['DescriptionFunds'];
		$dateneeded = $data['DateNeeded'];
		$categoryid = $data['CategoryID'];
		$ggol = $data['ggol_tagcol'];
		$MediaID = $data['MediaID'];
		
		if(empty($title) and empty($beneficiary) and empty($description) and empty($requiredfunds) and empty($descriptionfunds) and empty($dateneeded) and empty($categoryid)
			or empty($title) or empty($beneficiary) or empty($description) or empty($requiredfunds) or empty($descriptionfunds) or empty($dateneeded) or empty($categoryid)){
			$status = 'error';
			$message = 'Please fill up the missing fields';
		}else{
			$need = $this->my_needs->updateNeed($data);	
			if($need){
				$tag = $this->my_needs->saveTag($data);
				$media_need = $this->my_needs->uploadPhoto($MediaID);

				$status = 'success';
				$message = 'Successfully updated! Please wait...';
			}
		}

		$json = array('status' => $status, 'message' => $message);
		echo json_encode($json);
		exit;
	}

	function deleteTag(){
		$tid = $this->xss_clean->sanitize($this->input->get('tid'));
		$result = $this->my_needs->deleteTag($tid);
		if($result){ return true; }
		else{ return false; }
	}
	
	function updateStatus(){
		$data = $this->security->xss_clean($this->input->post());
		$needStatus = mysql_real_escape_string($data['Status']);
		
		if(empty($needStatus)){
			$status = "error";
			$message = "Please Fill up the missing field";
		}else{
			$updateStatus = $this->my_needs->saveNeedStatusUpdate($data);
			if($updateStatus){
				$status = "success";
				$message = "Updating need status...";
			}
		}
		
		$json = array('status' => $status, 'message' => $message);
		echo json_encode($json);
		exit;
	}
}