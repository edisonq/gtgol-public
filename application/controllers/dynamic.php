<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dynamic extends CI_Controller{
	
	function __construct(){
    	parent::__construct();
        $this->load->model('dynamic_model','dynamic');
    }
	
	function pages($page_name){
		$result = $this->dynamic->dynamic_pages($page_name);
		if($result){
			$styles = Array("how-it-works");
			$this->template->set('styles', $styles);
			$this->template->set('row', $result);
			$this->template->current_view = ('template/dynamicPage_view');
			$this->template->render();
		}else{
			$this->template->current_view = ('pageNotFound_view');
			$this->template->render();
		}
	}
	
}

