<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class myNeeds_model extends CI_Model {
	private $category;
	private $donations;
	private $needs;
	private $user;
	private $tags;
	private $media;
	private $nstatus;
	
	function __construct() {
		parent::__construct();
		$this->donations = "ggol_donations";
		$this->category = "ggol_category";
		$this->needs = "ggol_needs";
		$this->nstatus = "ggol_needstatus";
		$this->user = "ggol_users";
		$this->tags = "ggol_tag";
		$this->media = "ggol_media";
	}

	function getMyNeeds() {
		//$id = $this -> session -> userdata('ID');
		$id = 1;

		// find the ggol_user id using the Userid
		$q = "SELECT UserID FROM ggol_users WHERE SFLUserID= $id";
		$r = $this -> db -> query($q);
		$theresult = $r -> result();
		$id = $theresult[0] -> UserID;
		// of finding the ggol_user

		$result = $this -> db -> order_by('NeedID', 'desc') -> get_where($this -> needs, array('UserID' => $id));
		return $result -> result_array();
	}

	function getCategory($cid) {
		$query = $this -> db -> get_where($this -> category, array('CategoryID' => $cid));
		$result = $query -> row_array();
		$category = $result['CategoryName'];
		return $category;
	}

	function totalDonatedAmount($nid) {
		//DA = Donated Amount
		$this -> db -> select_sum('DonatedAmount');
		$sumDA = $this -> db -> get_where($this -> donations, array('NeedID' => $nid));
		$result = $sumDA -> row_array();
		$sumDA = $result['DonatedAmount'];
		return $sumDA;
	}

	function ifTargetReached($nid, $isApproved, $requiredFunds) {
		//DA = Donated Amount
		$sumDA = $this -> totalDonatedAmount($nid);
		if ($isApproved == 1 && $sumDA == $requiredFunds) {
			return 1;
		}
	}

	function getThumbnails($needID) {
		$q = "SELECT MediaID FROM ggol_media_has_ggol_needs WHERE NeedID=$needID";
		$r = $this -> db -> query($q);
		$result = $r -> result();
		if (!empty($result[0] -> MediaID)) {
			$q = "SELECT MediaName,MediaID FROM ggol_media WHERE MediaID=" . $result[0] -> MediaID;
			$r = $this -> db -> query($q);
			$result = $r -> result();
		} else {
			// Array ( [0] => stdClass Object ( [MediaName] => 17411_1210946230739_1352895370_n.jpg [MediaID] => 15 ) )
			// the line below creates this: Array ( [0] => stdClass Object ( [MediaName] => box.jpg [MediaID] => 0 ) )
			$objectArray = (object) array('MediaName' => 'box.jpg', 'MediaID' => false);
			$result = array(0 => $objectArray);
		}

		return $result;
	}

	function getNeedData($nid) {
		$this -> db -> select('c.*,n.*');
		$this -> db -> from('ggol_needs as n');
		$this -> db -> where('n.NeedID', $nid);
		$this -> db -> join('ggol_category as c', 'c.CategoryID = n.CategoryID', 'left');
		$result = $this -> db -> get();
		return $result -> row_array();
	}

	function getNeedTags($nid) {
		$this->db->where('NeedID',$nid);
		$result = $this->db->get($this->tags);
		return $result->result_array();
	}
	
	function deleteTag($tid){
		$this->db->where('TagID',$tid);
		$result = $this->db->delete($this->tags);
		return $result;
	}

	function updateNeed($data) {
		$DateNeeded = date("Y-m-d H:i:s", strtotime($data['DateNeeded']));
		$DateModified = date("Y-m-d H:i:s");
		$UserID = $data['UserId'];

		// find the ggol_user id using the Userid
		//temporary
		// of finding the ggol_user

		$need = array(
			'Title' => $data['Title'], 
			'Beneficiary' => $data['Beneficiary'], 
			'Description' => $data['Description'], 
			'RequiredFunds' => $data['RequiredFunds'], 
			'DescriptionFunds' => $data['DescriptionFunds'], 
			'DateNeeded' => $DateNeeded, 
			'DateModified' => $DateModified, 
			'CategoryID' => $data['CategoryID'] 
		);
		
		$this->db->where('NeedID',$data['NeedID']);
		$result = $this->db->update($this->needs, $need);
		if ($result) {
			return $data['NeedID'];
		} else {
			return false;
		}
	}
	
	function saveTag($data){
		if(!empty($data['ggol_tagcol'])){
			$DateCreated = date("Y-m-d H:i:s");
			$DateModified = date("Y-m-d H:i:s");
			$tagArray = explode(',',$data['ggol_tagcol']);
			
			foreach ($tagArray as $tagdata) {
				$tag = array(
					'NeedID' => $data['NeedID'],
					'ggol_tagcol' => $tagdata,
					'DateCreated' => $DateCreated,
					'DateModified' => $DateModified
				);
				$result = $this->db->insert($this->tags,$tag);
			}
			return $result;
		}		
	}
	
	function _generate_image_name(){
    	return rand().'_'.rand(1,1000000).rand(1111111,9999999).'_'.time().'_n';            
    } 
	
	function uploadPhoto($mid){
    	$config = array(
    		'allowed_types' => 'jpg|jpeg|gif|png',
            'upload_path' => realpath(APPPATH .'../images/needs'),
            'file_name' => $this->_generate_image_name(),
            'max_size' => 2000
        );
            
        $this->load->library('upload', $config);
        if($this->upload->do_upload()){
        	$image_data = $this->upload->data();
                
            $config = array(
            	'source_image' => $image_data['full_path'],
                'new_image' => 'images/needs/banners',
                'create_thumb'=>FALSE,
                'maintain_ratio' => TRUE,
                'width' => 710,
                'height' => 488
            );
            
            $this->load->library('image_lib', $config);     
            $this->image_lib->initialize($config);
                
            if (!$this->image_lib->resize()){
            	unlink('images/needs/'.$image_data['file_name']);
                return FALSE;
            }else{
               
                $picture = $this->_get_old_photo($mid);
                if(!empty($picture)){
                	if(file_exists('images/needs/banners/'.$picture)) {
                    	unlink('images/needs/banners/'.$picture);
                    }
                }
                    
                $result = $this->updateCoverPhoto($mid,$image_data['file_name']);
                unlink('images/needs/'.$image_data['file_name']);
                return TRUE;
            }       
        }else{
        	return FALSE;
        }
	}

	function _get_old_photo($mid) {
    	$picture = '';
       	$this->db->where('MediaID',$mid);
        $query = $this->db->get($this->media);
            
        if($query->num_rows() > 0 ){
         	$row = $query->row_array();
            $picture = $row['MediaURL']; 
        }
       	return $picture;  
    }

    function updateCoverPhoto($mid,$image_path){
		$DateModified = date("Y-m-d H:i:s");
		
    	$data = array(
        	'MediaName'=> $image_path,
        	'MediaURL' => $image_path,
        	'DateModified' => $DateModified
       	);
        
		$this->db->where('MediaID',$mid);           
        $result = $this->db->update($this->media,$data);
        return $result;
	}
	
	function saveNeedStatusUpdate($nid,$status){	
		$DateCreated = date("Y-m-d H:i:s");
		
		$data = array(
        	'StatusMessage'=> $status,
        	'DateCreated' => $DateCreated,
        	'DateModified' => $DateCreated,
			'NeedID' => $nid
       	);
		
		$result = $this->db->insert($this->nstatus,$data);
		return $result;
	}
	
	function checkUserOwnsNeed($id)
	{
		return 0;
	}

}
