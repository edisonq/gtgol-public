<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
    	parent::__construct();
		$this->load->model('studentneedviews', 'needs');
		$this->load->model('admincms_model','cms');
		$this->load->model('admincmsneed_model','acn');
		$this->load->model('admincmsneedstatus_model','acns');
		$this->load->model('admin_model','admin');
		$this->load->model('data_sanitizer_model','xss_clean');
		//$this->output->enable_profiler(TRUE);
	}
	function index(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$this->template->set('topneeds',$this->needs->getTopNeeds());
			$this->template->set('topdonors',$this->needs->getTopDonator());
			$this->template->set('unapprovedneeds', $this->acn->getPendingNeeds());
					
			
			$this->template->current_view = ('template/admin/dashboard');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	function dashboardAllNeeds(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
		
			$needs = $this->cms->getAllNeeds();
			$this->template->set('needs',$needs);
			$this->template->current_view = ('template/admin/allneeds');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	function dashboardPendingNeeds(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
		
			$pendingNeeds = $this->acn->getPendingNeeds();
			$this->template->set('pendingNeeds',$pendingNeeds);
			$this->template->current_view = ('template/admin/pendingneeds');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		} 
		
	}
	function dashboardEditNeeds(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
		
			$nid = $this->input->get('nid');
			$ctgry = $this->cms->getAllCategory();
			$needs = $this->cms->getSingleNeeds($nid);
			$this->template->set('row',$needs);
			$this->template->set('ctgry',$ctgry);
			$this->template->current_view = ('template/admin/needupdate');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
		
	}
	function dashboardStatus(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
		
			$pendingStatus = $this->acns->getPendingNeedStatus();
			$this->template->set('pendingStatus',$pendingStatus);
			$this->template->current_view = ('template/admin/status');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	function dashboardUpdateNeeds(){
		$data = $this->xss_clean->sanitize_script_tag($this->input->post());
		$result = $this->cms->updateNeed($data);
		redirect(base_url().'admin/needs');
	}
	function dashboardApprovedNeeds(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			
			$approvedNeeds = $this->cms->getApprovedNeeds();
			$this->template->set('approve',$approvedNeeds);
			$this->template->current_view = ('template/admin/approvedneeds');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function dashboardSetFeaturedNeeds(){
		if ($this->session->userdata('logged_in_admin')){
			$id = $this->input->get('NeedID');
			$this->cms->setFeatured($id);
			redirect($_SERVER['HTTP_REFERER']);
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function dashboardDonations() {
		//getAllDonations
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			
			// This query is absolutely abhorrent and needs to be rewritten.
            // select *,(SELECT CONCAT(lastname, ', ', firstname) as Fullname FROM userprofileview as upv where upv.UserID = dv.UserID) as Fullname, (select Title from ggol_needs where NeedID = dv.NeedID) as Title from donationsview as dv
			// $this->template->set('donations',$this->needs->getAllDonations());
			$this->template->current_view = ('template/admin/donations');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	function dashboardStories(){
		if ($this->session->userdata('logged_in_admin')){	
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			
			$result = $this->admin->getPendingStories();
			$this->template->set('pending',$result);
			$this->template->current_view = ('template/admin/stories');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function dashboardApproveStories(){
		if ($this->session->userdata('logged_in_admin')){	
		
			$sid = $this->input->get('sid');
			$result = $this->admin->approveStories($sid);
			if($result['status'] == "success") {
				/* Email here */
			}
			
			redirect(base_url().'admin/stories');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function downloadNeeds(){
		$this->acn->downloadAllNeeds();
		
		// $data = array(
					// array("Need ID","Beneficiary","Title","Description","Required Funds","Date Created","Date Needed","Status"),
					// foreach($needs as $n){
						// array( $n['NeedID'],$n['Beneficiary'],$n['Title'],$n['Description'],$n['RequiredFunds'],date('m/d/y', strtotime($n['DateCreated'])),date('m/d/y', strtotime($n['DateNeeded'])))
					// }
				// );
		// $this->load->helper('csv');
		// array_to_csv($data,'needs.csv');
	}

	function downloadDonation(){
		print_r($this->acn->downloadDonations());

	}
	function dashboardMedia() {
		if ($this->session->userdata('logged_in_admin')){	
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			
			$media = $this->needs->getMedia('receipt pdf');
			$this->template->set('media', $media);
			
			$this->template->current_view = ('template/admin/media');
			$this->template->render('adminpanel'); 
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
}