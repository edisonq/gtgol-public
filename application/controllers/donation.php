<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donation extends CI_Controller{
	function __construct(){
    	parent::__construct();
    }
	function DonationPage() {
		$styles = Array("donate", "how-it-works");
		$this->template->set('styles', $styles);
		$this->template->current_view = ('template/donate_view');
		$this->template->render();
	}
}