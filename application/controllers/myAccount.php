<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class myAccount extends CI_Controller {
	
	function __construct() {  
		parent::__construct();
		$this->load->model('myAccount_model','my_account');
		$this->load->model('data_sanitizer_model','xss_clean');
		
		if (!$this->session->userdata('logged_in')){
			 redirect(base_url().'login', 'refresh');
		}
		$this->load->model('myNeeds_model','my_needs');
	}

	function createNeed() {
		$categories = $this -> my_account -> getCategories();
		$styles = array("completed_needs", "additional-main", "create_a_need", "jquery-ui","pop-modal");
		$scripts = array("jquery.form", "need", "jquery-ui","pop-up-modal");
		$this -> template -> set('styles', $styles);
		$this -> template -> set('scripts', $scripts);
		$this -> template -> set('categories', $categories);
		$this -> template -> current_view = ('template/createNeed_view');
		$this -> template -> render();
	}
	 
	function completedNeed(){
		$styles = array("completed_needs","additional-main");
		$result = $this->my_account->getCompletedNeeds();
		$count = $this->my_account->countCompletedNeeds();
		$this->template->set('styles', $styles);
		$this->template->set('complete',$result);
		$this->template->set('count',$count);
		$this->template->current_view = ('template/completedNeeds_view');
		$this->template->render();
	}
	
	function saveNeed(){
		$data = $this->security->xss_clean($this->input->post());
		$title = $data['Title'];
		$beneficiary = $data['Beneficiary'];
		$description = $data['Description'];
		$requiredfunds = $data['RequiredFunds'];
		$descriptionfunds = $data['DescriptionFunds'];
		$dateneeded = $data['DateNeeded'];
		$categoryid = $data['CategoryID'];
		$ggol = $data['ggol_tagcol'];
		
		if(empty($title) and empty($beneficiary) and empty($description) and empty($requiredfunds) and empty($descriptionfunds) and empty($dateneeded) and empty($categoryid) and empty($ggol)
			or empty($title) or empty($beneficiary) or empty($description) or empty($requiredfunds) or empty($descriptionfunds) or empty($dateneeded) or empty($categoryid) or empty($ggol)){
			$status = 'error';
			$message = 'Please fill up the missing fields';
			$mid = 0;
		}else {
			if((int)strtotime((string)$dateneeded) >= (int)time()){
				if (!empty($_FILES['userfile']['name'])) {
					$need = $this -> my_account -> saveNeed($data);
					$serial = $this->my_account->generateSerialNumber();
					if ($need) {
						$tag = $this -> my_account -> saveTag($ggol, $need);
						$saveSerial = $this->my_account->saveSerialNumber($serial,$need);
						$email = $this->my_account->notifyEmail();
						$media_need = $this -> my_account -> uploadPhoto($need);

						$status = 'success';
						$message = 'Successfully created! Please wait...';
						$mid = $media_need;		
	
					}
				} else {
					$status = 'error';
					$message = 'Please select a cover photo for your project.';
					$mid = 0;
				}
			}else{
				$status = "error";
				$message = "You can't create deadline when date is already past.";
				$mid = 0;
			}		
		}

		$json = array('status' => $status, 'message' => $message, 'id' => $mid);
		echo json_encode($json);
		exit;
	}
	
	function myNeeds(){
		$myNeeds = $this->my_needs->getMyNeeds();
		$styles = Array("my-needs");
		$this->template->set('styles', $styles);
		$this->template->set('myNeeds', $myNeeds);
		$this->template->set('message', 'content area goes here');
		$this->template->current_view = ('template/myNeeds_view');
		$this->template->render();
	}
	
	function mostViewNeeds(){
		$myNeeds = $this->my_needs->getMostViewNeeds();
		$notViewed = $this->my_needs->getMyNeeds();
		$styles = Array("my-needs");
		$this->template->set('styles', $styles);
		$this->template->set('myNeeds', $myNeeds);
		$this->template->set('myNeeds2',$notViewed);
		$this->template->set('message', 'content area goes here');
		$this->template->current_view = ('template/myNeedsMostView_view');
		$this->template->render();
	}
	
	function cropPhotos(){
		$mid = $this->xss_clean->sanitize($this->input->get('mid'));
		$result = $this->my_account->getOriginalPhoto($mid);
		$styles = array("completed_needs","additional-main","create_a_need","jquery-ui");
		$scripts = array("jquery.form","need","jquery-ui","jquery-pack","jquery.imgareaselect.min");
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('row',$result);
		$this->template->current_view = ('template/cropPhotos_view');
		$this->template->render();
	}
	
	function resizeThumbnailImage(){
		$data = $this->input->post();
		$width = $this->input->post('w');
		$height = $this->input->post('h');
		
		if(empty($width) and empty($height) or empty($width) or empty($height)){
			echo 'error';
		}else{
			$result = $this->my_account->resizeThumbnailImage($data);	
			echo 'success';
		}
		
	}

	function generateSerialNumber(){
		echo $this->my_account->generateSerialNumber();
	}

}