<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class successStories extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('successstories_model','story');	
		$this->load->model('data_sanitizer_model','xss_clean');
		$this->load->model('myNeeds_model','myneeds');
	}
	
	public function index(){
		$pagenum = 1;
		$styles = Array("student-needs");
		$scripts = Array("success-stories");
		$result = $this->story->getSuccessStories();
		$cntsstories = $this->story->cntSuccessStories();
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('stories',$result);
		$this->template->set('pagenum', $pagenum);
		$this->template->set('cntsstories',$cntsstories);
		$this->template->current_view = ('template/successStories_view');
		$this->template->render();
	}
	
	public function viewPage(){
		$pagenum = $this->input->get('page');
		$offset= $this->input->get('offset');
		$styles = Array("student-needs");
		$result = $this->story->page($offset);
		$scripts = Array("success-stories");
		$cntsstories = $this->story->cntSuccessStories();
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('stories', $result);
		$this->template->set('pagenum', $pagenum);
		$this->template->set('cntsstories',$cntsstories);
		$this->template->current_view = ('template/successStories_view');
		$this->template->render();
	}
	
	public function completeSuccessStory($nid){
		if (!$this->session->userdata('logged_in')){
			 redirect(base_url().'login', 'refresh');
		}	
		else
		{
			// error_reporting(0);
			// print_r($this->session->userdata('ID'));

			# check here if the user owns the need.
			/*if (!($this->myneeds->checkUserOwnsNeed($this->session->userdata('ID')))) {
				# redirect to my-account/my-needs
				redirect(base_url().'my-account/my-needs', 'refresh');	
			}*/
			# end of checking if the user owns the need
		}
		$styles = array("completed_needs", "additional-main", "complete_a_success");
		$scripts = array("jquery.form", "success-stories","jquery-ui");
		$this->template->set('styles', $styles);
		$this->template->set('scripts',$scripts);
		$this->template->current_view = ('template/completeSuccessStory_view');
		$this->template->render();
	}
	
	public function _generate_image_name(){
    	return rand().'_'.rand(1,1000000).rand(1111111,9999999).'_'.time().'_n';            
    }  

	public function submitCompleteStory(){
		$data = $this->security->xss_clean($this->input->post());
		//$data = $this->input->post();
		$NeedID = $data['NeedID'];
		$letter = $data['thankYouLetter'];
		$MediaIDs = array();
		$status = "error";
		$message = "Fill up the required fields or upload the Required Files";
		
			//eprint_r($_FILES); die();
		$file1ext = pathinfo($_FILES['userfile1']['name']);
		$file2ext = pathinfo($_FILES['userfile2']['name']);
		
		//echo $file1ext['extension'];
		if(empty($letter) or empty($NeedID)){
			$status = "error";
			$message = "Fill up the Thank You Letter first";
		} else if(empty($_FILES['userfile1']['name']) or empty($_FILES['userfile2']['name'])){
			$status = "error";
			$message = "Please attach Receipts and Expense Form!";
		//} else if($_FILES['userfile1']['type'] != 'application/pdf' and $_FILES['userfile2']['type'] != 'application/pdf'){
		} else if($file1ext['extension'] != 'pdf' and $file2ext['extension'] != 'pdf'){
			$status = "error";
			$message = "Please attach PDF Files for Receipts and Expense Form!";
		} else {
			$this->load->library('upload');		
			$Youtube = $this->story->saveYoutubeLink($data); 
			
			if(!empty($_FILES['userfiles']['name'][0]) && count($_FILES['userfiles']['name']) >= 1){
				$i = 0;
				foreach($_FILES['userfiles']['name'] as $file):
				$_FILES['userPhotos'][$i]['name'] = $_FILES['userfiles']['name'][$i];
				$_FILES['userPhotos'][$i]['type'] = $_FILES['userfiles']['type'][$i];
				$_FILES['userPhotos'][$i]['tmp_name'] = $_FILES['userfiles']['tmp_name'][$i];
				$_FILES['userPhotos'][$i]['error'] = $_FILES['userfiles']['error'][$i];
				$_FILES['userPhotos'][$i]['size'] = $_FILES['userfiles']['size'][$i];
				$i++;
				endforeach;
				
				unset($_FILES['userfiles']);
				//die(print_r($_FILES['userPhotos']));
				
				foreach($_FILES['userPhotos'] as $_FILES['userfile']):
					$MediaIDs[] = $this->story->uploadPhoto();
				endforeach;
				
			}			
			
			if (!empty($_FILES['userfile1']['name']) and !empty($_FILES['userfile2']['name'])){
	            $receipt = $this->story->uploadReceipt();
	        	$expense = $this->story->uploadExpense();				
	       	}
					
			if(count($MediaIDs) >= 1){
				$stories = $this->story->saveStories($data,$MediaIDs[0]);
				if($stories){					
					foreach($MediaIDs as $MediaID):
						$media_stories = $this->story->saveHasMediaStories($NeedID,$MediaID,$stories);
					endforeach;
					
					$Embed = $this->story->saveHasMediaStories($NeedID,$Youtube,$stories);
					if (!empty($_FILES['userfile1']['name'])){
						$saveReceipt = $this->story->saveHasMediaStories($NeedID,$receipt['id'],$stories);
					}
					if(!empty($_FILES['userfile2']['name'])){
						$saveExpense = $this->story->saveHasMediaStories($NeedID,$expense['id'],$stories);
					}
					//redirect(base_url().'success-stories');
					$status = "success";
					$message = "Successfully done! Please wait... ";
					
					$this->load->library('email');
					$this->email->from('system@givethegiftofliberty.org', 'GGOL System');
					$this->email->to('amccobin@studentsforliberty.org');
					$this->email->subject('Receipts and Reimbursement Forms');
					$this->email->message('This is an automatic message from GGOL System, you can find attached the Receipts and Reimursement Forms for Story with ID:' . $stories . ' which is now waiting for your approval!');
					$this->email->attach(BASEPATH .'../files/needs/receipts/' . $receipt['filename']);
					$this->email->attach(BASEPATH .'../files/needs/expenses/' . $expense['filename']);
					$this->email->send();
				}
			}
		}
	
		$json = array('status' => $status, 'message' => $message);
		echo json_encode($json);
		exit;
		
    }

}
