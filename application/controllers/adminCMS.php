<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminCMS extends CI_Controller{
	
	function __construct(){
    	parent::__construct();
        $this->load->model('adminCMS_model','cms');
        $this->load->model('data_sanitizer_model','xss_clean');
    }
	
	function index(){
		
	}
	
	function dashboard(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$this->template->current_view = ('template/dashboard_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function pages(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$pages= $this->cms->showAllPages();
			$this->template->set('pages', $pages);
			$this->template->current_view = ('template/pages_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function addPage(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$this->template->current_view = ('template/addPage_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function savePage(){
		//$data = $this->xss_clean->sanitize_script_tag($this->input->post());
		$data = $this->input->post();
		$result = $this->cms->addPage($data);
		if($result) redirect(base_url().'admin/pages');
	}
	
	function showSinglePage(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$pid = $this->input->get('pid');
			$page = $this->cms->showSinglePage($pid);
			if($page){
				$this->template->set('page', $page);
				$this->template->current_view = ('template/pageSingle_view');
				$this->template->render('adminpanel');
			}
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function deletePage(){
		$pid = $this->input->get('pid');
		$result = $this->cms->deletePage($pid);
		if($result){ redirect(base_url().'admin/pages'); }
	}
	
	function editPage(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$pid = $this->input->get('pid');
			$page = $this->cms->showSinglePage($pid);
			$this->template->set('page', $page);
			$this->template->current_view = ('template/editPage_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function updatePage(){
		//$data = $this->xss_clean->sanitize_script_tag($this->input->post());
		$data = $this->input->post();
		$result = $this->cms->updatePage($data);
		if($result) redirect(base_url().'admin/pages/show?pid='.$data['PageID']);
	}
	
	function menu(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$menu = $this->cms->getAllMenu();
			$this->template->set('menu',$menu);
			$this->template->current_view = ('template/menu_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function addMenu(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$this->template->current_view = ('template/addMenu_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function saveMenu(){
		//$data = $this->xss_clean->sanitize_script_tag($this->input->post());
		$data = $this->input->post();
		$result = $this->cms->addMenu($data);
		if($result){
			redirect(base_url().'admin/menu');
		}
	}
	
	function deleteMenu(){
		$mid = $this->input->get('mid');
		$deleteMenuPages = $this->cms->deleteMenuPages($mid);
		if($deleteMenuPages){
			$deleteMenu = $this->cms->deleteMenu($mid);
			if($deleteMenu){
				redirect(base_url().'admin/menu');
			}
		}
	}
	
	function addMenuPages(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$pages = $this->cms->showAllPages();
			$singleMenu = $this->cms->getSingleMenu($this->input->get('mid'));
			$this->template->set('pages',$pages);
			$this->template->set('smenu',$singleMenu);
			$this->template->current_view = ('template/addMenuPages_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function saveMenuPages(){
		$data = $this->xss_clean->sanitize_script_tag($this->input->post());
		$check = $this->cms->checkMenuPages($data);
		if($check == false){
			$result = $this->cms->addMenuPages($data);
			if($result){
				redirect(base_url().'admin/menu');
			}
		}else{
			redirect(base_url().'admin/menu');
		}
	}
	
	function removeMenuPage(){
		$id = $this->input->get('id');
		$result = $this->cms->removeMenuPage($id);
		if($result){
			redirect(base_url().'admin/menu');
		}
	}
	
	function group(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$menuHeader = $this->cms->getMenuGroup('header');
			$menuSide = $this->cms->getMenuGroup('sidebar');
			$menuFooter = $this->cms->getMenuGroup('footer');
			$this->template->set('menuHeader', $menuHeader);
			$this->template->set('menuSide', $menuSide);
			$this->template->set('menuFooter', $menuFooter);
			$this->template->current_view = ('template/group_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}
	
	function addGroupMenu(){
		if ($this->session->userdata('logged_in_admin')){
			$this->template->set('username',$this->session->userdata('AdminUN'));
			$this->template->set('userid',$this->session->userdata('AdminID'));
			$menuHeader = $this->cms->getMenuGroup('header');
			$menu = $this->cms->getDefaultMenu();
			$this->template->set('menu',$menu);
			$this->template->current_view = ('template/addGroupMenu_view');
			$this->template->render('adminpanel');
		}
		else {
			$this->template->current_view = ('template/admin/login');
			$this->template->render('adminlogin');
		}
	}

	function updateGroupMenu(){
		$data = $this->xss_clean->sanitize_script_tag($this->input->post());
		if($this->input->post('MenuType') != 'Default'){
			$checkMenuGroup = $this->cms->checkGroupMenu($data);
			if($checkMenuGroup != FALSE){
				$res = $this->cms->updateCheckGroupMenu($checkMenuGroup['MenuID']);		
			}
		}
		$result = $this->cms->updateGroupMenu($data);
		if($result){
			redirect(base_url().'admin/group');
		}
	}
	
	function removeGroupMenu(){
		$mid = $this->input->get('mid');
		$result = $this->cms->removeGroupMenu($mid);
		if($result){
			redirect(base_url().'admin/group');
		}
	}
}
