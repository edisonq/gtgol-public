<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usercalls extends CI_Controller {
	function __construct() {  
		parent::__construct();
		$this->load->model('Siteusers');
		//$this->output->enable_profiler(TRUE);
	}
	public function TesCheckUsername($username) {	
		echo $this->Siteusers->check_user($username, 'login');
	}
	public function CheckUsername() {
		$username = $_POST['username'];
		$result = $this->Siteusers->check_user($username, 'login');
		echo json_encode($result);
	}
	private function _PCheckUsername($username) {
		$result = $this->Siteusers->check_user($username, 'login');
		return $result;
	}
	public function CheckEmail() {
		$email = $_POST['email'];
		$result = $this->Siteusers->check_user($email, 'email');
		echo json_encode($result);
	}
	private function _PCheckEmail($email) {
		$result = $this->Siteusers->check_user($email, 'email');
		return $result;
	}
	public function VerifyExist() {
		$email = $_POST['email'];
		$username = $_POST['username'];
		$result = $this->Siteusers->verify_exist($email, $username);
		echo json_encode($result);
	}
	private function _RegisterUser($username, $email, $firstname, $lastname) {
		$this->load->library('email');
		$data['username'] 	= $username;
		$data['email'] 		= $email;
		$data['firstname'] 	= $firstname;
		$data['lastname'] 	= $lastname;
		$data['password'] 	= $this->Siteusers->generate_password(); /*Generated Password*/
		$data['hashedpw'] 	= $this->phpass->hash($data['password']); /*Encrypted Password*/
		
		$isusercreated = $this->Siteusers->create_user($data);
		if($isusercreated > 0) {
			/*Email functions here*/
			$this->email->from('no-reply@givethegiftofliberty.org', 'Give the Gift of Liberty');
			$this->email->to($email); 
			//$this->email->to('yordan@themarketaces.com');
			$this->email->subject('Give the Gift of Liberty Account information');
			$message  = "Dear $username,\nYour Login information:\r\n"; 
			$message  .= 'Username: '.$username."\r\n"; 
			$message .= 'Password: '.$data['password']."\r\n"; 
			$message .= 'Login URL: ' . base_url('login'); 
			$message .= "\r\n\r\n";
			$message .= "Sincerely & For Liberty,\r\n";
			$message .= "Alexander McCobin\r\n";
			$message .= "President, Students For Liberty\r\n";
			
			$this->email->message($message);	
			
			$this->email->send();

		}
		return $isusercreated;
	}
	public function Register() {
		$data['username'] 	= $_POST['username'];
		$data['email'] 		= $_POST['email'];
		$data['firstname'] 	= $_POST['firstname'];
		$data['lastname'] 	= $_POST['lastname'];
		$ret = 1;
		if($data['username'] == "") {
			$ret = 'username is required';
		} 
		else if($data['email'] == "") {
			$ret = 'email is required';
		}
		else if($data['firstname'] == "") {
			$ret = 'firstname is required';
		} 
		else if($data['lastname'] == "") {
			$ret = 'lastname is required';
		}
		
		
		if($ret == 1) {
			if($this->_PCheckEmail($data['email']) != 0) {
				$ret = 'Email Already Exist';
			}
			if($this->_PCheckUsername($data['username']) != 0) {
				$ret = 'Username Already Exist';
			}
			if($ret == 1) {
				$this->_RegisterUser($data['username'], $data['email'], $data['firstname'], $data['lastname']);
			}
		}
		
		echo json_encode($ret);
	}
	public function LoginUser() {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$results = $this->Siteusers->check_login($username, $password);
		
		if($results['status'] == 1) {
			$userdata = array(
                'username'  => $username,
                'ID'  => $results['ID'],
				'SID'  => $results['SID'],
                'logged_in' => 1
            );

			$this->session->set_userdata($userdata);
		}
		echo json_encode($results['status']);
	}
	public function LoginAdmin() {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$results = $this->Siteusers->check_login_admin($username, $password);
		
		if($results['status'] == 1) {
			$userdata = array(
                'AdminUN'  => $username,
                'AdminID'  => $results['ID'],
                'logged_in_admin' => 1
            );

			$this->session->set_userdata($userdata);
		}
		echo json_encode($results['status']);
		
	}
	public function AdminLogout() {
		$userdata = array(
            'AdminUN'  => '',
            'AdminID'  => '',
            'logged_in_admin' => ''
        );

		$this->session->unset_userdata($userdata);
		header('Location:'.base_url().'admin');
	}
	public function IsLogin() {
		echo json_encode($this->session->userdata('logged_in'));
	}
	public function SendSampleMail($email) {
		$this->load->library('email');
		$this->email->from('alvin@themarketaces.com', 'Give the Gift of Liberty');
		$this->email->to($email.'@gmail.com'); 
		$this->email->subject('Give the Gift of Liberty Account information');
		$this->email->message('Hello');	
		
		$this->email->send();

		echo $this->email->print_debugger();
	}
	public function ServerInfo() {
		phpinfo();
	}
}
