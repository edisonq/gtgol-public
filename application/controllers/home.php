<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('studentneedviews');
		$this->load->model('successstories_model','story');
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        $styles = Array("homepage");
		$featured = $this->studentneedviews->getFeatured();
        $almostdue = $this->studentneedviews->getAlmostDueNeed();
        $latest = $this->studentneedviews->getLatestNeed();
        $success = $this->story->getSuccessStories();
        
        $this->template->set('success', $success);
        $this->template->set('featured', $featured);
		$this->template->set('styles', $styles);
        $this->template->set('latest', $latest);
        $this->template->set('almostdue', $almostdue);
        $this->template->current_view = ('template/home_view');
        $this->template->render();
    }

}