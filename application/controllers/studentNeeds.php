<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class studentNeeds extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('studentNeeds_model','needs');
		$this->load->model('Siteusers');
		$this->load->model('studentneedviews');
		$this->load->library('curl');
	} //for svn lang
	
	public function index(){
		$pagenum = 1;
		$styles = Array("student-needs");
		$scripts = array("need");
		$result = $this->needs->getNeeds();
		$cntneeds = $this->needs->cntNeeds();
	//	$links = $this->pagination->create_links();
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('needs', $result);
		$this->template->set('pagenum', $pagenum);
		$this->template->set('cntneeds',$cntneeds);
	//	$this->template->set('links',$links);
		$this->template->current_view = ('template/studentNeeds_view');
		$this->template->render();
	}
	
	public function viewPage(){
		$pagenum = $this->input->get('page');
		$offset= $this->input->get('offset');
		$styles = Array("student-needs");
		$scripts = array("need");
		$result = $this->needs->page($offset);
		$cntneeds = $this->needs->cntNeeds();
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('needs', $result);
		$this->template->set('pagenum', $pagenum);
		$this->template->set('cntneeds',$cntneeds);
		$this->template->current_view = ('template/studentNeeds_view');
		$this->template->render();
	}
	
	public function viewSingleNeeds($nid){
		if(isset($this->session->userdata['ID'])){
			$record = $this->needs->recordView($nid);
		}		
		$styles = Array("student-singleNeeds");
		$scripts = Array("need");
		$pasts = $this->needs->getPasts(4); //the parameter is for limit
		$result = $this->needs->getSingleNeed($nid);
		$status = $this->needs->getNeedStatus($nid);
		$story = $this->needs->getStory($this->uri->segment('3'));
		$photos = $this->needs->getStoryPhotos($this->uri->segment('3'));
		
		$this->template->set('photos',$photos);		
		$this->template->set('pasts', $pasts);
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('row', $result);
		$this->template->set('statuses',$status);
		$this->template->set('story',$story);
		$this->template->set('message', 'content area goes here');
		$this->template->current_view = ('template/studentSingleNeeds_view');
		$this->template->render();
	}
	public function donation($id) {
		if(!$this->session->userdata('ID')){ 

			$userdata = array(
				'username'  => 'Anonymous',
				'ID'  => '0',
				'SID'  => '',
				'logged_in' => 2
			);

			$this->session->set_userdata($userdata);
			
		}
		$isStory = $this->studentneedviews->checkStory($id);
		$styles = array("completed_needs","additional-main","donation","create_a_need");
		$scripts = Array("donate","jquery-ui","jquery.valid", "jquery.form");
		$title = $this->studentneedviews->getData($id, 'Title');
		$need = $this->studentneedviews->getNeed($id);
		
		$this->template->set('styles', $styles);
		$this->template->set('need', $need);
		$this->template->set('title', $title);
		$this->template->set('scripts', $scripts);
		$this->template->set('amount', $this->input->post('donateValue'));
		
		//echo empty($need)? 'Empty': 'Not Empty';
		if(empty($need)) {
			$this->template->current_view = ('template/cantdonate_view');
		}
		else {
			
		
			$daysleft =  $need->DaysLeft;
			$t = isset($daysleft)? 1: 0;
		
			if( ($isStory > 0 ) || ($t == 0) || ($daysleft < 0)) {
				$this->template->current_view = ('template/cantdonate_view');
			}
			else {
				$this->template->current_view = ('template/donation_view');
			}
		}
		$this->template->render();
	}
	public function testData() {
		$userdata = $this->studentneedviews->getProfileByEmail('alvinucsk@gmail.com');
		echo '<pre>';var_dump($userdata->firstname[0]);echo '</pre>';
	}
	public function addDonations($ispublish) {

		//error_reporting(E_ALL); ini_set("display_errors", 1);

		//if($this->session->userdata('ID')) {
			$this->load->library('email');
			
			$data = $this->input->post();
			$email = $_POST['EmailAddress'];
			
			if($this->session->userdata('ID') == 0) { 
				/* 
					Check if email is an existing user
				*/
				$email = $_POST['EmailAddress'];
				$result1 = $this->Siteusers->check_user($email, 'email');
				if($result1 > 0) {
					/* 
						User exist
						# Get User Credits
					*/
					$userdata = $this->studentneedviews->getProfileByEmail($email);
					$UserID = $userdata->UserID;
				} else {
					
					$UserID = $this->session->userdata('ID'); //Temporary value, dapat session id ni sa user
				}
			} else {
				$UserID = $this->session->userdata('ID'); //Temporary value, dapat session id ni sa user
			}

			$need = $this->needs->getSingleNeed($data['NeedID']);
			
			$info['cardtype'] = $_POST['CreditCardInfo'];
			$info['actnum'] = str_replace(' ', '', $_POST['CardNumber']);
			$info['expdate'] = $_POST['Month'].$_POST['Year'];
			$info['ccv2'] = $_POST['Code'];
			$info['fname'] = $_POST['FirstName'];
			$info['lname'] = $_POST['LastName'];
			$info['email'] = $_POST['EmailAddress'];
			$info['strt'] = $_POST['BillingAddress1'].$_POST['BillingAddress2'];
			$info['city'] = $_POST['City'];
			$info['state'] = $_POST['State'];
			$info['ccode'] = 'US';
			$info['zip'] = $_POST['Zip'];
			$info['amt'] = $_POST['DonatedAmount'];
			$info['PhoneNumber'] = $_POST['PhoneNumber'];
			$info['currencycode'] = 'USD';
			$info['desc'] = 'GGOL Donation';
			
			$api_version = $this->config->item('paypal_api_version');
			$api_username = $this->config->item('paypal_api_username');
			$api_password = $this->config->item('paypal_api_password');
			$api_signature = $this->config->item('paypal_api_signature');
			$api_endpoint = $this->config->item('paypal_api_endpoint');
			$api_method = "DoDirectPayment";
			
			$request_params = array (
				'METHOD' => $api_method,
				'USER' => $api_username,
				'PWD' => $api_password,
				'SIGNATURE' => $api_signature,
				'VERSION' => $api_version,
				'PAYMENTACTION' => 'Sale',
				'IPADDRESS' => '127.0.0.1',
				'RETURNFMFDETAILS' => 1,
				
				'CREDITCARDTYPE' => $info['cardtype'],
				'ACCT' => $info['actnum'],
				'EXPDATE' => $info['expdate'],
				'CVV2' => $info['ccv2'],
				'FIRSTNAME' => $info['fname'],
				'LASTNAME' =>  $info['lname'],
				'EMAIL' =>  $info['email'],
				'STREET' =>  $info['strt'],
				'CITY' =>  $info['city'],
				'STATE' =>  $info['state'],
				'COUNTRYCODE' =>  $info['ccode'],
				'ZIP' =>  $info['zip'],
				'AMT' =>  $info['amt'],
				'CURRENCYCODE' =>  $info['currencycode'],
				'DESC' =>  $info['desc'],
				'SHIPTOPHONENUM' =>  $info['PhoneNumber']
			);
			$nvp_string = '';
			foreach ($request_params as $var => $val) {
			   $nvp_string .= '&' . $var . '=' . urlencode($val);
			}
			$result = $this->curl->setUrl($api_endpoint)->post($nvp_string);
			$arr = explode('&', $result);
			$stat = explode('=', $arr[2]);
		
			if($stat[1] == "Success" || $stat[1] == "SuccessWithWarning") {
				if(@$result1 == 0) {
					/* Create special username */
					$susername = strtolower($info['fname'].$info['lname']).rand();
					
					for($icnt = 0;($this->_PCheckUsername($susername) == 1);$icnt) {
						$susername = strtolower($info['fname'].$info['lname']).rand();
					}
						
					/*Send user credits*/
					$this->_RegisterUser($susername, $email, $info['fname'], $info['lname']);
					$userdata2 = $this->studentneedviews->getProfileByEmail($email);
					$UserID = $userdata2->UserID;
				}
				$trans = explode('=',$arr[9]);
				$donor = $this->needs->addDonor($UserID);
				if($donor){
					
					$donation = $this->needs->addDonations($data,$donor,$ispublish,$trans[1]);
					$needurl = base_url()."student-needs/view/".$data['NeedID'];
					//redirect(base_url().'student-needs/thanks_donation_view');
					$this->email->from('no-reply@givethegiftofliberty.com', 'Give the Gift of Liberty');
					$this->email->to($email); 
					$this->email->subject('Thank you for your Donation');					
					$email_message  = $info['fname']." ".$info['lname'].", \r\n\r\n";
					$email_message .= "Thank you so much for donating to Students For Liberty through Give the Gift of Liberty! ";
					$email_message .= "Your contribution of $".$info['amt']." to support ".$need['Title']."($needurl) will greatly support education in the ";
					$email_message .= "ideas of liberty and provide the resources for students to become effective leaders of liberty. ";
					$email_message .= "Students For Liberty, is a 501(c)(3) nonprofit organization registered in the Commonwealth of ";
					$email_message .= "Virginia (EIN 94-3435899). Contributions to Students For Liberty are deductible under section 170 ";
					$email_message .= "of the Internal Revenue Code. Please consult your tax advisor for clarification and guidance on ";
					$email_message .= "the specifics of the deduction.\r\n\r\n";
					$email_message .= "Thank you again for your generous support and dedication to liberty! \r\n";
					$email_message .= "Please do not hesitate to reach out to us with any questions or inquiries you may have.\r\n";
					$email_message .= "\r\n\r\n";
					$email_message .= "Sincerely & For Liberty,\r\n";
					$email_message .= "Alexander McCobin\r\n";
					$email_message .= "President, Students For Liberty\r\n";
					$email_message .= "P.S.\r\n";
					$email_message .= "Learn more about Students For.\r\n";
					$email_message .= "Liberty and get involved at\r\n";
					$email_message .= "www.studentsforliberty.org\r\n";
					$this->email->message($email_message);
					
					$this->email->send();
					
					$this->sendEmailToOwner('amccobin@studentsforliberty.org',$info['fname']." ".$info['lname'], $need['NeedID'], $need['Title'], $info['email'], $info['strt'].' '.$info['city'].' '.$info['state'].' '.$info['ccode'].' '.$info['zip'], $info['amt'],$info['PhoneNumber']);
					echo "true"; //json_encode($stat[1]);
				}
			}
			else {
				echo "false"; //json_encode($stat[1]);
			}
		/*}
		else {
			$this->template->current_view = ('template/notloggedin_view.php');
			$this->template->render();
		}*/		
	}
	private function _RegisterUser($username, $email, $firstname, $lastname) {
		$this->load->library('email');
		$data['username'] 	= $username;
		$data['email'] 		= $email;
		$data['firstname'] 	= $firstname;
		$data['lastname'] 	= $lastname;
		$data['password'] 	= $this->Siteusers->generate_password(); /*Generated Password*/
		$data['hashedpw'] 	= $this->phpass->hash($data['password']); /*Encrypted Password*/
		
		$isusercreated = $this->Siteusers->create_user($data);
		if($isusercreated > 0) {
			/*Email functions here*/
			$this->email->from('no-reply@givethegiftofliberty.org', 'Give the Gift of Liberty');
			$this->email->to($email); 
			//$this->email->to('yordan@themarketaces.com');
			$this->email->subject('Give the Gift of Liberty Account information');
			$message  = 'Username: '.$username."\r\n"; 
			$message .= 'Password: '.$data['password']."\r\n"; 
			$message .= 'Login URL: ' . base_url('login'); 
			
			$this->email->message($message);	
			
			$this->email->send();

		}
		return $isusercreated;
	}
	private function _PCheckUsername($username) {
		$result = $this->Siteusers->check_user($username, 'login');
		return $result;
	}
	public function sendEmailToOwner($email, $donorname, $needid, $needname, $donoremail, $donoraddress, $amtdonated,$phone) {
		$this->email->from('no-reply@givethegiftofliberty.com', 'Give the Gift of Liberty');
		$this->email->to($email); 
		$this->email->subject('GTGOL - Someone has donated!');
		
		$needurl = base_url()."student-needs/view/".$needid;
		$email_message = "$donorname($donoremail) has donated with the amount of $amtdonated\n";
		$email_message .= "under the need of $needname($needid).\n";
		$email_message .= "URL : $needurl\n\r";
		$email_message .= "Contact Information:\n $donoraddress\n\r";
		$email_message .= "Phone number:\n $phone\n\r";
		
		$email_message .= "\r\n\r\n";
		$email_message .= "Sincerely & For Liberty,\r\n";
		$email_message .= "Alexander McCobin\r\n";
		$email_message .= "President, Students For Liberty\r\n";
		$this->email->message($email_message);
		
		$this->email->send();
	}
	public function searchStudentNeeds(){
		$pagenum = 1;
		$data = $this->input->get();
		$styles = Array("student-needs");
		$scripts = array("need");
		$sort = $data['sort'] = !empty($data['sort']) ? $data['sort'] : 'created';
		$schoolName = $data['schoolName'];
		
		if(isset($data['needType'])){
			$needType = $data['needType'];
			$this->template->set('needType',$needType);
		}else{
			$needType = '';
		}
		
		//echo '<pre>';
		//print_r($data);die();
		
		$result = $this->needs->searchStudentNeeds($data);
		$cntSSNeeds = $this->needs->cntSearchStudentNeeds($data);
		$this->template->set('sort',$sort);
		$this->template->set('schoolName',$schoolName);
		$this->template->set('pagenum',$pagenum);
		$this->template->set('cntSSNeeds',$cntSSNeeds);
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('needs', $result);
		$this->template->current_view = ('template/searchNeedResults_view');
		$this->template->render();
	}
	
	public function viewSSNPage(){
		$data = $this->input->get();
		$pagenum = $this->input->get('page');
		$offset= $this->input->get('offset');
		$styles = Array("student-needs");
		$scripts = array("need");
		$sort = $data['sort'];
		$schoolName = $data['schoolName'];
		if(isset($data['needType'])){
			$needType = $data['needType'];
			$this->template->set('needType',$needType);
		}else{
			$needType = '';
		}
		$result = $this->needs->pageSSN($offset,$data);
		$cntSSNeeds = $this->needs->cntSearchStudentNeeds($data);
		$this->template->set('sort',$sort);
		$this->template->set('schoolName',$schoolName);
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->set('needs', $result);
		$this->template->set('pagenum', $pagenum);
		$this->template->set('cntSSNeeds',$cntSSNeeds);
		$this->template->current_view = ('template/searchNeedResults_view');
		$this->template->render();
	}
	
	public function customSearch(){
		$data = $this->input->get();
		$ser = $this->needs->customSearchSerial($data);

		if($ser != false){
			redirect(base_url().'student-needs/view/'.$ser['NeedID']);		
		}else{
			$tag = $this->needs->customSearchTags($data);
			$projectName = $this->needs->customSearchProjects($data['keyword']);
			
			if($tag != false){
				redirect(base_url().'student-needs/search?sort=created&schoolName=&searchTag='.$data['keyword']);	 
			}else if($projectName){
				redirect(base_url().'student-needs/custom-search?keyword='.$data['keyword']);
			}else{
				redirect(base_url().'student-needs/search?sort=0&schoolName=0&searchTag=0');			
			}
		}

	}
	
	public function projectNameResults(){
		$k = $this->input->get('keyword');
		$res = $this->needs->customSearchProjects($k);	
		$styles = Array("student-needs");
		$scripts = array("need");
		$this->template->set('needs',$res);
		$this->template->set('styles', $styles);
		$this->template->set('scripts', $scripts);
		$this->template->current_view = ('template/projectNameResult');
		$this->template->render();
	}

}
