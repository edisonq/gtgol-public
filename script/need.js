$(function(){
	
	$(".sort").change(function(){
		var val = $(this).val();
		if(val == 'recent'){
			window.location = js_baseurl+"my-account/my-needs";
		}else{			
			window.location = js_baseurl+"my-account/my-needs/most-view";
		}
	});
	
	$('.submitNeedBtn').click(function(){
		$('.submit').click();
	});
	
	var options = {
		cache:false,
		url: js_baseurl+'my-account/save-need',
		beforeSubmit: function() {
			//$('#uploader').html('<img src="'+js_baseurl+'images/ajax-loader.gif" border="0"/> Loading..');
			$('#uploader').hide()
				.addClass('response-waiting')
				.text('Loading... Please wait')
				.fadeIn(200);
		},
		success:function(data) {
			var responseData = jQuery.parseJSON(data), property = 'color', attr = '';
			switch(responseData.status){
				case 'error':
					attr = '#900';	
				break;
				
				case 'success':
					attr = 'green';
					$("input[type=text],input[type=file],select,textarea").val("");
				//	$('#media-id').val(responseData.id);		
					$('#buttones').click();
					$("#anchor-crop").html("<a href='"+js_baseurl+"my-account/crop-photos?mid="+responseData.id+"' id='crop-id'>Crop your thumbnail</a>");
					$('body,html').animate({scrollTop:100 }, 800);
				break;
			}
					
			$('#uploader').fadeOut(200,function(){
            	$(this).removeClass('response-waiting')
            		.text(responseData.message)
            		.css(property,attr)
                	.fadeIn(200,function(){				  
	                    setTimeout(function(){
	                        $('#uploader').fadeOut(200,function(){		
	                            $('.submit').removeAttr('disabled');
	                            $(this).css('color','#222');
	                        });
						},2000)			  
                	});
            });
		}	
	};

	$('#create-needs').submit(function() {
		$('.submit').attr('disabled','disabled');
		$(this).ajaxSubmit(options);
		return false;
	});
	
	$("#coords").submit(function(){
		var form = $(this),
	        formData = form.serialize(),
			formUrl = form.attr('action');
		
		$("#save_thumb").attr('disabled','disabled');
		//$('#uploader').html('<img src="'+js_baseurl+'images/ajax-loader.gif" border="0"/> Loading..');
		$('#uploader').hide()
				.addClass('response-waiting')
				.text('Loading... Please wait')
				.fadeIn(200);
		
		$.ajax({
			url: formUrl,
			type: "POST",
			data: formData,
			success:
				function(data){
					var property = 'color', attr = '';
					switch(data){
						case 'error':
							attr = '#900';	
							message = "You must select and crop the image first";	
						break;
						
						case 'success':
							attr = 'green';
							message = "Successfully cropped! Please wait...";
							$("input[type=hidden]").val("");
							setTimeout(function(){
		                        window.location = js_baseurl+"my-account/my-needs";
							},2500)	
						break;
					}
		            
		            $('#uploader').fadeOut(200,function(){
		            	/*if (responseData.message != null){
		            		var displayMessage = responseData.message;
		            	} else {
		            		var displayMessage = '';
		            	}*/


		            	$(this).removeClass('response-waiting')
		            		.text(message)
		            		.css(property,attr)
		                	.fadeIn(200,function(){				  
			                    setTimeout(function(){
			                        $('#uploader').fadeOut(200,function(){		
			                            $('.submit').removeAttr('disabled');
			                            $(this).css('color','#222');
			                        });
								},2000)			  
		                	});
		            });
		         
				}
		});			
		
		return false;
	});
	
	var updateNeeds = {
		url: js_baseurl+'my-needs/update',
		beforeSubmit: function() {
			//$('#uploader').html('<img src="'+js_baseurl+'images/ajax-loader.gif" border="0"/> Loading..');
			$('#uploader').hide()
				.addClass('response-waiting')
				.text('Loading... Please wait')
				.fadeIn(200);
		},
		success:function(data) {
			var responseData = jQuery.parseJSON(data), property = 'color', attr = '';
			switch(responseData.status){
				case 'error':
					attr = '#900';				
				break;
				
				case 'success':
					attr = 'green';
					setTimeout(function(){
	                	window.location = js_baseurl+"my-account/my-needs";
					},2500)	
				break;
			}
					
				 $('#uploader').fadeOut(200,function(){
		            	$(this).removeClass('response-waiting')
		            		.text(responseData.message)
		            		.css(property,attr)
		                	.fadeIn(200,function(){				  
			                    setTimeout(function(){
			                        $('#uploader').fadeOut(200,function(){		
			                            $('.submit').removeAttr('disabled');
			                            $(this).css('color','#222');
			                        });
								},2000)			  
		                	});
		            });
		}	
	};
	
	$('#update-need').submit(function() {
		$('.submit').attr('disabled','disabled');
		$(this).ajaxSubmit(updateNeeds);
		return false;
	});
	
	$('.delete-tag').click(function(){
		var tid = $(this).attr('name');
		$('#uploader').html('<img src="'+js_baseurl+'images/ajax-loader.gif" border="0"/> Loading..');
		$.get(js_baseurl+"my-needs/delete-tag?tid="+tid, 
			function(){ 
				$('#uploader').html(""); 
				$("."+tid).remove();
		});
	});
	
	$('.add-more-tags').click(function(){
		$('#add-tag').show();
	});
	
	

});

function updateButton(id){
	var btn = $("#ub"+id);
	var posTop = btn.position().top;
	var popupStatus = $("#pus"+id);
	var display = popupStatus.css('display');
	
	if(display=="none"){
		popupStatus.css({position:'absolute',top:(posTop - 145),display:'inline', right:'10px'});
	}else{
		popupStatus.css('display','none');
	}
	

};

function updateStatus(nid){
		var form=$('#submitUpdateStatus'+nid),
			formData= form.serialize(),
			formUrl= form.attr('action');
		
		$.ajax({
			url: formUrl,
			type: "POST",
			data: formData,
			success:
				function(data){
					var responseData = jQuery.parseJSON(data);property = 'color',attr='';
					switch(responseData.status){
						case 'error':
							$('#txtareaUpdateStory'+nid).attr('disabled','disabled').css('background','white');
							attr = "#900";
							break;
						case 'success':
							attr = 'green';
							$('#txtareaUpdateStory'+nid).attr('disabled','disabled').css('background','white');
							break;
					}
					
					$('#updateStatusMessage'+nid).fadeOut(200,function(){
						$('#pus'+nid).css('margin-top','-20px');
						$('#updateStatusMessage'+nid).html(responseData.message)
							.css(property,attr)
							.fadeIn(200,function(){				  
									setTimeout(function(){
									$('#updateStatusMessage'+nid).fadeOut(200,function(){	
												$('#txtareaUpdateStory'+nid).val("").removeAttr('disabled');
												$('#pus'+nid).css('margin-top','8px');
												});
									},2000)			  
							});
					});
				}
		});
}

function readMore(nid){
	$("#needDescription"+nid).html($("#description"+nid).val().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g,'<br />'));
	$(".readMoreBottom").css('margin-left','140px');
};