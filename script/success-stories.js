$(function(){
	
	$('.submitStoryBtn').click(function(){
		$('.submit').click();
	});
	
	var options = {
		url: js_baseurl+'submit-complete-story',
		beforeSubmit: function() {
			//$('#uploader').html('<img src="'+js_baseurl+'images/ajax-loader.gif" border="0"/>Loading..');
			$('#uploader').hide()
				.addClass('response-waiting')
				.text('Loading... Please wait')
				.fadeIn(200);
		},
		success:function(data) {
			var responseData = jQuery.parseJSON(data), property = 'color', attr = '';
			switch(responseData.status){
				case 'error':
					attr = '#900';				
				break;
				
				case 'success':
					attr = 'green';
					$("input[type=text],input[type=file],select,textarea").val("");
					setTimeout(function(){
                        window.location = js_baseurl+"success-stories";
					},2500)	
				break;
			}
					
			$('#uploader').fadeOut(200,function(){
            	$(this).removeClass('response-waiting')
            		.text(responseData.message)
            		.css(property,attr)
                	.fadeIn(200,function(){				  
                    	setTimeout(function(){
                        	$('#uploader').fadeOut(200,function(){		
                            	$('.submit').removeAttr('disabled');	
                            	$(this).css('color','#222');	
                        	});
						},2000)			  
                	});
            });
		}	
	};

	$('#submit-success').submit(function() {
		$('.submit').attr('disabled','disabled');
		$(this).ajaxSubmit(options);
		return false;
	});
});

function readMore(sid){
	$("#ssDescription"+sid).html($("#description"+sid).val().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g,'<br />'));
	$(".readMoreBottom").css('margin-left','140px');
}