jQuery(document).ready(
	function() {
		jQuery("#uname").change(
			function() {
				var un = jQuery("#uname").val();
				if(un) {jQuery("#uname").css("border","1px solid #CCC");}
				else {
					jQuery("#errmessage").html("<small>Invalid Login</small>");
					jQuery("#errmessage").fadeIn("fast", function () {
						jQuery("#uname").css("border","1px solid red");					
					});
				}
			}
		);
		jQuery("#pwd").change(
			function() {
				var pw = jQuery("#pwd").val();
				if(pw) {jQuery("#pwd").css("border","1px solid #CCC");}
				else {
					jQuery("#errmessage").html("<small>Invalid Login</small>");
					jQuery("#errmessage").fadeIn("fast", function () {
						jQuery("#pwd").css("border","1px solid red");					
					});
				}
			}
		);
		jQuery("#btnlogin").click(
			function(){
				var un = jQuery("#uname").val();
				var pw = jQuery("#pwd").val();
				if(un) {
					jQuery("#errmessage").fadeOut("fast", function () {
						jQuery("#uname").css("border","1px solid #CCC");					
					});
				}
				else {
					jQuery("#errmessage").html("<small>Invalid Login</small>");
					jQuery("#errmessage").fadeIn("fast", function () {
						jQuery("#uname").css("border","1px solid red");					
					});
				}
				
				if(pw) {jQuery("#pwd").css("border","1px solid #CCC");}
				else {
					jQuery("#errmessage").html("<small>Invalid Login</small>");
					jQuery("#errmessage").fadeIn("fast", function () {
						jQuery("#pwd").css("border","1px solid red");					
					});
				}

				if(un && pw) {
					jQuery.post(js_base_url+"usercalls/LoginAdmin",{username: un, password: pw},function(e){
						if(e == 0) {
							jQuery("#errmessage").html("<small>Login Incorrect</small>");
							jQuery("#errmessage").fadeIn("fast", function () {});
						}else {
							jQuery("#errmessage").css("color", "green");
							jQuery("#errmessage").html("<small>Congratulations</small>");
							jQuery("#errmessage").fadeIn("slow", function () {
								window.location = js_base_url+"admin";
							});
						}
					});
				}
			}
		);
		
	}
);
