$(document).ready(function() {
	function checkEmail(MyEmail) {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(MyEmail)){
			return (true)
		}
		/*Invalid email*/
		return (false);
	}
	$("#signupPage #signupButton").click(
		function() 	{
			$.post(
				js_baseurl+'usercalls/Register',
				{
					username: $("#signupPage .username").val(),
					email: $("#signupPage .email").val(),
					firstname: $("#signupPage .firstname").val(),
					lastname: $("#signupPage .lastname").val()
				},
				function(e) {
					if(e != 1) {
						var err = e.substring(1);
						err = err.substring(0,err.length - 1);
						$("#errmessage").html(err);
						$("#errmessage").fadeIn("fast", function () {});
					}
					else {
						
						$("#errmessage").fadeOut("fast", function () {});
						window.location = js_baseurl+'welcome/?name='+$("#signupPage .username").val();
					}
				}
			);
		}
	);
	$("#signupPage .username").change(
		function() {
			$.post(
				js_baseurl+'usercalls/CheckUsername',
				{ username: $("#signupPage .username").val()},
				function(e) {
					if(e == 0) {
						$("#errmessage").fadeOut("fast", function () {});
					}
					else {
						$("#errmessage").html("Username already Exist");
						$("#errmessage").fadeIn("fast", function () {});
					}
				}
			);
		}
	);
	$("#signupPage .email").change(
		function() {
			if(checkEmail($("#signupPage .email").val())) {
				$("#errmessage").fadeOut("fast", function () {});
				$.post(
					js_baseurl+'usercalls/CheckEmail',
					{ email: $("#signupPage .email").val()},
					function(e) {
						if(e == 0) {
							$("#errmessage").fadeOut("fast", function () {});
						}
						else {
							$("#errmessage").html("Email already Exist");
							$("#errmessage").fadeIn("fast", function () {});
						}
					}
				);
			}
			else {
				$("#errmessage").html("Invalid Email");
				$("#errmessage").fadeIn("fast", function () {});
			}
			
		}
	);
	$("#loginbox #loginbutton").click(
		function() {
			$.post(
				js_baseurl+'usercalls/LoginUser',
				{
					username: $("#loginbox .username").val(),
					password: $("#loginbox .password").val()
				},
				function(e) {
					if(e == 0) {
						$("#errmessage2").html("Invalid Login");
						$("#errmessage2").fadeIn("fast", function () {});
					}
					else {
						$("#errmessage2").html("Correct Login");
						window.location = js_baseurl;
					}
				}
			);
		}
	);
});