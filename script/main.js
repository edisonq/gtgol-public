$(document).ready(function() {
	$('.searchField').each(function() {
	    var default_value = this.value;
	    $(this).css('color', '#666'); // this could be in the style sheet instead
	    $(this).focus(function() {
	        if(this.value == default_value) {
	            this.value = '';
	            $(this).css('color', '#333');
	        }
	    });
	    $(this).blur(function() {
	        if(this.value == '') {
	            $(this).css('color', '#666');
	            this.value = default_value;
	        }
			console.log(this.value);
	    });
	});
	/*
	$(".meter > .meter-inner > span").each(function() {
		$(this)
			.data("origWidth", $(this).width())
			.width(0)
			.animate({
				width: 300
			}, 1200);
	});
	
	$(".torch").each(function(){
		$(this)
			.data("origWidth", $(this).width())
			.width(0)
			.animate({
				width: 300
			}, 1200);
	});
	
	$(".torch-small").each(function(){
		$(this)
			.data("origWidth", $(this).width())
			.width(0)
			.animate({
				width: 100
			}, 1200);
	});	
	
	$(".meter-small > .meter-inner-small > span,.progressBar-inner-meter > span").each(function() {
		$(this)
			.data("origWidth", $(this).width())
			.width(0)
			.animate({
				width: 100
			}, 1200);
	});*/
});

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){ return false; }
    return true;
}