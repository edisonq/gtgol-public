$(document).ready(function() {
	function checkEmail(MyEmail) {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(MyEmail)){
			return (true)
		}
		/*Invalid email*/
		return (false);
	}
	function checkFields() {
		/*
			#signupPage .username
			#signupPage .email
			#signupPage .firstname
			#signupPage .lastname
		*/
		var ret = true;
		
		if(
			($('#signupPage .firstname').val() != '') && 
			($('#signupPage .lastname').val() != '') &&
			($('#signupPage .username').val() != '') && 
			($('#signupPage .email').val() != '')	
		  ){
			$("#errmessage").fadeOut("fast", function () {});
			return VerifyExistence();
		}
		else {
			if($('#signupPage .username').val() == '') 
				$("#errmessage").html("Username is required");
			else if($('#signupPage .email').val() == '') 
				$("#errmessage").html("Email is required");
			else if($('#signupPage .firstname').val() == '') 
				$("#errmessage").html("First name is required");
			else if($('#signupPage .lastname').val() == '') 
				$("#errmessage").html("Last name is required");
			$("#errmessage").fadeIn("fast", function () {});
		}
		return ret;
		
	}
	function VerifyExistence() {
		var ret = false;
			if(checkEmail($("#signupPage .email").val())) {
				
				$.post(
					js_baseurl+'user/VerifyExist',
					{ 
						email: $("#signupPage .email").val(),
						username: $("#signupPage .username").val()
					},
					function(e) {
						if(e == 0) {
							$("#errmessage").fadeOut("fast", function () {});
							ret = true;
						}
						else {
							$("#errmessage").html("User already Exist");
							$("#errmessage").fadeIn("fast", function () {});
							ret = false;
						}
					}
				);
			}
			else {
				$("#errmessage").html("Invalid Email");
				$("#errmessage").fadeIn("fast", function () {});
				ret = false;
			}
		return ret;
	}
	$("#signupPage #signupButton").click(
		function() {
			if(checkFields()==false) {
				$("#errmessage").html("Valid");
				/*
					ACTIONS
					Send an verification email to the registrant
					also registers the user
				*/
				var fn = $('#signupPage .firstname').val();
				var ln = $('#signupPage .lastname').val();
				var un = $('#signupPage .username').val();
				var em = $('#signupPage .email').val();
				$.post(
					js_baseurl+'user/RegisterUser',
					{ email: em,username: un,firstname: fn,lastname: ln },
					function(e) {
						
					}
				);
			}
			else {}
		}
	);
	$("#signupPage .username").change(
		function() {
			$.post(
				js_baseurl+'user/CheckUsername',
				{ username: $("#signupPage .username").val()},
				function(e) {
					if(e == 0) {
						$("#errmessage").fadeOut("fast", function () {});
					}
					else {
						$("#errmessage").html("Username already Exist");
						$("#errmessage").fadeIn("fast", function () {});
					}
				}
			);
		}
	);
	$("#signupPage .email").change(
		function() {
			if(checkEmail($("#signupPage .email").val())) {
				$("#errmessage").fadeOut("fast", function () {});
				$.post(
					js_baseurl+'user/CheckEmail',
					{ email: $("#signupPage .email").val()},
					function(e) {
						if(e == 0) {
							$("#errmessage").fadeOut("fast", function () {});
						}
						else {
							$("#errmessage").html("Email already Exist");
							$("#errmessage").fadeIn("fast", function () {});
						}
					}
				);
			}
			else {
				$("#errmessage").html("Invalid Email");
				$("#errmessage").fadeIn("fast", function () {});
			}
			
		}
	);
});