$(function() {
	
	var options = {
		success:function(data) {
			$('#wait-ui').css('display','none');
			//wherever the result is we show the button
			$(".submitNeedBtn").css('display','block');
			if(data == "true") {
				//transaction is accepted
				//so we reset the whole form with cc info
				$("#frmDonate").trigger("reset");
				$('#modal-ui').css('display','block');
				$('#modal-ui .bot').css('display','block');
				$('#modal-ui .bot2').css('display','none');
				$('#dialog-ui').css('display','block');				
			}
			else {
				$('#modal-ui').css('display','block');
				$('#modal-ui .bot').css('display','none');
				$('#modal-ui .bot2').css('display','block');
				$('#dialog-ui').css('display','block');
			}
			
		}
	}
	
	$("#frmDonate").validate();
	$(".submitNeedBtn").click(function(){
		if($("#frmDonate").valid()) {
			window.location = '#';
			$('#wait-ui').css('display','block');
			$('#dialog-ui').css('display','block');
			$("#frmDonate").ajaxSubmit(options);
			//hiding the checkout button to prevent double charging
			$(this).css('display','none');
			//alert('Please wait while we are processing your donation!');
		}		
	});
	$(".close").click(
		function() {
			$('#dialog-ui').css('display','none');
			$('#modal-ui').css('display','none');
		}
	);
	$("#ispublish").click(
		function(){
			var isChecked = $('#ispublish').attr('checked')?1:0;
			var frmAction = $('#frmDonate').attr('action');			
			$('#frmDonate').attr('action', ((frmAction.substring(0, frmAction.length - 1))+isChecked));
		}
	);
});