$(document).ready(function() {
	$("#searchButton").click(
		function() 	{
			$.post(
				js_baseurl+'search/Basic',
				{
					websiteSearchinput: $("#websiteSearchinput").val(),
				},
				function(e) {
					if(e != 1) {
						var err = e.substring(1);
						err = err.substring(0,err.length - 1);
						$("#errmessage").html(err);
						console.log(err);
						$("#errmessage").fadeIn("fast", function () {});
					}
					else {
						
						$("#errmessage").fadeOut("fast", function () {});
						window.location = js_baseurl+'welcome/?name='+$("#signupPage .username").val();
					}
				}
			);
		}
	);
});