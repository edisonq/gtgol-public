SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `giftofgivingdb` ;
CREATE SCHEMA IF NOT EXISTS `giftofgivingdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;

-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_category` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_category` (
  `CategoryID` INT NOT NULL AUTO_INCREMENT ,
  `CategoryName` VARCHAR(45) NULL ,
  `CategoryDesc` VARCHAR(255) NULL ,
  `IsActive` TINYINT(1) NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`CategoryID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_needstatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_needstatus` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_needstatus` (
  `StatusID` INT NOT NULL AUTO_INCREMENT ,
  `StatusMessage` VARCHAR(255) NULL ,
  `IsActive` TINYINT(1) NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`StatusID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_usertypes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_usertypes` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_usertypes` (
  `UserTypeID` INT NOT NULL ,
  `UserTypeName` VARCHAR(45) NULL ,
  `UserTypeDescription` VARCHAR(255) NULL ,
  `IsActive` TINYINT(1) NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`UserTypeID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`sfl_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`sfl_users` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`sfl_users` (
  `user_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `user_facebook` VARCHAR(45) NULL ,
  `user_school` VARCHAR(45) NULL ,
  `user_gradyear` YEAR NULL ,
  `user_phone` VARCHAR(45) NULL ,
  `user_addr` VARCHAR(100) NULL ,
  `user_city` VARCHAR(45) NULL ,
  `user_state` VARCHAR(2) NULL ,
  `user_zip` VARCHAR(10) NULL ,
  `user_country` VARCHAR(45) NULL ,
  `user_login_init` INT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`user_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`wp_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`wp_users` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`wp_users` (
  `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `user_login` VARCHAR(60) NULL ,
  `user_pass` VARCHAR(64) NULL ,
  `user_nicename` VARCHAR(50) NULL ,
  `user_email` VARCHAR(100) NULL ,
  `user_url` VARCHAR(100) NULL ,
  `user_registered` DATETIME NULL ,
  `user_activation_key` VARCHAR(60) NULL ,
  `user_status` INT(11) NULL ,
  `display_name` VARCHAR(250) NULL ,
  `sfl_users_user_id` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `fk_wp_users_sfl_users1_idx` (`sfl_users_user_id` ASC) ,
  CONSTRAINT `fk_wp_users_sfl_users1`
    FOREIGN KEY (`sfl_users_user_id` )
    REFERENCES `giftofgivingdb`.`sfl_users` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_users` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_users` (
  `UserID` INT NOT NULL AUTO_INCREMENT ,
  `SFLUserID` BIGINT(20) NULL ,
  `IsActive` TINYINT(1) NOT NULL DEFAULT 0 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `UserTypeID` INT NOT NULL ,
  `wp_users_ID` BIGINT(20) UNSIGNED NOT NULL ,
  PRIMARY KEY (`UserID`) ,
  INDEX `fk_ggol_users_ggol_usertypes1_idx` (`UserTypeID` ASC) ,
  INDEX `fk_ggol_users_wp_users1_idx` (`wp_users_ID` ASC) ,
  CONSTRAINT `fk_ggol_users_ggol_usertypes1`
    FOREIGN KEY (`UserTypeID` )
    REFERENCES `giftofgivingdb`.`ggol_usertypes` (`UserTypeID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_users_wp_users1`
    FOREIGN KEY (`wp_users_ID` )
    REFERENCES `giftofgivingdb`.`wp_users` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_needs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_needs` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_needs` (
  `NeedID` INT NOT NULL AUTO_INCREMENT ,
  `Beneficiary` VARCHAR(255) NULL ,
  `Title` VARCHAR(255) NULL ,
  `Description` VARCHAR(255) NULL ,
  `RequiredFunds` DECIMAL NULL ,
  `DateNeeded` DATETIME NULL ,
  `IsApproved` TINYINT(1)  NULL ,
  `ApprovalDate` DATETIME NULL ,
  `IsActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `CategoryID` INT NOT NULL ,
  `StatusID` INT NOT NULL ,
  `UserID` INT NOT NULL ,
  PRIMARY KEY (`NeedID`) ,
  INDEX `fk_ggol_needs_ggol_category_idx` (`CategoryID` ASC) ,
  INDEX `fk_ggol_needs_ggol_needstatus1_idx` (`StatusID` ASC) ,
  INDEX `fk_ggol_needs_ggol_users1_idx` (`UserID` ASC) ,
  CONSTRAINT `fk_ggol_needs_ggol_category`
    FOREIGN KEY (`CategoryID` )
    REFERENCES `giftofgivingdb`.`ggol_category` (`CategoryID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_needs_ggol_needstatus1`
    FOREIGN KEY (`StatusID` )
    REFERENCES `giftofgivingdb`.`ggol_needstatus` (`StatusID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_needs_ggol_users1`
    FOREIGN KEY (`UserID` )
    REFERENCES `giftofgivingdb`.`ggol_users` (`UserID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_tag` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_tag` (
  `TagID` INT NOT NULL AUTO_INCREMENT ,
  `NeedID` INT NOT NULL ,
  `IsActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `ggol_tagcol` VARCHAR(255) NULL ,
  PRIMARY KEY (`TagID`) ,
  INDEX `fk_ggol_tag_ggol_needs1_idx` (`NeedID` ASC) ,
  CONSTRAINT `fk_ggol_tag_ggol_needs1`
    FOREIGN KEY (`NeedID` )
    REFERENCES `giftofgivingdb`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_media`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_media` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_media` (
  `MediaID` INT NOT NULL AUTO_INCREMENT ,
  `MediaType` VARCHAR(45) NULL ,
  `MediaName` VARCHAR(45) NULL ,
  `Description` VARCHAR(255) NULL ,
  `MediaURL` VARCHAR(45) NULL ,
  `IsActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`MediaID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_stories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_stories` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_stories` (
  `StoryID` INT NOT NULL AUTO_INCREMENT ,
  `MediaID` INT NULL ,
  `IsActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `NeedID` INT NOT NULL ,
  `thankYouLetter` TEXT NULL ,
  PRIMARY KEY (`StoryID`) ,
  INDEX `fk_ggol_stories_ggol_needs1_idx` (`NeedID` ASC) ,
  CONSTRAINT `fk_ggol_stories_ggol_needs1`
    FOREIGN KEY (`NeedID` )
    REFERENCES `giftofgivingdb`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_donor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_donor` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_donor` (
  `DonorID` INT NOT NULL AUTO_INCREMENT ,
  `IsActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `UserID` INT NOT NULL ,
  PRIMARY KEY (`DonorID`) ,
  INDEX `fk_ggol_donor_ggol_users1_idx` (`UserID` ASC) ,
  CONSTRAINT `fk_ggol_donor_ggol_users1`
    FOREIGN KEY (`UserID` )
    REFERENCES `giftofgivingdb`.`ggol_users` (`UserID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_donations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_donations` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_donations` (
  `DonationID` INT NOT NULL AUTO_INCREMENT ,
  `TransactionNumber` VARCHAR(45) NULL ,
  `DonationReason` VARCHAR(255) NULL ,
  `DonatedAmount` DECIMAL NULL ,
  `IsPublishDonorsName` TINYINT(1)  NULL ,
  `IsActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `DateDonated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `DateCreated` DATETIME NULL ,
  `NeedID` INT NOT NULL ,
  `DonorID` INT NOT NULL ,
  PRIMARY KEY (`DonationID`) ,
  INDEX `fk_ggol_donations_ggol_needs1_idx` (`NeedID` ASC) ,
  INDEX `fk_ggol_donations_ggol_donor1_idx` (`DonorID` ASC) ,
  CONSTRAINT `fk_ggol_donations_ggol_needs1`
    FOREIGN KEY (`NeedID` )
    REFERENCES `giftofgivingdb`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_donations_ggol_donor1`
    FOREIGN KEY (`DonorID` )
    REFERENCES `giftofgivingdb`.`ggol_donor` (`DonorID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_cms_menus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_cms_menus` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_cms_menus` (
  `MenuID` INT NOT NULL ,
  `MenuName` INT NULL ,
  `IsActive` TINYINT(1) NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`MenuID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_cms_page_template`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_cms_page_template` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_cms_page_template` (
  `PageTemplateID` INT NOT NULL AUTO_INCREMENT ,
  `PageTemplateName` VARCHAR(45) NULL ,
  `PageTemplateDescription` VARCHAR(255) NULL ,
  `IsActive` TINYINT(1) NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`PageTemplateID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_cms_page`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_cms_page` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_cms_page` (
  `PageID` INT NOT NULL AUTO_INCREMENT ,
  `PageName` VARCHAR(45) NULL ,
  `PageDescription` VARCHAR(45) NULL ,
  `PageKeyword` VARCHAR(45) NULL ,
  `PageTitle` VARCHAR(255) NULL ,
  `PageHTMLContent` TEXT NULL ,
  `IsActive` TINYINT(1)  NOT NULL DEFAULT 1 ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `MenuID` INT NOT NULL ,
  `PageTemplateID` INT NOT NULL ,
  PRIMARY KEY (`PageID`) ,
  INDEX `fk_ggol_cms_page_ggol_cms_menus1_idx` (`MenuID` ASC) ,
  INDEX `fk_ggol_cms_page_ggol_cms_page_template1_idx` (`PageTemplateID` ASC) ,
  CONSTRAINT `fk_ggol_cms_page_ggol_cms_menus1`
    FOREIGN KEY (`MenuID` )
    REFERENCES `giftofgivingdb`.`ggol_cms_menus` (`MenuID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_cms_page_ggol_cms_page_template1`
    FOREIGN KEY (`PageTemplateID` )
    REFERENCES `giftofgivingdb`.`ggol_cms_page_template` (`PageTemplateID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`wp_usermeta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`wp_usermeta` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`wp_usermeta` (
  `umeta_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `user_id` BIGINT(20) NULL ,
  `meta_key` VARCHAR(255) NULL ,
  `meta_value` LONGTEXT NULL ,
  `sfl_users_user_id` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`umeta_id`) ,
  INDEX `fk_wp_usermeta_sfl_users1_idx` (`sfl_users_user_id` ASC) ,
  CONSTRAINT `fk_wp_usermeta_sfl_users1`
    FOREIGN KEY (`sfl_users_user_id` )
    REFERENCES `giftofgivingdb`.`sfl_users` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`sfl_student`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`sfl_student` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`sfl_student` (
  `user_id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `student_active` TINYINT(1) NULL ,
  `school_id` INT(11) NOT NULL ,
  `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  `wp_users_ID` BIGINT(20) UNSIGNED NOT NULL ,
  PRIMARY KEY (`user_id`, `school_id`) ,
  INDEX `fk_sfl_student_wp_users1_idx` (`wp_users_ID` ASC) ,
  CONSTRAINT `fk_sfl_student_wp_users1`
    FOREIGN KEY (`wp_users_ID` )
    REFERENCES `giftofgivingdb`.`wp_users` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `giftofgivingdb`.`ggol_media_has_ggol_needs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `giftofgivingdb`.`ggol_media_has_ggol_needs` ;

CREATE  TABLE IF NOT EXISTS `giftofgivingdb`.`ggol_media_has_ggol_needs` (
  `HasMediaID` INT NOT NULL AUTO_INCREMENT ,
  `MediaID` INT NOT NULL ,
  `NeedID` INT NOT NULL ,
  `StoryID` INT NULL ,
  `IsActive` TINYINT(1)  NOT NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  INDEX `fk_ggol_media_has_ggol_needs_ggol_media1` (`MediaID` ASC) ,
  INDEX `fk_ggol_media_has_ggol_needs_ggol_needs1` (`NeedID` ASC) ,
  PRIMARY KEY (`HasMediaID`) ,
  INDEX `fk_ggol_media_has_ggol_needs_ggol_stories1` (`StoryID` ASC) ,
  CONSTRAINT `fk_ggol_media_has_ggol_needs_ggol_media1`
    FOREIGN KEY (`MediaID` )
    REFERENCES `giftofgivingdb`.`ggol_media` (`MediaID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_media_has_ggol_needs_ggol_needs1`
    FOREIGN KEY (`NeedID` )
    REFERENCES `giftofgivingdb`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_media_has_ggol_needs_ggol_stories1`
    FOREIGN KEY (`StoryID` )
    REFERENCES `giftofgivingdb`.`ggol_stories` (`StoryID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Placeholder table for view `giftofgivingdb`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `giftofgivingdb`.`view1` (`id` INT);

-- -----------------------------------------------------
-- View `giftofgivingdb`.`view1`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `giftofgivingdb`.`view1` ;
DROP TABLE IF EXISTS `giftofgivingdb`.`view1`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `giftofgivingdb`.`ggol_category`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
INSERT INTO `giftofgivingdb`.`ggol_category` (`CategoryID`, `CategoryName`, `CategoryDesc`, `IsActive`, `DateCreated`, `DateModified`) VALUES (NULL, 'Travel Scholarships', 'Travel Scholarships', '1', NULL, NULL);
INSERT INTO `giftofgivingdb`.`ggol_category` (`CategoryID`, `CategoryName`, `CategoryDesc`, `IsActive`, `DateCreated`, `DateModified`) VALUES (NULL, 'Protests', 'Protests', '1', NULL, NULL);
INSERT INTO `giftofgivingdb`.`ggol_category` (`CategoryID`, `CategoryName`, `CategoryDesc`, `IsActive`, `DateCreated`, `DateModified`) VALUES (NULL, 'Campus Events', 'Campus Events', '1', NULL, NULL);
INSERT INTO `giftofgivingdb`.`ggol_category` (`CategoryID`, `CategoryName`, `CategoryDesc`, `IsActive`, `DateCreated`, `DateModified`) VALUES (NULL, 'Books', 'Books', '1', NULL, NULL);
INSERT INTO `giftofgivingdb`.`ggol_category` (`CategoryID`, `CategoryName`, `CategoryDesc`, `IsActive`, `DateCreated`, `DateModified`) VALUES (NULL, 'Other Group Supplies', 'Other Group Supplies', '1', NULL, NULL);

COMMIT;
