SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `wp_sfl` ;
CREATE SCHEMA IF NOT EXISTS `wp_sfl` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;

-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_typeneeds`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_typeneeds` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_typeneeds` (
  `TypeneedsID` INT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(250) NULL ,
  `Description` TEXT NULL ,
  `Isactive` TINYINT(1)  NULL ,
  `Datecreated` DATETIME NULL ,
  `Datemodified` DATETIME NULL ,
  PRIMARY KEY (`TypeneedsID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_user_sfl`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_user_sfl` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_user_sfl` (
  `User_sflID` INT NOT NULL AUTO_INCREMENT ,
  `Id_fromSFL` INT NULL ,
  PRIMARY KEY (`User_sflID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_needs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_needs` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_needs` (
  `NeedID` INT NOT NULL AUTO_INCREMENT ,
  `ggol_User_sflID` INT NOT NULL ,
  `StatusID` INT NULL ,
  `Beneficiary` VARCHAR(255) NULL ,
  `Title` VARCHAR(255) NULL ,
  `Description` VARCHAR(255) NULL ,
  `RequiredFunds` DECIMAL NULL ,
  `DateNeeded` DATETIME NULL ,
  `ggol_TypeneedsID` INT NOT NULL ,
  `CategoryID` INT NULL ,
  `IsActive` TINYINT(1)  NULL ,
  `IsApproved` TINYINT(1)  NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `DateApproved` DATETIME NULL ,
  PRIMARY KEY (`NeedID`) ,
  INDEX `fk_ggol_needs_ggol_typeneeds1` (`ggol_TypeneedsID` ASC) ,
  INDEX `fk_ggol_needs_ggol_user_sfl1` (`ggol_User_sflID` ASC) ,
  CONSTRAINT `fk_ggol_needs_ggol_typeneeds1`
    FOREIGN KEY (`ggol_TypeneedsID` )
    REFERENCES `wp_sfl`.`ggol_typeneeds` (`TypeneedsID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_needs_ggol_user_sfl1`
    FOREIGN KEY (`ggol_User_sflID` )
    REFERENCES `wp_sfl`.`ggol_user_sfl` (`User_sflID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_tag` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_tag` (
  `TagID` INT NOT NULL AUTO_INCREMENT ,
  `ggol_NeedID` INT NOT NULL ,
  `tag` VARCHAR(250) NULL ,
  `IsActive` BINARY NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`TagID`) ,
  INDEX `fk_ggol_tag_ggol_needs1` (`ggol_NeedID` ASC) ,
  CONSTRAINT `fk_ggol_tag_ggol_needs1`
    FOREIGN KEY (`ggol_NeedID` )
    REFERENCES `wp_sfl`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_category` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_category` (
  `CategoryID` INT NOT NULL AUTO_INCREMENT ,
  `CategoryName` VARCHAR(45) NULL ,
  `CategoryDesc` VARCHAR(255) NULL ,
  `IsActive` BINARY NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`CategoryID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_media`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_media` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_media` (
  `MediaID` INT NOT NULL AUTO_INCREMENT ,
  `MediaType` VARCHAR(45) NULL ,
  `MediaName` VARCHAR(45) NULL ,
  `Description` VARCHAR(255) NULL ,
  `MediaURL` VARCHAR(45) NULL ,
  `IsActive` BINARY NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`MediaID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_needstatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_needstatus` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_needstatus` (
  `StatusID` INT NOT NULL AUTO_INCREMENT ,
  `ggol_NeedID` INT NOT NULL ,
  `StatusMessage` VARCHAR(255) NULL ,
  `IsActive` TINYINT(1)  NULL ,
  `IsApproved` TINYINT(1)  NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `DateApproved` DATETIME NULL ,
  PRIMARY KEY (`StatusID`) ,
  INDEX `fk_ggol_needstatus_ggol_needs1` (`ggol_NeedID` ASC) ,
  CONSTRAINT `fk_ggol_needstatus_ggol_needs1`
    FOREIGN KEY (`ggol_NeedID` )
    REFERENCES `wp_sfl`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_stories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_stories` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_stories` (
  `StoryID` INT NOT NULL AUTO_INCREMENT ,
  `NeedID` INT NULL ,
  `MediaID` INT NULL ,
  `IsActive` BINARY NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  `thankYouLetter` TEXT NULL ,
  `ggol_NeedID` INT NOT NULL ,
  PRIMARY KEY (`StoryID`) ,
  INDEX `fk_ggol_stories_ggol_needs1` (`ggol_NeedID` ASC) ,
  CONSTRAINT `fk_ggol_stories_ggol_needs1`
    FOREIGN KEY (`ggol_NeedID` )
    REFERENCES `wp_sfl`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_donor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_donor` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_donor` (
  `DonorID` INT NOT NULL AUTO_INCREMENT ,
  `UserID` INT NULL ,
  `IsActive` BINARY NULL ,
  `DateCreated` DATETIME NULL ,
  `DateModified` DATETIME NULL ,
  PRIMARY KEY (`DonorID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_donations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_donations` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_donations` (
  `DonationID` INT NOT NULL AUTO_INCREMENT ,
  `DonorID` INT NULL ,
  `ggol_NeedID` INT NOT NULL ,
  `ggol_User_sflID` INT NOT NULL ,
  `TransactionNumber` VARCHAR(45) NULL ,
  `DonationReason` TEXT NULL ,
  `DonatedAmount` DECIMAL NULL ,
  `IsPublishDonorsName` TINYINT(1)  NULL ,
  `DateDonated` DATETIME NULL ,
  `IsActive` TINYINT(1)  NULL ,
  `DateModified` DATETIME NULL ,
  `DateCreated` DATETIME NULL ,
  PRIMARY KEY (`DonationID`) ,
  INDEX `fk_ggol_donations_ggol_needs1` (`ggol_NeedID` ASC) ,
  INDEX `fk_ggol_donations_ggol_user_sfl1` (`ggol_User_sflID` ASC) ,
  CONSTRAINT `fk_ggol_donations_ggol_needs1`
    FOREIGN KEY (`ggol_NeedID` )
    REFERENCES `wp_sfl`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_donations_ggol_user_sfl1`
    FOREIGN KEY (`ggol_User_sflID` )
    REFERENCES `wp_sfl`.`ggol_user_sfl` (`User_sflID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_needs_has_ggol_media`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_needs_has_ggol_media` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_needs_has_ggol_media` (
  `needs_mediaID` INT NOT NULL AUTO_INCREMENT ,
  `ggol_NeedID` INT NOT NULL ,
  `ggol_MediaID` INT NOT NULL ,
  PRIMARY KEY (`needs_mediaID`) ,
  INDEX `fk_ggol_needs_has_ggol_media_ggol_needs1` (`ggol_NeedID` ASC) ,
  INDEX `fk_ggol_needs_has_ggol_media_ggol_media1` (`ggol_MediaID` ASC) ,
  CONSTRAINT `fk_ggol_needs_has_ggol_media_ggol_needs1`
    FOREIGN KEY (`ggol_NeedID` )
    REFERENCES `wp_sfl`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_needs_has_ggol_media_ggol_media1`
    FOREIGN KEY (`ggol_MediaID` )
    REFERENCES `wp_sfl`.`ggol_media` (`MediaID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_admin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_admin` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_admin` (
  `adminID` INT NOT NULL AUTO_INCREMENT ,
  `ggol_User_sflID` INT NOT NULL ,
  PRIMARY KEY (`adminID`) ,
  INDEX `fk_ggol_admin_ggol_user_sfl1` (`ggol_User_sflID` ASC) ,
  CONSTRAINT `fk_ggol_admin_ggol_user_sfl1`
    FOREIGN KEY (`ggol_User_sflID` )
    REFERENCES `wp_sfl`.`ggol_user_sfl` (`User_sflID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_approved_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_approved_status` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_approved_status` (
  `approved_needsID` INT NOT NULL AUTO_INCREMENT ,
  `ggol_needstatus_StatusID` INT NOT NULL ,
  `ggol_admin_adminID` INT NOT NULL ,
  INDEX `fk_ggol_approved_needs_ggol_needstatus1` (`ggol_needstatus_StatusID` ASC) ,
  INDEX `fk_ggol_approved_needs_ggol_admin1` (`ggol_admin_adminID` ASC) ,
  UNIQUE INDEX `ggol_needstatus_StatusID_UNIQUE` (`ggol_needstatus_StatusID` ASC) ,
  PRIMARY KEY (`approved_needsID`) ,
  CONSTRAINT `fk_ggol_approved_needs_ggol_needstatus1`
    FOREIGN KEY (`ggol_needstatus_StatusID` )
    REFERENCES `wp_sfl`.`ggol_needstatus` (`StatusID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_approved_needs_ggol_admin1`
    FOREIGN KEY (`ggol_admin_adminID` )
    REFERENCES `wp_sfl`.`ggol_admin` (`adminID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_approved_needs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_approved_needs` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_approved_needs` (
  `approved_needsID` INT NOT NULL AUTO_INCREMENT ,
  `ggol_needs_NeedID` INT NOT NULL ,
  `ggol_admin_adminID` INT NOT NULL ,
  PRIMARY KEY (`approved_needsID`) ,
  INDEX `fk_ggol_approved_needs_ggol_needs1` (`ggol_needs_NeedID` ASC) ,
  UNIQUE INDEX `ggol_needs_NeedID_UNIQUE` (`ggol_needs_NeedID` ASC) ,
  INDEX `fk_ggol_approved_needs_ggol_admin2` (`ggol_admin_adminID` ASC) ,
  CONSTRAINT `fk_ggol_approved_needs_ggol_needs1`
    FOREIGN KEY (`ggol_needs_NeedID` )
    REFERENCES `wp_sfl`.`ggol_needs` (`NeedID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_approved_needs_ggol_admin2`
    FOREIGN KEY (`ggol_admin_adminID` )
    REFERENCES `wp_sfl`.`ggol_admin` (`adminID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wp_sfl`.`ggol_stories_has_ggol_media`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wp_sfl`.`ggol_stories_has_ggol_media` ;

CREATE  TABLE IF NOT EXISTS `wp_sfl`.`ggol_stories_has_ggol_media` (
  `ggol_stories_StoryID` INT NOT NULL ,
  `ggol_media_MediaID` INT NOT NULL ,
  `type` VARCHAR(45) NULL ,
  PRIMARY KEY (`ggol_stories_StoryID`) ,
  INDEX `fk_ggol_stories_has_ggol_media_ggol_stories1` (`ggol_stories_StoryID` ASC) ,
  INDEX `fk_ggol_stories_has_ggol_media_ggol_media1` (`ggol_media_MediaID` ASC) ,
  CONSTRAINT `fk_ggol_stories_has_ggol_media_ggol_stories1`
    FOREIGN KEY (`ggol_stories_StoryID` )
    REFERENCES `wp_sfl`.`ggol_stories` (`StoryID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ggol_stories_has_ggol_media_ggol_media1`
    FOREIGN KEY (`ggol_media_MediaID` )
    REFERENCES `wp_sfl`.`ggol_media` (`MediaID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
