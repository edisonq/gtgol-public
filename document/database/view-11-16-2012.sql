DROP TABLE IF EXISTS `giftofgivingdb`.`almostdueneeds` ;
DROP TABLE IF EXISTS `giftofgivingdb`.`latestneeds` ;
-- -----------------------------------------------------
-- View `giftofgivingdb`.`almostdueneeds`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `giftofgivingdb`.`almostdueneeds` ;
DROP TABLE IF EXISTS `giftofgivingdb`.`almostdueneeds`;
CREATE  OR REPLACE VIEW `giftofgivingdb`.`almostdueneeds` AS select `gn`.`NeedID` AS `NeedID`,`gn`.`Beneficiary` AS `Beneficiary`,`gn`.`Title` AS `Title`,`gn`.`Description` AS `Description`,`gn`.`RequiredFunds` AS `RequiredFunds`,`gn`.`DescriptionFunds` AS `DescriptionFunds`,`gn`.`DateNeeded` AS `DateNeeded`,`gn`.`IsApproved` AS `IsApproved`,`gn`.`ApprovalDate` AS `ApprovalDate`,`gn`.`IsActive` AS `IsActive`,`gn`.`DateCreated` AS `DateCreated`,`gn`.`DateModified` AS `DateModified`,`gn`.`CategoryID` AS `CategoryID`,`gn`.`UserID` AS `UserID`,(select sum(`giftofgivingdb`.`ggol_donations`.`DonatedAmount`) from `giftofgivingdb`.`ggol_donations` where (`giftofgivingdb`.`ggol_donations`.`NeedID` = `gn`.`NeedID`)) AS `TotalDonation` from `giftofgivingdb`.`ggol_needs` `gn` where (`gn`.`IsApproved` = 1) order by `gn`.`ApprovalDate` limit 0,10;

-- -----------------------------------------------------
-- View `giftofgivingdb`.`latestneeds`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `giftofgivingdb`.`latestneeds` ;
DROP TABLE IF EXISTS `giftofgivingdb`.`latestneeds`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `giftofgivingdb`.`latestneeds` AS select `gn`.`NeedID` AS `NeedID`,`gn`.`Beneficiary` AS `Beneficiary`,`gn`.`Title` AS `Title`,`gn`.`Description` AS `Description`,`gn`.`RequiredFunds` AS `RequiredFunds`,`gn`.`DescriptionFunds` AS `DescriptionFunds`,`gn`.`DateNeeded` AS `DateNeeded`,`gn`.`IsApproved` AS `IsApproved`,`gn`.`ApprovalDate` AS `ApprovalDate`,`gn`.`IsActive` AS `IsActive`,`gn`.`DateCreated` AS `DateCreated`,`gn`.`DateModified` AS `DateModified`,`gn`.`CategoryID` AS `CategoryID`,`gn`.`UserID` AS `UserID`,(select sum(`giftofgivingdb`.`ggol_donations`.`DonatedAmount`) from `giftofgivingdb`.`ggol_donations` where (`giftofgivingdb`.`ggol_donations`.`NeedID` = `gn`.`NeedID`)) AS `TotalDonation` from `giftofgivingdb`.`ggol_needs` `gn` where (`gn`.`IsApproved` = 1) order by `gn`.`ApprovalDate` limit 0,10;