$(document).ready(function() {
	$('.searchField').each(function() {
	    var default_value = this.value;
	    $(this).css('color', '#666'); // this could be in the style sheet instead
	    $(this).focus(function() {
	        if(this.value == default_value) {
	            this.value = '';
	            $(this).css('color', '#333');
	        }
	    });
	    $(this).blur(function() {
	        if(this.value == '') {
	            $(this).css('color', '#666');
	            this.value = default_value;
	        }
			console.log(this.value);
	    });
	});

	$(".meter > .meter-inner > span").each(function() {
		$(this)
			.data("origWidth", $(this).width())
			.width(0)
			.animate({
				width: 1200
			}, 1200);
	});
	
	$('.torch')
		.data("origWidth", $(this).width())
		.width(0)
		.animate({
			width: 300
		}, 1200);
	
	$('.torch-small')
		.data("origWidth", $(this).width())
		.width(0)
		.animate({
			width: 100
		}, 1200);
	
	$(".meter-small > .meter-inner-small > span").each(function() {
		$(this)
			.data("origWidth", $(this).width())
			.width(0)
			.animate({
				width: 1200
			}, 1200);
	});
});