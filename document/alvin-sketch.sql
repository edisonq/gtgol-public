
-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'Needs'
-- 
-- ---

DROP TABLE IF EXISTS `Needs`;
		
CREATE TABLE `Needs` (
  `NeedID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `UserID` TINYINT NULL DEFAULT NULL,
  `StatusID` TINYINT NULL DEFAULT NULL,
  `Title` VARCHAR NULL DEFAULT NULL,
  `Beneficiary` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `RequiredFunds` TINYINT NULL DEFAULT NULL,
  `DateNeeded` DATETIME NULL DEFAULT NULL,
  `CategoryID` TINYINT NULL DEFAULT NULL,
  `MediaID` TINYINT NULL DEFAULT NULL,
  `IsApproved` bit NULL DEFAULT NULL,
  `ApprovalDate` DATETIME NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`NeedID`)
);

-- ---
-- Table 'Tag'
-- 
-- ---

DROP TABLE IF EXISTS `Tag`;
		
CREATE TABLE `Tag` (
  `TagID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`TagID`)
);

-- ---
-- Table 'Category'
-- 
-- ---

DROP TABLE IF EXISTS `Category`;
		
CREATE TABLE `Category` (
  `CategoryID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `CategoryName` VARCHAR NULL DEFAULT NULL,
  `CategoryDesc` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
);

-- ---
-- Table 'Media'
-- 
-- ---

DROP TABLE IF EXISTS `Media`;
		
CREATE TABLE `Media` (
  `MediaID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `MediaType` TINYINT NULL DEFAULT NULL,
  `MediaName` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `MediaURL` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`MediaID`)
);

-- ---
-- Table 'NeedStatus'
-- 
-- ---

DROP TABLE IF EXISTS `NeedStatus`;
		
CREATE TABLE `NeedStatus` (
  `StatusID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `StatusMessage` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
);

-- ---
-- Table 'Stories'
-- 
-- ---

DROP TABLE IF EXISTS `Stories`;
		
CREATE TABLE `Stories` (
  `StoryID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `MediaID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`StoryID`)
);

-- ---
-- Table 'Donor'
-- 
-- ---

DROP TABLE IF EXISTS `Donor`;
		
CREATE TABLE `Donor` (
  `DonorID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `UserID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`DonorID`)
);

-- ---
-- Table 'Donations'
-- 
-- ---

DROP TABLE IF EXISTS `Donations`;
		
CREATE TABLE `Donations` (
  `DonationID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `DonorID` TINYINT NULL DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `TransactionNumber` VARCHAR NULL DEFAULT NULL,
  `DonationReason` MEDIUMTEXT NULL DEFAULT NULL,
  `DonatedAmount` VARCHAR NULL DEFAULT NULL,
  `IsPublishDonorsName` BINARY NULL DEFAULT NULL,
  `DateDonated` DATETIME NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`DonationID`)
);

-- ---
-- Foreign Keys 
-- ---


-- ---
-- Table Properties
-- ---

-- ALTER TABLE `Needs` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Tag` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Category` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Media` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `NeedStatus` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Stories` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Donor` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Donations` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `Needs` (`NeedID`,`UserID`,`StatusID`,`Title`,`Beneficiary`,`Description`,`RequiredFunds`,`DateNeeded`,`CategoryID`,`MediaID`,`IsApproved`,`ApprovalDate`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','','','
-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'Needs'
-- 
-- ---

DROP TABLE IF EXISTS `Needs`;
		
CREATE TABLE `Needs` (
  `NeedID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `UserID` TINYINT NULL DEFAULT NULL,
  `StatusID` TINYINT NULL DEFAULT NULL,
  `Title` VARCHAR NULL DEFAULT NULL,
  `Beneficiary` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `RequiredFunds` TINYINT NULL DEFAULT NULL,
  `BudgetInfo` VARCHAR NULL DEFAULT NULL,
  `DateNeeded` DATETIME NULL DEFAULT NULL,
  `CategoryID` TINYINT NULL DEFAULT NULL,
  `MediaID` TINYINT NULL DEFAULT NULL,
  `IsApproved` bit NULL DEFAULT NULL,
  `ApprovalDate` DATETIME NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`NeedID`)
);

-- ---
-- Table 'Tag'
-- 
-- ---

DROP TABLE IF EXISTS `Tag`;
		
CREATE TABLE `Tag` (
  `TagID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`TagID`)
);

-- ---
-- Table 'Category'
-- 
-- ---

DROP TABLE IF EXISTS `Category`;
		
CREATE TABLE `Category` (
  `CategoryID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `CategoryName` VARCHAR NULL DEFAULT NULL,
  `CategoryDesc` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
);

-- ---
-- Table 'Media'
-- 
-- ---

DROP TABLE IF EXISTS `Media`;
		
CREATE TABLE `Media` (
  `MediaID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `MediaType` TINYINT NULL DEFAULT NULL,
  `MediaName` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `MediaURL` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`MediaID`)
);

-- ---
-- Table 'NeedStatus'
-- 
-- ---

DROP TABLE IF EXISTS `NeedStatus`;
		
CREATE TABLE `NeedStatus` (
  `StatusID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `StatusMessage` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
);

-- ---
-- Table 'Stories'
-- 
-- ---

DROP TABLE IF EXISTS `Stories`;
		
CREATE TABLE `Stories` (
  `StoryID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `MediaID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`StoryID`)
);

-- ---
-- Table 'Donor'
-- 
-- ---

DROP TABLE IF EXISTS `Donor`;
		
CREATE TABLE `Donor` (
  `DonorID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `UserID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`DonorID`)
);

-- ---
-- Table 'Donations'
-- 
-- ---

DROP TABLE IF EXISTS `Donations`;
		
CREATE TABLE `Donations` (
  `DonationID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `DonorID` TINYINT NULL DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `TransactionNumber` VARCHAR NULL DEFAULT NULL,
  `DonationReason` MEDIUMTEXT NULL DEFAULT NULL,
  `DonatedAmount` VARCHAR NULL DEFAULT NULL,
  `IsPublishDonorsName` BINARY NULL DEFAULT NULL,
  `DateDonated` DATETIME NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`DonationID`)
);

-- ---
-- Foreign Keys 
-- ---


-- ---
-- Table Properties
-- ---

-- ALTER TABLE `Needs` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Tag` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Category` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Media` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `NeedStatus` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Stories` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Donor` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Donations` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `Needs` (`NeedID`,`UserID`,`StatusID`,`Title`,`Beneficiary`,`Description`,`RequiredFunds`,`BudgetInfo`,`DateNeeded`,`CategoryID`,`MediaID`,`IsApproved`,`ApprovalDate`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','','','','','','','','','','','','');
-- INSERT INTO `Tag` (`TagID`,`NeedID`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','');
-- INSERT INTO `Category` (`CategoryID`,`CategoryName`,`CategoryDesc`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','','');
-- INSERT INTO `Media` (`MediaID`,`MediaType`,`MediaName`,`Description`,`MediaURL`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `NeedStatus` (`StatusID`,`StatusMessage`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','');
-- INSERT INTO `Stories` (`StoryID`,`NeedID`,`MediaID`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','','');
-- INSERT INTO `Donor` (`DonorID`,`UserID`,`IsActive`,`DateCreated`,`DateModified`) VALUES
-- ('','','','','');
-- INSERT INTO `Donations` (`DonationID`,`DonorID`,`NeedID`,`TransactionNumber`,`DonationReason`,`DonatedAmount`,`IsPublishDonorsName`,`DateDonated`,`IsActive`,`DateModified`,`DateCreated`) VALUES
-- ('','','','','','','','','','','');
