CREATE TABLE `Needs` (
  `NeedID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `UserID` TINYINT NULL DEFAULT NULL,
  `StatusID` TINYINT NULL DEFAULT NULL,
  `Title` VARCHAR NULL DEFAULT NULL,
  `Beneficiary` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `RequiredFunds` TINYINT NULL DEFAULT NULL,
  `DateNeeded` DATETIME NULL DEFAULT NULL,
  `CategoryID` TINYINT NULL DEFAULT NULL,
  `MediaID` TINYINT NULL DEFAULT NULL,
  `IsApproved` bit NULL DEFAULT NULL,
  `ApprovalDate` DATETIME NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`NeedID`)
);

CREATE TABLE `Tag` (
  `TagID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`TagID`)
);

CREATE TABLE `Category` (
  `CategoryID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `CategoryName` VARCHAR NULL DEFAULT NULL,
  `CategoryDesc` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
);


CREATE TABLE `Media` (
  `MediaID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `MediaType` TINYINT NULL DEFAULT NULL,
  `MediaName` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `MediaURL` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`MediaID`)
);

CREATE TABLE `NeedStatus` (
  `StatusID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `StatusMessage` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
);


		
CREATE TABLE `Stories` (
  `StoryID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `MediaID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`StoryID`)
);


CREATE TABLE `Donor` (
  `DonorID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `UserID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`DonorID`)
);
		
CREATE TABLE `Donations` (
  `DonationID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `DonorID` TINYINT NULL DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `TransactionNumber` VARCHAR NULL DEFAULT NULL,
  `DonationReason` MEDIUMTEXT NULL DEFAULT NULL,
  `DonatedAmount` VARCHAR NULL DEFAULT NULL,
  `IsPublishDonorsName` BINARY NULL DEFAULT NULL,
  `DateDonated` DATETIME NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`DonationID`)
);


CREATE TABLE `Needs` (
  `NeedID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `UserID` TINYINT NULL DEFAULT NULL,
  `StatusID` TINYINT NULL DEFAULT NULL,
  `Title` VARCHAR NULL DEFAULT NULL,
  `Beneficiary` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `RequiredFunds` TINYINT NULL DEFAULT NULL,
  `BudgetInfo` VARCHAR NULL DEFAULT NULL,
  `DateNeeded` DATETIME NULL DEFAULT NULL,
  `CategoryID` TINYINT NULL DEFAULT NULL,
  `MediaID` TINYINT NULL DEFAULT NULL,
  `IsApproved` bit NULL DEFAULT NULL,
  `ApprovalDate` DATETIME NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`NeedID`)
);

CREATE TABLE `Tag` (
  `TagID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `NeedID` TINYINT NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`TagID`)
);

CREATE TABLE `Category` (
  `CategoryID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `CategoryName` VARCHAR NULL DEFAULT NULL,
  `CategoryDesc` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
);

CREATE TABLE `Media` (
  `MediaID` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `MediaType` TINYINT NULL DEFAULT NULL,
  `MediaName` VARCHAR NULL DEFAULT NULL,
  `Description` VARCHAR NULL DEFAULT NULL,
  `MediaURL` VARCHAR NULL DEFAULT NULL,
  `IsActive` BINARY NULL DEFAULT NULL,
  `DateCreated` DATETIME NULL DEFAULT NULL,
  `DateModified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`MediaID`)
);